#!/bin/bash
# wget -O build.sh https://bitbucket.org/numberfive/sdl2guitests/raw/master/build.sh
# chmod +x build.sh
echo try to download and build carnine CarPC
wget -O DebianPackages.txt https://bitbucket.org/numberfive/sdl2guitests/raw/master/DebianPackages.txt
DEPENSFILE="DebianPackages.txt"

InstallPackage(){
	packageName="$1"
	PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $packageName|grep "install ok installed")
	echo Checking for $packageName: $PKG_OK
	if [ "" == "$PKG_OK" ]; then
		echo "No $packageName. Setting up $packageName."
		sudo apt-get --yes install $packageName
	fi
}

InstallSDLComponent(){
	packageName="$1"
	packageVersion="$2"
	if [ ! -d "$packageVersion" ]; then
		wget -N https://www.libsdl.org/projects/$packageName/release/$packageVersion.tar.gz
		exitCode=$?
		if [ $exitCode -ne 0 ] ; then
		   echo "wget give an Error"
		   exit $exitCode
		fi
		tar -xzf $packageVersion.tar.gz
		exitCode=$?
		if [ $exitCode -ne 0 ] ; then
		   echo "tar give an Error"
		   exit $exitCode
		fi
		cd $packageVersion
		./autogen.sh
		./configure
		make -j$(nproc)
		sudo make install
		cd ..
	fi
}

sudo apt-get --yes update
sudo apt-get --yes upgrade
sudo apt-get autoremove -y
sudo apt-get purge -y libsdl2-dev
sudo apt-get purge -y libsdl2-2.0-0
sudo apt-get autoremove -y

rpiversion=$(cat /sys/firmware/devicetree/base/compatible |cut -c1-13)
echo "PI $rpiversion"

InstallPackage git
InstallPackage cmake

while read LINE; do
     InstallPackage $LINE
done < $DEPENSFILE

DIRECTORY="SDL2-2.0.12"
if [ ! -d "$DIRECTORY" ]; then
	wget -N https://www.libsdl.org/release/$DIRECTORY.tar.gz
	exitCode=$?
	if [ $exitCode -ne 0 ] ; then
	   echo "wget give an Error"
	   exit $exitCode
	fi
	tar -xzf $DIRECTORY.tar.gz
	exitCode=$?
	if [ $exitCode -ne 0 ] ; then
	   echo "tar give an Error"
	   exit $exitCode
	fi
fi

cd $DIRECTORY
./autogen.sh
if [ "$rpiversion" == "raspberrypi,4" ] ; then
./configure --enable-video-kmsdrm --disable-esd --disable-video-wayland --disable-video-opengl --disable-video-rpi --host=arm-raspberry-linux-gnueabihf
else
./configure --disable-esd --disable-video-wayland --disable-video-opengl --host=arm-raspberry-linux-gnueabihf
fi
exitCode=$?
if [ $exitCode -ne 0 ] ; then
   echo "configure give an Error"
   exit $exitCode
fi
make -j$(nproc)
sudo make install
cd ..

InstallSDLComponent SDL_image SDL2_image-2.0.5
InstallSDLComponent SDL_mixer SDL2_mixer-2.0.4
InstallSDLComponent SDL_net SDL2_net-2.0.1
InstallSDLComponent SDL_ttf SDL2_ttf-2.0.15

DIRECTORY="libosmscout"
if [ ! -d "$DIRECTORY" ]; then
	git clone https://github.com/Framstag/libosmscout.git
	exitCode=$?
	if [ $exitCode -ne 0 ] ; then
	   echo "git give an Error"
	   exit $exitCode
	fi
	cd libosmscout
	git checkout 2e2d6c6
	exitCode=$?
	if [ $exitCode -ne 0 ] ; then
	   echo "git give an Error"
	   exit $exitCode
	fi
else
	cd libosmscout
	git checkout 2e2d6c6
	exitCode=$?
	if [ $exitCode -ne 0 ] ; then
	   echo "git give an Error"
	   exit $exitCode
	fi
fi

echo buildding libosmscout debug
DIRECTORY="build"
if [ ! -d "$DIRECTORY" ]; then
	mkdir $DIRECTORY
fi
cd $DIRECTORY
cmake .. -DCMAKE_BUILD_TYPE=Debug -DOSMSCOUT_ENABLE_SSE=ON
cmake --build .
sudo cmake --build . --target install
cd ..

echo buildding libosmscout release
DIRECTORY="buildrelease"
if [ ! -d "$DIRECTORY" ]; then
	mkdir $DIRECTORY
fi
cd $DIRECTORY
cmake .. -DCMAKE_BUILD_TYPE=Release -DOSMSCOUT_ENABLE_SSE=ON
cmake --build .
sudo cmake --build . --target install
cd ..
cd ..

echo buildding carnine

DIRECTORY="sdl2guitests"
if [ ! -d "$DIRECTORY" ]; then
	git clone https://bitbucket.org/numberfive/sdl2guitests.git
	exitCode=$?
	if [ $exitCode -ne 0 ] ; then
	   echo "git give an Error"
	   exit $exitCode
	fi
	cd sdl2guitests
else
	cd sdl2guitests
	git pull
	exitCode=$?
	if [ $exitCode -ne 0 ] ; then
	   echo "git give an Error"
	   exit $exitCode
	fi
fi

DIRECTORY="build-release"
if [ ! -d "$DIRECTORY" ]; then
	mkdir $DIRECTORY
fi
cd $DIRECTORY

cmake .. -DCMAKE_BUILD_TYPE=Release -DTARGET=Linux -DENABLE_CPPCHECK=OFF
if [ $exitCode -ne 0 ] ; then
	echo "cmake give an Error"
	exit $exitCode
fi

cmake --build .
exitCode=$?
if [ $exitCode -ne 0 ] ; then
	echo "cmake give an Error"
	exit $exitCode
fi
cd ..
