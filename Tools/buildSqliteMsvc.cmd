@echo off

REM Call Sample
REM buildSqliteMsvc D:\Mine\C++\SDL2GuiTests\src\SDL2PaintFunctionTest\Debug D:\Mine\C++\SDL2GuiTests\Debug
REM we need wget and seven Zip in path
REM sqlite need tlcsh for building the code

SETLOCAL ENABLEEXTENSIONS
echo Build Sqlite on windows

set buildir=%1
set outdir=%2

if "%buildir%"=="" (
	goto ExitError
)

if "%outdir%"=="" (
	goto ExitError
)

echo build in %buildir% put in %outdir%

IF NOT EXIST "%buildir%" (
	mkdir %buildir%
)

cd %buildir%

IF NOT EXIST "sqlite.tar.gz" (
	echo Downloading Sqlite source
    wget -o build_cmd.log -O sqlite.tar.gz https://www.sqlite.org/src/tarball/sqlite.tar.gz?r=release
	IF %ERRORLEVEL% NEQ 0 (
		echo Error Downloading Sqlite 
		exit /b %ERRORLEVEL%
	)
)

IF NOT EXIST "sqlite" (
   7z x sqlite.tar.gz -so | 7z x -aoa -si -ttar -o"sqlite"
   IF %ERRORLEVEL% NEQ 0 (
	  echo Error unzip sqlite 
	  exit /b %ERRORLEVEL%
   )
)

cd sqlite

IF NOT EXIST "build" (
	mkdir build
)

cd sqlite
xcopy /R /F /Y /E /S Makefile.msc ..\build\.
xcopy /R /F /Y /E /S version ..\build\.
cd ..

cd build

REM set PLATFORM=x86

call nmake /f Makefile.msc TOP=..\sqlite NO_TCL=1 PLATFORM=x86
rem call nmake /f ..\sqlite\Makefile.msc sqlite3.c TOP=..\sqlite
rem call nmake /f ..\sqlite\Makefile.msc sqlite3.dll TOP=..\sqlite
rem call nmake /f ..\sqlite\Makefile.msc sqlite3.exe TOP=..\sqlite
rem call nmake /f ..\sqlite\Makefile.msc test TOP=..\sqlite

goto noerror

:ExitError
echo build Failed

:noerror
echo build done

