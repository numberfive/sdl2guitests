/*
 * Copyright (C) 2019 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>
#include "InterfaceTest.h"
#include "../../common/plugin/Plugin.h"


CARNINE_PLUGIN(TestPlugin, "Test Plugin", "0.0.1")
        
TestPlugin::TestPlugin() {
  std::cout << "TestPlugin: Create" << std::endl;
}

TestPlugin::~TestPlugin() {
  std::cout << "TestPlugin: Destroy" << std::endl;
}

bool TestPlugin::onCommand (const char* node, const char* data, unsigned int size) {
    std::cout << "TestPlugin: Command: " << node << ": " << data << std::endl;  
    return true;
}