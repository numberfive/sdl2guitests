/*
 * Copyright (C) 2019 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   InterfaceTest.h
 * Author: punky
 *
 * Created on 15. Februar 2019, 11:09
 */

#ifndef INTERFACETEST_H
#define INTERFACETEST_H

#include "../../common/plugin/PluginInterface.h"

class TestPlugin : public PLUGIN::IPlugin {
  public:
    TestPlugin();
    virtual ~TestPlugin();

    bool onCommand(const char* node, const char* data, unsigned int size);

};

#endif /* INTERFACETEST_H */

