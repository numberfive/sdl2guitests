#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <iostream>
#include "../common/utils/easylogging++.h"
#include "../common/utils/LogNetDispatcher.hpp"
#include "../common/commonutils.h"

//#include "InterruptHandler.h"
#include "BackendService.h"

//https://stackoverflow.com/questions/34438357/cmake-one-build-directory-for-multiple-projects
//http://quabr.com/36648048/how-to-create-a-c-linux-program-for-multithreading
//https://github.com/ivanmejiarocha/micro-service/blob/master/source/main.cpp
//http://headerphile.com/sdl2/sdl2-part-13-multiplayer-tcp/
//https://github.com/wjwwood/serial

INITIALIZE_EASYLOGGINGPP

int main(int argc, char **argv)
{
    std::cout << "Starting Simulation Backend" << std::endl;
    START_EASYLOGGINGPP(argc, argv);
    
    if(utils::FileExists("loggerback.conf")) {
        // Load configuration from file
        el::Configurations conf("loggerback.conf");
        // Now all the loggers will use configuration from file and new loggers
        el::Loggers::setDefaultConfigurations(conf, true);
    }

    //nc -l -p 9090 -u
    el::Helpers::installLogDispatchCallback<LogNetDispatcher>("NetworkDispatcher");
    auto dispatcher = el::Helpers::logDispatchCallback<LogNetDispatcher>("NetworkDispatcher");
    dispatcher->setEnabled(true);
    dispatcher->updateServer("127.0.0.1", 9090, 8000);

    LOG(INFO) << "Starting Service";
    
    auto service = new BackendService();
    
    try {
        if(service->Start() == 0) {
            //InterruptHandler::waitForUserInterrupt();
            auto exitLoop = false;
            while(!exitLoop){
                std::cout << "Please enter your Command" << std::endl;
                std::string input = "";
                getline(std::cin, input);
                if(input == "end"){
                     std::cout << "Bye bye" << std::endl;
                     exitLoop = true;
                } else if(input == "exit") {
                     std::cout << "Bye bye" << std::endl;
                     exitLoop = true;
                } else if(input == "geo") {
                     std::cout << "Sending Geopos" << std::endl;
                     service->SendGeopos();
                } else if(input == "geo2") {
                     std::cout << "Sending Geopos2" << std::endl;
                     service->SendGeopos2();
                } else if(input == "geo3") {
                     std::cout << "Sending Geopos3" << std::endl;
                     service->SendGeopos3();
               } else if(input == "geo4") {
                    std::cout << "Sending Geopos4" << std::endl;
                    service->SendGeopos4();
               } else if(input == "geo5") {
                    std::cout << "Sending Geopos5" << std::endl;
                    service->SendGeopos5();
               } else if(input == "geof") {
                    std::cout << "Sending Geopos from file" << std::endl;
                    service->SendGeofile();
               } else if(input == "geof2") {
                    std::cout << "Sending Geopos from file2" << std::endl;
                    service->SendGeofile2();
               } else if(input == "upf") {
                    std::cout << "Send update File Signal" << std::endl;
                    service->SendUpdateMediaFile();
               } else if(input == "off") {
                    std::cout << "Send Poweroff Signal" << std::endl;
                    service->SendPowersupplyOff();
               } else if(input == "+") {
                    std::cout << "Send Volume Up Signal" << std::endl;
                    service->SendVolumeUp();
                } else if(input == "-") {
                    std::cout << "Send Volume Down Signal" << std::endl;
                    service->SendVolumeDown();
               } else {
                    std::cout << "unbekannter befehl" << std::endl;
               }
          }
        }
        LOG(INFO) << "Stopping Service";
        service->Stop();
    } catch(std::exception& e) {
        LOG(ERROR) << "Error ";
    }
    
    delete service;
    
    LOG(INFO) << "Service Stopped";
    
    return 0;
}
