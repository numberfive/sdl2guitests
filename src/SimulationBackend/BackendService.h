#pragma once

#include <thread>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>
#include "../common/ClientSocketConnection.h"
#include "../common/utils/json.hpp"

using json = nlohmann::json;

class BackendService
{
    IPaddress serverIp_;
    TCPsocket server_;
    bool run_;
    std::thread loop_thread_;
    std::vector<std::unique_ptr<ClientSocketConnection>> clientConnections_;
    SDLNet_SocketSet socketSet_;
    void Loop();
    void UpdateSocketSet();
    void IncomingMessage(const std::string& MessageName, json const& Message);
    void ReadGPSFile(std::ifstream& tourFile);
public:
    BackendService();
    ~BackendService();
    int Start();
    void Stop();
    void SendGeopos();
    void SendGeopos2();
    void SendGeopos3();
    void SendGeopos4();
    void SendGeopos5();
    void SendGeofile();
    void SendGeofile2();
    void SendUpdateMediaFile();
    void SendPowersupplyOff();
    void SendVolumeUp();
    void SendVolumeDown();
};

