#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "BackendService"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include <chrono>
#include <thread>
#include "../common/utils/easylogging++.h"
#include "../common/BackendMessages.h"
#include "../common/NMEADecoder.h"
#include <osmscout/GeoCoord.h>
#include "BackendService.h"

BackendService::BackendService() {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    socketSet_ = nullptr;
}

BackendService::~BackendService() {
    
}

int BackendService::Start() {
    if(SDL_Init(0)==-1) {
        LOG(ERROR) << SDL_GetError();
        return -1;
    }
    
    if(SDLNet_Init() == -1) {
        LOG(ERROR) << SDLNet_GetError();
        return -1;
    }
    
    LOG(INFO) << "Starting TCP";
    
    if(SDLNet_ResolveHost(&serverIp_,NULL,SERVERPORT)==-1) {
        LOG(ERROR) << "SDLNet_ResolveHost: " << SDLNet_GetError();
        return -1;
    }
    
    auto ipaddr=SDL_SwapBE32(serverIp_.host);
    
    printf("IP Address : %d.%d.%d.%d\n",
			ipaddr>>24,
			(ipaddr>>16)&0xff,
			(ipaddr>>8)&0xff,
			ipaddr&0xff);
            
    server_ = SDLNet_TCP_Open(&serverIp_);
    if(!server_) {
        LOG(ERROR) << "SDLNet_TCP_Open: " << SDLNet_GetError();
        return -1;
    }
    run_= true;
    
    loop_thread_ = std::thread(&BackendService::Loop, this);
    
    return 0;
}

void BackendService::Stop() {
    run_= false;
    
    //Unlock TCP Waiting Thread SDLNet_CheckSockets
    SDLNet_TCP_Close(server_);
     
    loop_thread_.join();
        
    SDLNet_Quit();
    SDL_Quit();
}

void BackendService::UpdateSocketSet() {
    if(socketSet_ != nullptr) {
        SDLNet_FreeSocketSet(socketSet_);
    }
    socketSet_ = SDLNet_AllocSocketSet(clientConnections_.size() + 1);
    SDLNet_TCP_AddSocket(socketSet_,server_);
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->AddToSocketSet(&socketSet_);
    }
}

void BackendService::IncomingMessage(const std::string& MessageName, json const& Message) {
    LOG(DEBUG) << "Incoming Message " << MessageName;
    if(MessageName == "ShutdownMessage") {
        std::raise(SIGTERM);
    }
    
    if(MessageName == "InitDoneMessage") {
         std::cout << "power on Hifi Amp" << std::endl;
    }

    if(MessageName == "SwitchAction") {
         SwitchAction action = Message;
         std::cout << "Schwitch chanced " << action.id << std::endl;

        SwitchEvent message;
        message.device = action.device;
        message.id = action.id;
        message.event = action.event;   

        for(const auto& clientConnection : clientConnections_) {
            clientConnection->Send(message);
        }
    }
}

void BackendService::Loop() {
    LOG(INFO) << "TCP thread is running";
    
    UpdateSocketSet();
    
    int numready = 0;
    while(run_) {
        numready = SDLNet_CheckSockets(socketSet_, (Uint32)-1);
        
        if(numready==-1){
            LOG(ERROR) << "SDLNet_CheckSockets: " << SDLNet_GetError();
            break;
        }
        
        if(numready == 0) continue;
        
        if(SDLNet_SocketReady(server_)) {
            numready--;
            auto sock = SDLNet_TCP_Accept(server_);
            if(sock) {
                LOG(INFO) << "New TCP Connect";
                std::unique_ptr<ClientSocketConnection> connection(new ClientSocketConnection(sock));
                clientConnections_.push_back(std::move(connection));
                UpdateSocketSet();
                auto clientConnection = --clientConnections_.end();
                auto newDatadelegate = std::bind(&BackendService::IncomingMessage, this, std::placeholders::_1, std::placeholders::_2);
                (*clientConnection)->SetNewDataCallback(newDatadelegate);
                (*clientConnection)->SendWelcomeMessage();
            }
        }
        
        if(numready == 0) continue;
        
        auto updateSocketSet = false;
        auto clientConnection = clientConnections_.begin();
        while(clientConnection != clientConnections_.end()) {
            auto needRead = false;
            if((*clientConnection)->CheckSocket(&needRead)) {
                if(needRead){
                    (*clientConnection)->Read();
                    if(!(*clientConnection)->IsConnected()){
                        clientConnection = clientConnections_.erase(clientConnection);
                        updateSocketSet = true;
                    } else {
                        ++clientConnection;
                    }
                } else {
                    ++clientConnection;
                }
            } else {
                clientConnection = clientConnections_.erase(clientConnection);
                updateSocketSet = true;
            }
        }
        if(updateSocketSet){
            UpdateSocketSet();
        }
    }
    SDLNet_FreeSocketSet(socketSet_);
    LOG(INFO) << "TCP thread is stopped";
}

void BackendService::ReadGPSFile(std::ifstream& tourFile) {
    std::string str;
    NMEADecoder decoder;
    osmscout::GeoCoord coordLast(-1,-1);
    long ignoreCount = 0;
    while (std::getline(tourFile, str)) {
		if(decoder.Decode(str)) {
			LOG(DEBUG) << "Line Read and decode " << str;
            if(decoder.IsPositionValid()) {
                auto now = std::chrono::system_clock::now();
                GPSMessage message;
                message.Latitude = decoder.GetLatitude();
                message.Longitude = decoder.GetLongitude();
                if (decoder.IsCompassValid()) {
                    message.Compass = decoder.GetCompass();
                }
                if (decoder.IsSpeedValid()) {
                    message.Speed = decoder.GetSpeed();
                }
                osmscout::GeoCoord coord(decoder.GetLatitude(), decoder.GetLongitude());
                if(coordLast.GetLat() == -1) {
                    if(clientConnections_.size() == 0) break;
                    for(const auto& clientConnection : clientConnections_) {
                        clientConnection->Send(message);
                    }
                    coordLast = coord;
                } else {
                    if (coord.GetLat() != coordLast.GetLat() || coord.GetLon() != coordLast.GetLon()) {
                        if(clientConnections_.size() == 0) break;
                        for(const auto& clientConnection : clientConnections_) {
                            clientConnection->Send(message);
                        }
                        coordLast = coord;
                        std::this_thread::sleep_until(now + std::chrono::milliseconds(500));
                    } else {
                        ignoreCount++;
                        std::cout << ignoreCount << " ignore position" << std::endl;
                    }
                }
            }
        }
    }
}

void BackendService::SendGeopos() {
    GPSMessage message;
    message.Latitude = 50.4117;
    message.Longitude = 9.3704;
    message.Compass = 32.80;
    message.Speed = 57.1342;
    
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->Send(message);
    }
}

void BackendService::SendGeopos4() {
    GPSMessage message;
    message.Latitude = 50.411835;
    message.Longitude = 9.37052;
    message.Compass = 32.95;
    message.Speed = 57.1342;
    
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->Send(message);
    }
}

void BackendService::SendGeopos2() {
    GPSMessage message;
    message.Latitude = 50.42182;
    message.Longitude = 9.41157;
    message.Compass = 163.22;
    message.Speed = 9.45;
    
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->Send(message);
    }
}

void BackendService::SendGeopos5() {
    GPSMessage message;
    message.Latitude = 50.4117;
    message.Longitude = 9.3704;
    message.Compass = 32.80;
    message.Speed = 57.1342;
    
    auto now = std::chrono::system_clock::now();

    for(const auto& clientConnection : clientConnections_) {
        clientConnection->Send(message);
    }

    std::this_thread::sleep_until(now + std::chrono::milliseconds(500));

    message.Latitude = 50.411835;
    message.Longitude = 9.37052;
    message.Compass = 32.95;
    message.Speed = 57.1342;
    
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->Send(message);
    }
}

void BackendService::SendGeopos3() {
    //A66 Steinau an der Strasse
    GPSMessage message;
    message.Latitude = 50.3162;
    message.Longitude = 9.4318;
    message.Compass = 163.22;
    message.Speed = 9.45;
    
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->Send(message);
    }
}

void BackendService::SendGeofile() {
    //std::ifstream file("F:/Austausch/libosmscout/GPS-Tegut-Tour.txt");
    std::ifstream file("/home/punky/develop/GPS-Tegut-Tour.txt");
    if(!file.is_open()) {
        LOG(ERROR) << "Open File ";
        std::cout << "Error Open File";
        return;
    }
    ReadGPSFile(file);
}

void BackendService::SendGeofile2() {
    std::ifstream file("F:/Austausch/libosmscout/GPS-Adnan-Tour.txt");
    //std::ifstream file("/home/punky/develop/GPS-Tegut-Tour.txt");
    if(!file.is_open()) {
        LOG(ERROR) << "Open File ";
        std::cout << "Error Open File";
        return;
    }
    ReadGPSFile(file);
}

void BackendService::SendUpdateMediaFile() {
    MediaFileUpdateMessage message;
        
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->Send(message);
    }
}

void BackendService::SendPowersupplyOff() {
    PowerSupplyOff message;
        
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->Send(message);
    }
}

void BackendService::SendVolumeUp() {
    VolumeUpdateMessage message;
    message.value = "1";
        
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->Send(message);
    }

}

void BackendService::SendVolumeDown() {
    VolumeUpdateMessage message;
    message.value = "-1";
        
    for(const auto& clientConnection : clientConnections_) {
        clientConnection->Send(message);
    }

}
    