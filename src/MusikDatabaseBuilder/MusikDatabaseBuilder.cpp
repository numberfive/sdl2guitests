// MusikDatabaseBuilder.cpp : Defines the entry point for the console application.
//
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "Main"
#endif

#include "stdafx.h"
#include "../common/commonutils.h"
#include <taglib/fileref.h>
#include <taglib/mpegfile.h>
#include <taglib/id3v2tag.h>
#include <taglib/attachedpictureframe.h>
#include "../common/utils/easylogging++.h"
#include "../common/database/DatabaseManager.h"
#include "../common/database/Statement.h"
#include "../common/database/SqliteException.h"
#include "../common/MediaFileFormat.h"
#include "../common/metadata_decoders/MP3MetadataDecoder.h"
#include "../common/metadata_decoders/FlacMetadataDecoder.h"
#include "../common/metadata_decoders/OggMetadataDecoder.h"
#include "../common/filesystem/recursive_directory_iterator.h"

#ifdef DEBUG
	#include "../../common/nvwa/debug_new.h"
#endif

el::base::type::StoragePointer sharedLoggingRepository();

//https://gist.github.com/guymac/1468279
//https://mail.kde.org/pipermail/taglib-devel/2009-November/001354.html
//https://stackoverflow.com/questions/6542465/c-taglib-cover-art-from-mpeg-4-files

const char ffmpegPath[] = "D:\\Mine\\OpenSource\\ffmpeg\\bin\\"; //Todo put it on config
bool convertWmaToMp3 = true; //Todo put it on config

std::string DoConvertWmaToMp3(const std::string& filename);

struct MusicMetadataDecoder_ {
    std::function<bool (std::string file) > canHandle;
    std::function<MediaTitleFileEntry(std::string fileName) > getMetadata;
};

MusicMetadataDecoder_ decoders[] = {
    {MP3MetadataDecoder::canHandle, MP3MetadataDecoder::getMetadata},
    {FlacMetadataDecoder::canHandle, FlacMetadataDecoder::getMetadata},
    {OggMetadataDecoder::canHandle, OggMetadataDecoder::getMetadata}
};

INITIALIZE_EASYLOGGINGPP

struct genretype{
	std::string name;
	long id;
};

struct artisttype {
	std::string name;
	long id;
};

struct songtype {
	long id;
	std::string name;
	int track;
	std::string image;
	int year;
	double duration;
	std::string file;
	std::string file2;
	std::string file3;
	std::string artist;
	std::string album;
	std::string albumImage;
};

struct FindName : std::unary_function<songtype, bool> {
	std::string name;
	explicit FindName(const std::string name) :name(name) { }
	bool operator()(songtype const& song) const {
		return song.name == name;
	}
};


int main(int argc, char* argv[])
{
	if(argc < 2) {
		printf("Please add search basepath and CarPC Mediadatebase lokation on command line MusikDatabaseBuilder <path to root dir>\n");
		return 0;
	}
	
	START_EASYLOGGINGPP(argc, argv);

	if(utils::FileExists("loggerbuilder.conf")) {
        // Load configuration from file
        el::Configurations conf("loggerback.conf");
        // Now all the loggers will use configuration from file and new loggers
        el::Loggers::setDefaultConfigurations(conf, true);
    }

	el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);

	std::vector<std::string> fileList;
	try
	{
		for (const std::string fileName : recursive_directory_iterator(argv[1])) {
			auto found = false;
			for (MusicMetadataDecoder_ decoder : decoders) {
				if (decoder.canHandle(fileName)) {
					//LOG(TRACE) << "This file is handleable.";
					fileList.push_back(fileName);
					found = true;
					break;
				}
			}

			if(!found && utils::hasEnding(fileName, ".wma") && convertWmaToMp3){
				LOG(TRACE) << "This file is handleable.";
				fileList.push_back(fileName);
			}
		}
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
		return -99;
	}

	if(fileList.size() == 0) {
		printf("path or no file found stoping\n");
		return 0;
	}

	std::vector<std::string> wmaFiles;
	std::vector<std::string> unknownFiles;
	std::vector<std::string> genres;
	std::vector<std::string> problemMusikFile;
	std::vector<genretype> genretyplist;
	std::vector<std::string> artists;
	std::vector<artisttype> artisttyplist;
	std::vector<songtype> songtypelist;

	auto filefoundCount = fileList.size();

	auto itFileName = fileList.begin();
	while  (itFileName != fileList.end()) {
		if (utils::hasEnding(*itFileName, ".wma")) {
			if(convertWmaToMp3) {
				auto newfile = DoConvertWmaToMp3(*itFileName);
				if(newfile.size() > 0) {
					itFileName = fileList.erase(itFileName);
					fileList.push_back(newfile);
				} else {
					wmaFiles.push_back(*itFileName);
					itFileName = fileList.erase(itFileName);
				}
			} else {
				wmaFiles.push_back(*itFileName);
				itFileName = fileList.erase(itFileName);
			}
			continue;
		}
		++itFileName;
	}

	MediaTitleFile sampleFile;

	for (auto fileName : fileList) {
		auto decoderFound = false;
		for (MusicMetadataDecoder_ decoder : decoders) {
            if (decoder.canHandle(fileName)) {
                LOG(DEBUG) << fileName << " this file is handleable.";
                sampleFile.Files.push_back(decoder.getMetadata(fileName));
				decoderFound = true;
				break;
            }
        }
		if(!decoderFound) {
			LOG(WARNING) << fileName << " this file is not handleable.";
		}
	}

	auto problemFound = false;
	auto secondFile = 0;
	for (auto sampleFileInfo : sampleFile.Files) {
		if (sampleFileInfo.Name.size() == 0 || sampleFileInfo.Name == sampleFileInfo.MediaFile) {
			std::cout << "title is empty " << std::endl;
			problemMusikFile.push_back(sampleFileInfo.MediaFile);
			continue;
		}
		if (sampleFileInfo.Genre.size() == 0) {
			std::cout << "Genre is empty " << std::endl;
			problemMusikFile.push_back(sampleFileInfo.MediaFile);
			continue;
		}
		if (sampleFileInfo.Artist.size() == 0) {
			std::cout << "artist is empty " << std::endl;
			problemMusikFile.push_back(sampleFileInfo.MediaFile);
			continue;
		}

		if (std::find(genres.begin(), genres.end(), sampleFileInfo.Genre) == genres.end()) {
			genres.push_back(sampleFileInfo.Genre);
		}
		if (std::find(artists.begin(), artists.end(), sampleFileInfo.Artist) == artists.end()) {
			artists.push_back(sampleFileInfo.Artist);
		}

		songtype data;
		data.name = sampleFileInfo.Name;
		data.file = sampleFileInfo.MediaFile.substr(strlen(argv[1]));
		data.track = sampleFileInfo.track;
		data.year = sampleFileInfo.year;
		data.duration = sampleFileInfo.duration;
		data.artist = sampleFileInfo.Artist;
		data.album = sampleFileInfo.Album;
		data.image = sampleFileInfo.ImageFile;
		
		auto const pos = data.file.find_last_of(DIRCHAR);
		std::string path = argv[1];
		path += data.file.substr(0, pos + 1);
		path += "Folder.jpg";
		if(utils::FileExists(path)) {
			cimg_library::CImg<int32_t> image;
			image.load_jpeg(path.c_str());
			image.resize(32, 32);
			auto newFileName = data.file.substr(pos + 1);
			newFileName.replace(newFileName.end()-4,newFileName.end(),"XXXXXX");
			newFileName = "/tmp/" + newFileName;
			newFileName += ".jpg";
			int fd = mkstemps(&newFileName[0],4);
			if (fd == -1) {
				LOG(ERROR) << "Failed to create temporary file '" << newFileName << "' with error:" << strerror(errno);
			} else {
				close(fd);
				data.albumImage = newFileName;
				if(data.image.size() == 0) {
					data.image = newFileName;
				}
				image.save_jpeg(newFileName.c_str());
			}
		}
		
		auto songData = std::find_if(songtypelist.begin(), songtypelist.end(), FindName(data.name));
		if (songData != songtypelist.end()) {
			printf("'%s' found twice\n", data.name.c_str());
			if(songData->artist == data.artist) {
				printf("%s i think same song two versions\n", data.name.c_str());
				if(songData->file2.size() == 0) {
					songData->file2 = data.file;
					secondFile++;
				} else {
					if(songData->file3.size() == 0) {
						songData->file3 = data.file;
						secondFile++;
					} else {
						printf("%s %s 4 Versions ? !?!?\n", data.name.c_str(), data.artist.c_str());
						problemFound = true;
						problemMusikFile.push_back(sampleFileInfo.MediaFile);
					}
				}
				
			} else {
				printf("'%s' '%s' '%s' '%s' same song two Artist\n", data.name.c_str(), data.artist.c_str(), songData->name.c_str(), songData->artist.c_str());
				auto newSongName = data.name + " (" + data.artist + ")";
				auto songData2 = std::find_if(songtypelist.begin(), songtypelist.end(), FindName(newSongName));
				if(songData2 == songtypelist.end()) {
					data.name = newSongName;
					songtypelist.push_back(data);
				} else {
					auto newSongName = data.name + " (" + songData->artist.c_str() + ")";
					auto songData2 = std::find_if(songtypelist.begin(), songtypelist.end(), FindName(newSongName));
					if(songData2 == songtypelist.end()) {
						data.name = newSongName;
						songtypelist.push_back(data);
					} else {
						printf("%s is in list not know wath we can do with this\n", newSongName.c_str());
						problemFound = true;
						problemMusikFile.push_back(sampleFileInfo.MediaFile);
					}
				}
			}
		}
		else
		{
			songtypelist.push_back(data);
		}
	}
		
	for (auto genre : genres) {
		printf("%s\n", genre.c_str());
	}

	if(problemMusikFile.size() > 0) {
		printf("Problem Files:\n");
		for (auto problemFile : problemMusikFile) {
			printf("%s\n", problemFile.c_str());
		}
	}

	std::cout << "Statistik:" << std::endl;
	std::cout << std::to_string(filefoundCount) << " Files Found" << std::endl;
	std::cout << std::to_string(sampleFile.Files.size()) << " Files with Mediadata" << std::endl;
	std::cout << std::to_string(wmaFiles.size()) << " Files wma" << std::endl;
	std::cout << std::to_string(unknownFiles.size()) << " Files unknown" << std::endl;
	auto filesCount = songtypelist.size() + secondFile;
	std::cout << std::to_string(songtypelist.size()) << " Songs can used with " << std::to_string(filesCount) << " Files" << std::endl;
	std::cout << std::to_string(problemMusikFile.size()) << " Files problem" << std::endl;
	
	///home/punky/.local/share/MiNeSoftware/CarNiNe
	std::cout << "Home: " << utils::GetHomePath() << std::endl;
	auto databasePath = utils::GetHomePath();
	if(databasePath.back() != DIRCHAR){
		databasePath += DIRCHAR;
	}

	if(!problemFound) {
		databasePath += ".local/share/MiNeSoftware/CarNiNe/";
		auto databaseFile = databasePath + "CarPC.db";
		if(utils::FileExists(databaseFile)) { 
			std::cout << "Import to " << databaseFile << " ?" << std::endl;
			char c = getchar();
			if(c == 'y') {
				//Todo make C++ like label is bad code i know
				goto importDatabase;
			}
		}
	}
	
	//unlink/remove temp files
	for (const auto songtype : songtypelist) {
		if(remove(songtype.image.c_str()) != 0){
			unlink(songtype.image.c_str());
		}
	}

	return 0;

importDatabase:
	auto databaseManager = new DatabaseManager(databasePath);
	databaseManager->Init();

	try
	{
		for (auto genre : genres) {
			auto id = databaseManager->GetIdFromStringKey("GENRE", "NAME", genre.c_str());
			if (id == -1) {
				auto strSQL = databaseManager->PrepareSQL("INSERT INTO GENRE (NAME) VALUES('%s')", genre.c_str());
				databaseManager->DoDml(strSQL);
				id = databaseManager->GetIdFromStringKey("GENRE", "NAME", genre.c_str());
			}
			genretype data;
			data.name = genre;
			data.id = id;
			genretyplist.push_back(data);
		}

		for (auto artist : artists) {
			auto id = databaseManager->GetIdFromStringKey("ARTIST", "NAME", artist.c_str());
			if (id == -1) {
				auto strSQL = databaseManager->PrepareSQL("INSERT INTO ARTIST (NAME) VALUES('%s')", artist.c_str());
				databaseManager->DoDml(strSQL);
				id = databaseManager->GetIdFromStringKey("ARTIST", "NAME", artist.c_str());
			}
			artisttype data;
			data.name = artist;
			data.id = id;
			artisttyplist.push_back(data);
		}

		std::string strSQL;
		for (const auto songtype : songtypelist) {
			auto songId = databaseManager->GetIdFromStringKey("SONG", "NAME", songtype.name.c_str());
			if (songId == -1) {
				//Song is not found
				strSQL = databaseManager->PrepareSQL("INSERT INTO SONG (NAME, TRACK, IMAGE, YEAR, DURATION) VALUES('%s', %d, '%s', %d, %lf)", songtype.name.c_str(), songtype.track, utils::getFileName(songtype.image).c_str(), songtype.year, songtype.duration);
				databaseManager->DoDml(strSQL);

				songId = databaseManager->GetIdFromStringKey("SONG", "NAME", songtype.name.c_str());
			} else {
				if(songtype.image.size() > 0) {
					strSQL = databaseManager->PrepareSQL("UPDATE SONG SET IMAGE = '%s' WHERE ID = %d", utils::getFileName(songtype.image).c_str(), songId);
					databaseManager->DoDml(strSQL);
					databaseManager->SaveSmallImageDatabase(songtype.image);
				}
			}
			
			auto sourceId = databaseManager->GetIdFromStringKey("MEDIASOURCE", "SOURCE", songtype.file.c_str());
			if(sourceId == -1) {
				strSQL = databaseManager->PrepareSQL("INSERT INTO MEDIASOURCE (SOURCETYPE,SOURCE,HASH) VALUES('FILE', '%s', 'undef')", songtype.file.c_str());
				databaseManager->DoDml(strSQL);
				sourceId = databaseManager->GetIdFromStringKey("MEDIASOURCE", "SOURCE", songtype.file.c_str());
				strSQL = databaseManager->PrepareSQL("INSERT INTO SONG_MEDIASOURCE (SONGID,MEDIASOURCEID,ORDERNR) VALUES(%d, %d, 0)", songId, sourceId);
				databaseManager->DoDml(strSQL);
			}
						
			if(songtype.file2.size() > 0) {
				//to solve the Problem one Song is on more than one Album
				auto secondSourceId = databaseManager->GetIdFromStringKey("MEDIASOURCE", "SOURCE", songtype.file2.c_str());
				if (secondSourceId == -1) {
					strSQL = databaseManager->PrepareSQL("INSERT INTO MEDIASOURCE (SOURCETYPE,SOURCE,HASH) VALUES('FILE', '%s', 'undef')", songtype.file2.c_str());
					databaseManager->DoDml(strSQL);
					secondSourceId = databaseManager->GetIdFromStringKey("MEDIASOURCE", "SOURCE", songtype.file2.c_str());
					strSQL = databaseManager->PrepareSQL("INSERT INTO SONG_MEDIASOURCE (SONGID,MEDIASOURCEID,ORDERNR) VALUES(%d, %d, 1)", songId, secondSourceId);
					databaseManager->DoDml(strSQL);
				}
			}

			if(songtype.file3.size() > 0) {
				//to solve the Problem one Song is on more than one Album
				auto secondSourceId = databaseManager->GetIdFromStringKey("MEDIASOURCE", "SOURCE", songtype.file3.c_str());
				if (secondSourceId == -1) {
					strSQL = databaseManager->PrepareSQL("INSERT INTO MEDIASOURCE (SOURCETYPE,SOURCE,HASH) VALUES('FILE', '%s', 'undef')", songtype.file3.c_str());
					databaseManager->DoDml(strSQL);
					secondSourceId = databaseManager->GetIdFromStringKey("MEDIASOURCE", "SOURCE", songtype.file3.c_str());
					strSQL = databaseManager->PrepareSQL("INSERT INTO SONG_MEDIASOURCE (SONGID,MEDIASOURCEID,ORDERNR) VALUES(%d, %d, 2)", songId, secondSourceId);
					databaseManager->DoDml(strSQL);
				}
			}

			if (songtype.artist.size() > 0) {
				const auto artistId = databaseManager->GetIdFromStringKey("ARTIST", "NAME", songtype.artist.c_str());
				if (artistId > -1) {
					auto found = false;
					strSQL = databaseManager->PrepareSQL("select SONGID from SONG_ARTIST where songId = %d and artistId = %d and ORDERNR = 0 ", songId, artistId);
					auto query = databaseManager->CreateQuery(strSQL);
					if (query->Eof()) {
						delete query;
					} else {
						if (query->Execute() > 0) {
							found = true;
							delete query;
						}
					}
					if (!found)	{
						strSQL = databaseManager->PrepareSQL("INSERT INTO SONG_ARTIST (SONGID,ARTISTID,ORDERNR) VALUES (%d, %d, 0)", songId, artistId);
						databaseManager->DoDml(strSQL);
					}
				}
			}

			if (songtype.album.size() > 0) {
				auto albumId = databaseManager->GetIdFromStringKey("ALBUM", "NAME", songtype.album.c_str());
				if (albumId == -1) {
					strSQL = databaseManager->PrepareSQL("INSERT INTO ALBUM (NAME,YEAR) VALUES('%s', %d)", songtype.album.c_str(), songtype.year);
					databaseManager->DoDml(strSQL);
					albumId = databaseManager->GetIdFromStringKey("ALBUM", "NAME", songtype.album.c_str());
				}

				if (songtype.albumImage.size() > 0) {
					strSQL = databaseManager->PrepareSQL("UPDATE ALBUM SET IMAGE = '%s' WHERE ID = %d", utils::getFileName(songtype.albumImage).c_str(), albumId);
					databaseManager->DoDml(strSQL);
					databaseManager->SaveSmallImageDatabase(songtype.albumImage);
				}

				auto count = databaseManager->GetCountFromTable("ALBUM_SONG", "ALBUMID",  std::to_string(albumId), "SONGID", std::to_string(songId));
				if(count == 0) {
					strSQL = databaseManager->PrepareSQL("INSERT INTO ALBUM_SONG (ALBUMID, SONGID) VALUES (%d, %d)", albumId, songId);
					databaseManager->DoDml(strSQL);
				}
			}
		}
		strSQL = databaseManager->PrepareSQL("delete from IMAGE32X32 where name not in (select image from song union select image from album)");
		databaseManager->DoDml(strSQL);
	}
	catch(SqliteException& exp) {
		printf("Error %s\n", exp.what());
	}
	databaseManager->Deinit();
	delete databaseManager;

	return 0;
}

std::string DoConvertWmaToMp3(const std::string& filename) {
	auto filenameFrom = filename;
	auto filenameTo = filename;
	filenameTo.erase(filenameTo.end() - 3, filenameTo.end());
	filenameTo += "mp3";

	std::cout << filenameTo << std::endl;

	const char commandLinePlatern[] = "%sffmpeg -i \"%s\" -write_id3v1 1 \"%s\"";
	char commandLine[2048];

	sprintf(commandLine, commandLinePlatern, ffmpegPath, filenameFrom.c_str(), filenameTo.c_str());
	int code;
	auto consoleOutput = utils::exec(commandLine, code);
	
	std::cout << consoleOutput << std::endl;

	if(code != 0) {
		return "";
	}
	remove(filenameFrom.c_str());

	return filenameTo;
}
