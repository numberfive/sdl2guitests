// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#define TAGLIB_STATIC
#define __STDC_WANT_LIB_EXT1__ 1

#ifdef _MSC_VER
    #define cimg_use_jpeg
    #define cimg_display 0
#endif

#ifdef _MSC_VER
    #include "targetver.h"
#endif

#include <stdio.h>
#ifdef _MSC_VER
    #include <tchar.h>
#endif
#include <string>
#include <iostream>

//Todo: I am not happy with this
#if defined(__GNUC__)
    #define UNUSED(x) ((void)(x))
#else
    #define UNUSED(x) UNREFERENCED_PARAMETER(x) 
#endif


#include "../common/gsl-lite.hpp"
#include "../../Externals/CImg/CImg.h"
