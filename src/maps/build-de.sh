#!/bin/bash

# wget exits with code 8 on "429 Too Many Requests" todo implement this in Script
# https://www.gnu.org/software/wget/manual/html_node/Exit-Status.html

CONTINENT=europe
COUNTRY=germany
srtmver=2.1
downloadmap=no
BASEDIR=$PWD
OSMSCOUTDIR=/usr/local/bin/

#1sec
RESOLUTION=1
STEP=20
CAT=400,100

#3sec
STEP=100
CAT=1000,500
RESOLUTION=3


mkdir -p tmp
mkdir -p $CONTINENT
mkdir -p $CONTINENT/$COUNTRY

wget "http://download.geofabrik.de/$CONTINENT/$COUNTRY-latest.osm.pbf.md5" -O$CONTINENT/$COUNTRY/$COUNTRY-latest.osm.pbf.md5
exitCode=$?

if [ $exitCode -ne 0 ] ; then
   echo "Error"
   exit $exitCode
fi

file1=$CONTINENT/$COUNTRY/$COUNTRY-latest.osm.pbf.md5
file2=$CONTINENT/$COUNTRY/current.md5

if [ ! -f $CONTINENT/$COUNTRY/current.md5 ] ; then
   echo "$CONTINENT/$COUNTRY/current.md5 not found!"
   downloadmap=yes
else
   if ! cmp "$file1" "$file2"; then
       echo "md5 changed download new"
       downloadmap=yes
   fi
fi

echo $downloadmap

if [ $downloadmap == "yes" ] ; then
   echo "try download"
   wget "http://download.geofabrik.de/$CONTINENT/$COUNTRY.poly" -O$CONTINENT/$COUNTRY/$COUNTRY.poly
   exitCode=$?
   if [ $exitCode -ne 0 ] ; then
      echo "Error"
      exit $exitCode
   fi
   wget "http://download.geofabrik.de/$CONTINENT/$COUNTRY-latest.osm.pbf" -O$CONTINENT/$COUNTRY/$COUNTRY-latest.osm.pbf
   exitCode=$?
   if [ $exitCode -ne 0 ] ; then
      echo "Error"
      exit $exitCode
   fi
   md5sum < $CONTINENT/$COUNTRY/$COUNTRY-latest.osm.pbf| cut -c 1-32 > $file2
   truncate --size -1 $file2 # Todo find better way to remove the new line
   printf "  $COUNTRY-latest.osm.pbf\n" >> $file2
   if ! cmp --silent "$file1" "$file2"; then
       echo "md5 wrong download failed abort script"
       exit 99
   fi
   echo "map download done"
else
   echo "map already up to date"
fi

echo "begin generate contour lines"

#todo understand all this parameter
#--pbf makes osm.pdf files but we don't how many files phyghtmap generate so we use osmconver for One File

echo "starting phyghtmap"

phyghtmap --polygon=$CONTINENT/$COUNTRY/$COUNTRY.poly \
          --srtm-version=$srtmver --no-zero-contour --output-prefix=tmp/$CONTINENT-$COUNTRY \
          --max-nodes-per-tile=0 \
          --max-nodes-per-way=300 \
          --start-node-id=$(( 1 << 33 )) --start-way-id=$(( 1 << 33 )) \
          --srtm=$RESOLUTION \
          --viewfinder-mask=$RESOLUTION \
          --source=view$RESOLUTION,srtm$RESOLUTION --void-range-max=-500 \
          --step=$STEP --line-cat=$CAT

exitCode=$?
if [ $exitCode -ne 0 ] ; then
   echo "phyghtmap give an Error"
   exit $exitCode
fi

echo "starting osmconvert"

RESTEXT=$RESOLUTION
RESTEXT+="sec"

osmconvert \
  --verbose --statistics \
  tmp/$CONTINENT-$COUNTRY*.osm \
  -B=$CONTINENT/$COUNTRY/$COUNTRY.poly \
  -o=$CONTINENT/$COUNTRY/$COUNTRY-contours-$RESTEXT.osm.pbf

exitCode=$?
if [ $exitCode -ne 0 ] ; then
   echo "osmconvert give an Error"
   exit $exitCode
fi

echo "deleting tmp files"

rm tmp/$CONTINENT-$COUNTRY*.osm

time nice $OSMSCOUTDIR/Import \
   -d \
   --eco true \
   --typefile $BASEDIR/map.ost \
   --rawCoordBlockSize $(( 60 * 1000000 )) \
   --rawWayBlockSize $(( 4 * 1000000 )) \
   --altLangOrder en \
   --destinationDirectory "$CONTINENT/$COUNTRY" \
   --bounding-polygon $CONTINENT/$COUNTRY/$COUNTRY.poly \
   $IMPORT_ARGS \
   $BASEDIR/$CONTINENT/$COUNTRY/$COUNTRY-latest.osm.pbf \
   $BASEDIR/$CONTINENT/$COUNTRY/$COUNTRY-contours-$RESTEXT.osm.pbf \
   2>&1 | tee "$BASEDIR/$CONTINENT/$COUNTRY/import.log"

