#pragma once

#include <string>
#include "../common/utils/json.hpp"
using json = nlohmann::json;

class WorkerMessage
{
public:
    WorkerMessage();
    ~WorkerMessage();
    std::string _messageType;
    std::string _messageData;
    json _messageJson;
};

