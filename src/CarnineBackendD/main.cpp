
//https://github.com/wmark/systemd-transparent-udp-forwarderd/blob/master/udp-proxy.c

#include <cstdlib>
#include <cerrno>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h> 
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <systemd/sd-daemon.h>
#include <systemd/sd-journal.h>
#include <systemd/sd-event.h>
#include <SDL.h>
#include <SDL_net.h>
#include <iostream>

#include "PowerSupplySerial.h"
#include "NmeaSerial.h"
#include "ServiceConfig.h"
#include "WorkerMessage.h"
#include "ServerSocketInternal.h"
#include "../common/NMEADecoder.h"
#include "../common/BackendMessages.h"
#include "../common/utils/easylogging++.h"
#include "../common/utils/LogNetDispatcher.hpp"
#include "../common/commonutils.h"
#include "USBHotplugNotifier.h"
#include "../common/MediaFileFormat.h"
#include "filesystem/FilesystemUtils.h"
#include "system/SystemInfo.h"
#include "remotecontrol/RemoteControl.h"
#include "../common/ConfigErrorException.h"
#include "../common/NullPointerException.h"

static PowerSupplySerial* portPowerSupply = nullptr;
static NmeaSerial* portNmeaSerial = nullptr;
static ServerSocketInternal* serverSocketInternal = nullptr;
static RemoteControl* remoteControl = nullptr;
static SystemInfo* systemInfo = nullptr;
static ServiceConfig* config;
static NMEADecoder* decoder = nullptr;
static long lastTemp = 0;
static USBHotplugNotifier* usb = nullptr;

double lastLatitude = -1;
double lastLongitude = -1;


static void checkSystem() {
    const auto currentTemp = systemInfo->GetCpuTemp();
    if(lastTemp != currentTemp) {
        LOG(INFO) << "CPU Temp " << currentTemp;
        lastTemp = currentTemp;
        systemInfo->CheckFan();
    }
}

//Time Callback called every ?
static const uint64_t stats_every_usec = 10 * 1000000;
/* These counters are reset in display_stats(). */
static size_t received_counter = 0, sent_counter = 0;

/* display_stats is expected to be called by the event loop. */
static int display_stats(sd_event_source *es, uint64_t now, void *userdata) {
  
    sd_notifyf(false, "STATUS=%zu telegrams send in the last %d seconds.", sent_counter, (unsigned int)(stats_every_usec / 1000000));
    sd_notifyf(false, "STATUS=%zu telegrams received in the last %d seconds.", received_counter, (unsigned int)(stats_every_usec / 1000000));
    
    checkSystem();
    
    sd_event_source_set_time(es, now + stats_every_usec); /* reschedules */
    sent_counter = received_counter = 0;
    
    return 0;
}

static int pipe_receive(sd_event_source *es, int fd, uint32_t revents, void *userdata){
    ++received_counter;
    
    WorkerMessage* message = nullptr;
    if(read(fd, &message, sizeof(message)) != sizeof(message)) {
        LOG(ERROR) << "Read error on pipe";
        sd_journal_print(LOG_ERR, "Read error on pipe");
    } else {
        LOG(DEBUG) << "Worker message " << message->_messageType;
        sd_journal_print(LOG_DEBUG, "Worker message %s", message->_messageType.c_str());
        if(message->_messageType == "PowerSupplyOff") {
            PowerSupplyOff messageSend;
            serverSocketInternal->SendAll(messageSend);
            usb->SetInShutdown();
            ++sent_counter;
        } else if(message->_messageType == "ShutdownSystem") {
            if(portPowerSupply != nullptr) {
                //Todo add an Function to portPowerSupply
                portPowerSupply->Write("6"); //Amp off
                if(message->_messageData == "true") {
                    portPowerSupply->ShutdownSystem();
                } else {
                    portPowerSupply->Write("!"); //enter Service mode disable Watchdog
                }
            }
        } else if(message->_messageType == "PowerOnAmp") {
            if(portPowerSupply != nullptr) {
                portPowerSupply->PowerOnAmp();
            }
            if(systemInfo != nullptr) {
                systemInfo->SendIoStates();
            }
        } else if(message->_messageType == "NMEAData") {
            if(decoder->Decode(message->_messageData)) {
                if(decoder->IsPositionValid()) {
                    GPSMessage gpsMessage;
                    gpsMessage.Latitude = decoder->GetLatitude();
                    gpsMessage.Longitude = decoder->GetLongitude();
                    if (decoder->IsCompassValid()) {
                        gpsMessage.Compass = decoder->GetCompass();
                    }
                    if (decoder->IsSpeedValid()) {
                        gpsMessage.Speed = decoder->GetSpeed();
                    }
                    if(lastLatitude != gpsMessage.Latitude || lastLongitude != gpsMessage.Longitude) {
                        serverSocketInternal->SendAll(gpsMessage);
                        ++sent_counter;
                        lastLatitude = gpsMessage.Latitude;
                        lastLongitude = gpsMessage.Longitude;
                    }
                }
            }
        } else if (message->_messageType == "DRIVE ADDED"){
            // I'm not sure I understand the need to use this function to send message through the socket (the callback would suffice)

            MediaTitleFile sampleFile = listMusicFilesFrom(message->_messageData);

            LOG(INFO) << "Found " << sampleFile.Files.size() << " music files on the new USB drive.";

            // Write the file to disk
            std::string filename(config->GetAppDataPath () + "playlist.json");
            LOG(INFO) << "Try to write " << filename;
            
            std::ofstream o(filename);
            const json jConfig = sampleFile;
            o << std::setw(4) << jConfig << std::endl;
            o.close();

            serverSocketInternal->SendAll(nlohmann::json{
                {"Ver", BACKENDPROTOCOLVER},
                {"MessageType", message->_messageType},
                {"path", message->_messageData}
            });
            ++sent_counter;

            serverSocketInternal->SendAll(MediaFileUpdateMessage());
        } else if (message->_messageType == "DRIVE REMOVED"){
            std::string filename(config->GetAppDataPath () + "playlist.json");
            LOG(INFO) << "Try to write " << filename;
            
            std::ofstream o(filename);
            const json jConfig = MediaTitleFile();
            o << std::setw(4) << jConfig << std::endl;
            o.close();

            serverSocketInternal->SendAll(nlohmann::json{
                {"Ver", BACKENDPROTOCOLVER},
                {"MessageType", message->_messageType},
                {"path", message->_messageData}
            });
            ++sent_counter;

            serverSocketInternal->SendAll(MediaFileUpdateMessage());
        } else if (message->_messageType == "RemoteConnected"){
            RemoteControlMessage messageSend;
            messageSend.info = "add";
            serverSocketInternal->SendAll(messageSend);
            ++sent_counter;
        } else if (message->_messageType == "VolumeUpdate"){
            VolumeUpdateMessage messageSend;
            messageSend.value = message->_messageData;
            serverSocketInternal->SendAll(messageSend);
            ++sent_counter;
        } else if (message->_messageType == "CpuTempUpdate"){
            CpuTempUpdate messageSend;
            messageSend.value = message->_messageData;
            serverSocketInternal->SendAll(messageSend);
            ++sent_counter;
        } else if(message->_messageType == "JsonMessageIn") {
            auto baseMessage = json::parse(message->_messageData);
            BaseMessage messageJson = baseMessage;
            if(messageJson.MessageType == "SwitchAction") {
                SwitchAction action = baseMessage;
                if(systemInfo != nullptr) {
                    systemInfo->DoSwitchAction(action);
                }
            }
            else if(messageJson.MessageType == "SendIoStates") {
                if(systemInfo != nullptr) {
                    systemInfo->SendIoStates();
                }
            }
        } else if(message->_messageType == "JsonMessageOut") {
            serverSocketInternal->SendAll(message->_messageJson);
        } else {
            LOG(WARNING) << "Message not handeld";
        }
        delete message;
    }
    
    return 0;
}

INITIALIZE_EASYLOGGINGPP

static sd_event *event = nullptr;

void handleUserInterrupt(int sig) {
    LOG(DEBUG) << "handleUserInterrupt " << std::to_string(sig);
    if(sig == SIGINT || sig == SIGABRT) {
        auto const result = sd_event_exit(event, 99);
        std::cout << "sd_event_exit " << result << std::endl;
    } else if(sig == SIGTERM) {
        usb->SetInShutdown();
        auto const result = sd_event_exit(event, 0);
        std::cout << "sd_event_exit " << result << std::endl;
    }
  
}

int main(int argc, char **argv)
{
    int exit_code = EXIT_SUCCESS;
    sd_event_source *event_source = nullptr;
    sd_event_source *timer_source = nullptr;
    uint64_t now;
    int eventLoopResult;
    int functionResult;
    
    int pipefd[2];
    std::thread* threadUSB = nullptr;
    
    signal(SIGINT, handleUserInterrupt);
    signal(SIGTERM, handleUserInterrupt);
    signal(SIGABRT, handleUserInterrupt);
    
    START_EASYLOGGINGPP(argc, argv);
    el::Helpers::setThreadName("Backend Main");

    std::string argv_str(argv[0]);
    std::string base = argv_str.substr(0, argv_str.find_last_of("/"));

    if(utils::FileExists(base+"/loggerback.conf")) {
        // Load configuration from file
        el::Configurations conf(base+"/loggerback.conf");
        // Now all the loggers will use configuration from file and new loggers
        el::Loggers::setDefaultConfigurations(conf, true);
    } else {
        sd_journal_print(LOG_WARNING, "Logger Config not Found");
    }

    if(argc < 2) {
        sd_journal_print(LOG_ERR, "Missing Commanline Parameter Configfile");
        LOG(ERROR) << "Missing Commandline Parameter Configfile";
        std::cout << "Missing Commandline Parameter Configfile" << std::endl;
        exit_code = -6;
        goto finish;
    } else {
        sd_journal_print(LOG_INFO, "Using Config File %s", argv[1]);
        LOG(INFO) << "Using Config File" << argv[1];
        config = new ServiceConfig(argv[1]);
        config->Load();

        if(!config->GetUdpLogServer().empty()) {
            el::Helpers::installLogDispatchCallback<LogNetDispatcher>("NetworkDispatcher");
            auto dispatcher = el::Helpers::logDispatchCallback<LogNetDispatcher>("NetworkDispatcher");
            dispatcher->setEnabled(true);
            dispatcher->updateServer(config->GetUdpLogServer(), 9090, 8001);
        }
    }
        
    if(sd_event_default(&event) < 0) {
        sd_journal_print(LOG_ERR, "Cannot instantiate the event loop.");
        exit_code = 201;
        goto finish;
    }
    
    sigset_t ss;
    if(sigemptyset(&ss) < 0) {
        exit_code = errno;
        goto finish;
    }
    
    if(sigaddset(&ss, SIGTERM) < 0) {
        exit_code = errno;
        goto finish;
    }
    
    if(sigaddset(&ss, SIGINT) < 0) {
        exit_code = errno;
        goto finish;
    }
    
    if(sigaddset(&ss, SIGHUP) < 0) {
        exit_code = errno;
        goto finish;
    }
    
    if (sigprocmask(SIG_BLOCK, &ss, NULL) < 0) {
        exit_code = errno;
        goto finish;
    }

    functionResult = sd_event_add_signal(event, NULL, SIGTERM, NULL, NULL);
    if( functionResult < 0) {
        exit_code = 202;
        sd_journal_print(LOG_ERR, "Cannot add_signal SIGTERM");
        goto finish;
    }
    
    if(sd_event_add_signal(event, NULL, SIGINT, NULL, NULL) < 0) {
        sd_journal_print(LOG_ERR, "Cannot add_signal SIGINT");
        exit_code = 203;
        goto finish;
    }
    
    functionResult = sd_event_add_signal(event, NULL, SIGHUP, NULL, NULL);
    if( functionResult < 0) {
        sd_journal_print(LOG_ERR, "Cannot add_signal SIGHUP %d", functionResult);
        exit_code = 204;
        goto finish;
    }
    
    /* Genrate an Timer for Stats */
    sd_event_now(event, CLOCK_MONOTONIC, &now);
    sd_event_add_time(event, &timer_source, CLOCK_MONOTONIC, now + stats_every_usec, 0, display_stats, NULL);

    functionResult = sd_event_source_set_enabled(timer_source, SD_EVENT_ON);
    if( functionResult < 0) {
        sd_journal_print(LOG_ERR, "Cannot enabled timer %d", functionResult);
    }
    
    functionResult = pipe2(pipefd, O_CLOEXEC|O_NONBLOCK);
    if ( functionResult != 0) {
        sd_journal_print(LOG_ERR, "Cannot create the pipe %d", functionResult);
    }
    
    functionResult = sd_event_add_io(event, &event_source, pipefd[0], EPOLLIN, pipe_receive, nullptr);
    if ( functionResult < 0) {
        (void) sd_journal_print(LOG_CRIT, "event_add_io failed for pipe no: %d", functionResult);
        exit_code = 72;
        goto finish;
    }

    serverSocketInternal = new ServerSocketInternal();
    if(serverSocketInternal->Start(pipefd[1]) < 0) {
        sd_journal_print(LOG_ERR, "Failed Start serverSocketInternal");
    }
    
    if(config->PowerSupplyDevice().size() > 0) {
        portPowerSupply = new PowerSupplySerial(config->PowerSupplyDevice());
        if(!portPowerSupply->Open(B38400)) {
            sd_journal_print(LOG_ERR, "Failed open port power supply");
            LOG(ERROR) << "Failed open port power supply";
        } else {
            /* register new data on port sd_event_add_io work only with soket ?*/
            functionResult = portPowerSupply->RegisterCallBack(pipefd[1]);
            if(functionResult == 0) {
                sd_journal_print(LOG_DEBUG, "CallBack Registered");
                LOG(INFO) << "CallBack Registered";
            } else {
                sd_journal_print(LOG_ERR, "Cannot Register CallBack %d", functionResult);
                LOG(ERROR) << "Cannot Register CallBack " << std::to_string(functionResult);
            }
        }
    }
    
    decoder = new NMEADecoder();
    
    if(config->NmeaDevice().size() > 0) {
        portNmeaSerial = new NmeaSerial(config->NmeaDevice());
        if(!portNmeaSerial->Open(B4800)) {
            sd_journal_print(LOG_ERR, "Failed open port nmea device");
        } else {
            portNmeaSerial->SetNMEARawFile(config->NmeaFile());
            /* register new data on port sd_event_add_io work only with soket ?*/
            functionResult = portNmeaSerial->RegisterCallBack(pipefd[1]);
            if(functionResult == 0) {
                sd_journal_print(LOG_DEBUG, "CallBack Registered");
            } else {
                sd_journal_print(LOG_ERR, "Cannot Register CallBack %d", functionResult);
            }
        }
    }
    
    if(config->GetRemoteBroadcastPort() > 0 ) {
        sd_journal_print(LOG_INFO, "Remote Control is enabled");
        LOG(INFO) << "try to start RemoteControl CertFile " << config->CertFile();
        remoteControl = new RemoteControl(config->GetRemoteBroadcastPort(), config->GetRemoteSslPort());
        remoteControl->Init(config->CertFile(), config->KeyFile(), pipefd[1]);
    }

    usb = new USBHotplugNotifier(); 
    // Register the USB hotplug listener
    usb->onDriveAdded([&pipefd](std::string mountPoint){
        LOG(INFO) << "New drive mounted on " << mountPoint;
        WorkerMessage* message = new WorkerMessage();
        message->_messageType = "DRIVE ADDED";
        message->_messageData = mountPoint;
        int rc = write(pipefd[1], &message, sizeof (message));
        if (rc != sizeof (message)){
            sd_journal_print(LOG_ERR, "Error when notifying addition of drive on '%s'. Writing to pipe failed: %s.", strerror(errno), mountPoint.c_str());
        }
    });
    usb->onDriveRemoved([&pipefd](std::string mountPoint){
        LOG(INFO) << "Drive removed from " << mountPoint;
        WorkerMessage* message = new WorkerMessage();
        message->_messageType = "DRIVE REMOVED";
        message->_messageData = mountPoint;
        int rc = write(pipefd[1], &message, sizeof (message));
        if (rc != sizeof (message)){
            sd_journal_print(LOG_ERR, "Error when notifying removal of drive on '%s'. Writing to pipe failed: %s.", strerror(errno), mountPoint.c_str());
        }
    });
    
    // TODO: Find a way to notify new clients of all mounted drives.
    threadUSB = new std::thread([](){
        usb->Loop ();
    });

    systemInfo = new SystemInfo(pipefd[1]);
    try {
        systemInfo->Init(config);
    } catch(ConfigErrorException const& exp) {
        sd_journal_print(LOG_ERR, exp.what());
        exit_code = 210;
        goto finish;
    } catch(NullPointerException const& exp) {
        sd_journal_print(LOG_ERR, exp.what());
        exit_code = 211;
        goto finish;
    }
    
    sd_journal_print(LOG_INFO, "Written by M. Nenninger http://www.carnine.de");
    sd_journal_print(LOG_INFO, "Done setting everything up. Serving.");

    config->Save();
    
    sd_notify(false, "READY=1\n" "STATUS=Up and running.");

    LOG(INFO) << "Up and running.";
    
    eventLoopResult = sd_event_loop(event);
    if (eventLoopResult < 0) {
        sd_journal_print(LOG_ERR, "Failure: %s\n", strerror(-eventLoopResult));
        exit_code = -eventLoopResult;
    }
    
    LOG(INFO) << "After loop begin shutdown";
    
finish:
    if(systemInfo != nullptr) {
        delete systemInfo;
    }
    if(threadUSB != nullptr) {
        usb->Shutdown();
        threadUSB->join();
        delete threadUSB;
        delete usb;
    }

    if(portPowerSupply != nullptr){
        portPowerSupply->Close();
        delete portPowerSupply;
    }
    
    if(portNmeaSerial != nullptr){
        portNmeaSerial->Close();
        delete portNmeaSerial;
    }
   
    if(remoteControl != nullptr){
        remoteControl->Deinit();
        delete remoteControl;
    }
      
    if(decoder != nullptr){
        delete decoder;
    }

    if (timer_source != NULL) {
        sd_event_source_set_enabled(timer_source, SD_EVENT_OFF);
        timer_source = sd_event_source_unref(timer_source);
    }
    
    sd_journal_print(LOG_DEBUG, "Freeing references to event-source and the event-loop.");
    event_source = sd_event_source_unref(event_source);
    event = sd_event_unref(event);

    close(pipefd[0]);
    close(pipefd[1]);
    
    return exit_code;
}
