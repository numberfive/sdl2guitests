#pragma once
#include "MNSerial.h"

class PowerSupplySerial : public MNSerial
{
    bool _shutdownInWork;
    void AnalyseBuffer(int count) override;
    void HandleSystemState(char value);
    void HandleCommandResult(char value);
    void HandleExternVoltage(char* value);
    void HandlePiAlive(char* value);
    void HandleKlemme15(char value);
    void PowerOffSystemdBus(int sec);
public:
    PowerSupplySerial(const std::string& portname);
    ~PowerSupplySerial();
    void ShutdownSystem();
    void PowerOnAmp();
};

