#pragma once

#include <string>

struct ConfigFile {
    std::string powerSupplyDevice;
    std::string nemaDevice;
    int32_t tcpPort;
    int32_t remoteBroadcastPort_;
    int32_t remoteSslPort_;
    bool hardwareVolumeControl_;
    std::string _certFile;
    std::string _keyFile;
    std::string _nmeaFileName;
    unsigned char _mcpDeviceAddr1;
    std::string _udpLogServer;
};

class ServiceConfig
{
    std::string appDataPath_;
    ConfigFile configFile_;
    std::string filenameConfig_;
public:
    ServiceConfig(std::string filename);
    ~ServiceConfig();
    
    void Load();
    void Save();
    std::string PowerSupplyDevice() const;
    std::string NmeaDevice() const;
    std::string GetAppDataPath() const;
    int32_t GetRemoteBroadcastPort() const;
    int32_t GetRemoteSslPort() const;
    bool GetHardwareVolumeControl() const;
    std::string CertFile() const;
    std::string KeyFile() const;
    std::string NmeaFile() const;
    unsigned char GetMcpDeviceAddr1() const;
    std::string GetUdpLogServer() const;
};

