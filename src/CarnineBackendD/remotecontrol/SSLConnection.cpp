/*
 * Copyright (C) 2018 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   SSLConnection.cpp
 * Author: punky
 * 
 * Created on 29. November 2018, 05:21
 */

#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "SSLConnection"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include <iostream>
#include <unistd.h>     // UNIX standard function definitions
#include "../WorkerMessage.h"
#include "../../common/utils/easylogging++.h"
#include "SSLConnection.h"

static const char *cpByeMessage = "ByeBye\n"; 
static const char *cpLoginMessage = "Login\n";
static const char *cpLoginResponse = "Pass?\n"; 
static const char *cpPassMessage = "Pass:roboter\n";
static const char *cpPassResponse = "Pass:Ok\n";
    
SSLConnection::SSLConnection(SSL* ssl, int pipefdwrite) {
  el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
  _ssl = ssl;
  _pipefdwrite = pipefdwrite;
}

bool SSLConnection::Servlet() {
   char buf[1024] = {0};
   
    int bytes;
     
    if ( SSL_accept(_ssl) < 0 ) {     /* do SSL-protocol accept this only work on block sockts*/
        ERR_print_errors_fp(stderr);
        return false;
    }

    //ShowCerts(ssl);        /* get any certificates */
    bytes = SSL_read(_ssl, buf, sizeof(buf)); /* get request */
    buf[bytes] = '\0';

    //Todo Make Save it is not garantie get message in one run.
    printf("Client msg: \"%s\"\n", buf);

    if ( bytes > 0 )
    {
        if(strcmp(cpLoginMessage,buf) == 0){
            SSL_write(_ssl, cpLoginResponse, strlen(cpLoginResponse)); /* send reply */
        }
        else if(strcmp(cpPassMessage,buf) == 0){
            SSL_write(_ssl, cpPassResponse, strlen(cpPassResponse)); /* send reply */
            SendWorkerMessage("RemoteConnected", "");
        }
        else if(strcmp(cpByeMessage,buf) == 0){
            return false;
        }
        else
        {
            SSL_write(_ssl, "Invalid Message", strlen("Invalid Message")); /* send reply */
        }
    }
    else
    {
        ERR_print_errors_fp(stderr);
        return false;
    }
    //sd = SSL_get_fd(ssl);       /* get socket connection */
    //SSL_free(ssl);         /* release SSL state */
    //close(sd);          /* close connection */
    
    return true;
}

void SSLConnection::SendWorkerMessage(const std::string& type, const std::string& data) {
    auto message = new WorkerMessage();
    message->_messageType = type;
    message->_messageData = data;
    int rc = write(_pipefdwrite, &message, sizeof(message));
    if(rc != sizeof(message)) {
        LOG(ERROR) << "error writing pipe %s" << strerror(errno);
    }
}

SSLConnection::~SSLConnection() {
}

