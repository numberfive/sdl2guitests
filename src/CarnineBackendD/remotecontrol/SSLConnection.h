/*
 * Copyright (C) 2018 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   SSLConnection.h
 * Author: punky
 *
 * Created on 29. November 2018, 05:21
 */

#ifndef SSLCONNECTION_H
#define SSLCONNECTION_H

#include <openssl/ssl.h>
#include <openssl/err.h>

class SSLConnection {
    SSL* _ssl;
    int _pipefdwrite;
    void SendWorkerMessage(const std::string& type, const std::string& data);
public:
    SSLConnection(SSL* ssl, int pipefdwrite);
    SSLConnection(const SSLConnection& orig) = delete;
    SSLConnection(SSLConnection&& other) = delete;
    SSLConnection& operator=(const SSLConnection& other) = delete;
    SSLConnection& operator=(SSLConnection&& other) = delete;
    virtual ~SSLConnection();
    bool Servlet();

};

#endif /* SSLCONNECTION_H */

