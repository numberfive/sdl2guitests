/*
 * Copyright (C) 2018 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   RemoteControll.h
 * Author: punky
 *
 * Created on 27. Oktober 2018, 06:29
 */

#include <arpa/inet.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <openssl/ssl.h>

#ifndef REMOTECONTROL_H
#define REMOTECONTROL_H

class RemoteControl {
  int _pipefdwrite;
public:
    RemoteControl(int remoteBroadcastPort, int remoteSslPort);
    virtual ~RemoteControl();
    
    bool Init(const std::string& certFile, const std::string& keyFile, int pipefdwrite);
    void Deinit();
private:
    void BroadcastLoop();
    void SslLoop();
    void GetLocalIp();
    int remoteBroadcastPort_;
    int remoteSslPort_;
    int receiverBSocket_;
    int receiverSslSocket_;
    bool _loopRun;
    std::thread loop_thread_;
    std::thread loop_thread_ssl_;
    char myIp_[INET_ADDRSTRLEN + 1];
    SSL_CTX* sslCtx_;
};

#endif /* REMOTECONTROLL_H */

