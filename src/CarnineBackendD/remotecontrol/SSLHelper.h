/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SSLHelper.h
 * Author: punky
 *
 * Created on 1. September 2018, 07:14
 */

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <string.h>

#ifndef SSLHELPER_H
#define SSLHELPER_H

SSL_CTX* InitServerCTX(void);
void LoadCertificates(SSL_CTX* ctx, const char* CertFile, const char* KeyFile);

#endif /* SSLHELPER_H */

