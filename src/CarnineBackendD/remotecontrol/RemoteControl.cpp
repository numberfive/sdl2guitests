/*
 * Copyright (C) 2018 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   RemoteControll.cpp
 * Author: punky
 * 
 * Created on 27. Oktober 2018, 06:29
 */

#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "RemoteControl"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "../../common/utils/easylogging++.h"
#include "RemoteControl.h"
#include "SSLHelper.h"
#include "SSLConnection.h"

RemoteControl::RemoteControl(int remoteBroadcastPort, int remoteSslPort) {
  el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
  remoteBroadcastPort_ = remoteBroadcastPort;
  remoteSslPort_ = remoteSslPort;
  myIp_[0] = 0;
}

int OpenListener(int port)
{   int sd;
    struct sockaddr_in addr;
 
    sd = socket(PF_INET, SOCK_STREAM, 0);
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0 )
    {
        perror("can't bind port");
        abort();
    }
    if ( listen(sd, 10) != 0 )
    {
        perror("Can't configure listening port");
        abort();
    }
    return sd;
}

bool RemoteControl::Init(const std::string& certFile, const std::string& keyFile, int pipefdwrite) {
  _pipefdwrite = pipefdwrite;
  receiverBSocket_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if(receiverBSocket_ < 1) {
    return false;
  }
  
  GetLocalIp();
  if(strlen(myIp_) == 0){
      return false;
  }
  
  int broadcast=1;
  setsockopt(receiverBSocket_, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof broadcast);
  
  if(SSL_library_init() < 0) {
    LOG(ERROR) << "SSL Init Failed";
    return false;
  }
  
  sslCtx_ = InitServerCTX();        /* initialize SSL */
  LoadCertificates(sslCtx_, certFile.c_str(), keyFile.c_str()); /* load certs */
  receiverSslSocket_ = OpenListener(remoteSslPort_);    /* create server socket */
    
  _loopRun = true;
  loop_thread_ = std::thread(&RemoteControl::BroadcastLoop, this);
  loop_thread_ssl_ = std::thread(&RemoteControl::SslLoop, this);
  
  return true;
}

void RemoteControl::BroadcastLoop() {
    LOG(INFO) << "BroadcastLoop thread is running";
    
    sockaddr_in si_me, si_other;
  
    memset(&si_me, 0, sizeof(si_me));
    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(remoteBroadcastPort_);
    si_me.sin_addr.s_addr = INADDR_ANY;
    
    bind(receiverBSocket_, (sockaddr *)&si_me, sizeof(sockaddr));
    
    while(_loopRun) {
        char buf[512];
        unsigned slen=sizeof(sockaddr);
        const auto resultCount = recvfrom(receiverBSocket_, buf, sizeof(buf)-1, 0, (sockaddr *)&si_other, &slen);
        if(resultCount > 0) {
            buf[resultCount] = 0;
            LOG(DEBUG) << buf;
            
            if(strcmp(buf, "\u0002Where is CarNiNe\u0003") == 0) {
                LOG(DEBUG) << "Where Message Get";
                snprintf(buf, 512, "\u0002Here:%s:%d\u0003", myIp_, remoteSslPort_);
                si_other.sin_port = htons(remoteBroadcastPort_);
                if(sendto(receiverBSocket_, buf, strlen(buf), 0, (sockaddr *)&si_other, slen) == (ssize_t)strlen(buf)) {
                    LOG(DEBUG) << "The Answer " << buf;
                }
            }
        } else {
            auto const lasterror = errno;
            LOG(ERROR) << "Error Code " << lasterror << " " << strerror(lasterror);
        }
        
    }
    
    LOG(DEBUG) << "BroadcastLoop done";
}

void RemoteControl::GetLocalIp() {
    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;
    void * tmpAddrPtr=NULL;
    bool first = true;
    getifaddrs(&ifAddrStruct);

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (!ifa->ifa_addr) {
            continue;
        }
        if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
            // is a valid IP4 Address
            tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            LOG(DEBUG) << ifa->ifa_name << " IP V4 "  << addressBuffer;
            if(first && strcmp(ifa->ifa_name, "lo") != 0 ) {
                first = false;
                strncpy(myIp_, addressBuffer, INET_ADDRSTRLEN + 1);
            }
        } else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
            // is a valid IP6 Address
            tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
            char addressBuffer[INET6_ADDRSTRLEN];
            inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
            LOG(DEBUG) << ifa->ifa_name << " IP V6 "  << addressBuffer; 
        } 
    }
    if (ifAddrStruct!=NULL) freeifaddrs(ifAddrStruct);
}

void RemoteControl::SslLoop() {
  //This Code Allows Only one Connection Think about this
  while (_loopRun)
    {   struct sockaddr_in addr;
        socklen_t len = sizeof(addr);
        SSL *ssl;
 
        LOG(DEBUG) << "Server is runnig waiting for connect";
        
        int client = accept(receiverSslSocket_, (struct sockaddr*)&addr, &len);  /* accept connection as usual */
        if(client > -1) {
            LOG(DEBUG) << "Connection: " << inet_ntoa(addr.sin_addr) << ":" << ntohs(addr.sin_port);

            ssl = SSL_new(sslCtx_);       /* get new SSL state with context */
            SSL_set_fd(ssl, client);      /* set connection socket to SSL state */

            //Todo Put in Thread to Allow more than one connection / return Errormessg if to many
            auto connection = new SSLConnection(ssl, _pipefdwrite);
            auto result = true;
            do {
                result = connection->Servlet();         /* service connection */
            }while(result);

            delete connection;
            
            SSL_free(ssl);
            shutdown(client, SHUT_RDWR);
            close(client);
        } else {
            auto const lasterror = errno;
            LOG(ERROR) << "Error Code " << lasterror << " " << strerror(lasterror);
        }
    }
  
    LOG(DEBUG) << "SslLoop done";
}

void RemoteControl::Deinit() {
    LOG(DEBUG) << "Shutdown RemoteConnections";
    _loopRun = false;
    
    shutdown(receiverBSocket_, SHUT_RDWR);
    shutdown(receiverSslSocket_, SHUT_RDWR);
    
    close(receiverBSocket_);
    close(receiverSslSocket_);
    
    loop_thread_.join();
    loop_thread_ssl_.join();
    
    LOG(DEBUG) << "Shutdown RemoteConnections done";
}

RemoteControl::~RemoteControl() {
}

