#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "ServiceConfig"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include <fstream>
#include <SDL.h>
#include <systemd/sd-journal.h>
#include <iomanip>

#include "ServiceConfig.h"
#include "../common/utils/json.hpp"
#include "../common/commonutils.h"
#include "../common/utils/easylogging++.h"

using json = nlohmann::json;

void to_json(json& j, const ConfigFile& p) {
	j = json{
		{ "PowerSupplyDevice", p.powerSupplyDevice },
        { "NemaDevice", p.nemaDevice },
        { "TCPPort" , p.tcpPort},
        { "RemoteBroadcastPort" , p.remoteBroadcastPort_},
        { "RemoteSslPort" , p.remoteSslPort_},
        { "HardwareVolumeControl" , p.hardwareVolumeControl_},
        { "CertFile" , p._certFile},
        { "KeyFile" , p._keyFile},
        { "NMEAFileName" , p._nmeaFileName},
        { "McpDeviceAddr1" , p._mcpDeviceAddr1},
        { "UdpLogServer" , p._udpLogServer}
	};
}

void from_json(const json& j, ConfigFile& p) {
	p.powerSupplyDevice = j.at("PowerSupplyDevice").get<std::string>();
	p.nemaDevice = j.at("NemaDevice").get<std::string>();
	p.tcpPort = j.at("TCPPort").get<int32_t>();
    p.remoteBroadcastPort_ = j.at("RemoteBroadcastPort").get<int32_t>();
    p.remoteSslPort_ = j.at("RemoteSslPort").get<int32_t>();
    p.hardwareVolumeControl_ = j.at("HardwareVolumeControl").get<bool>();
    p._certFile = j.at("CertFile").get<std::string>();
    p._keyFile = j.at("KeyFile").get<std::string>();
    p._nmeaFileName = j.at("NMEAFileName").get<std::string>();
    p._mcpDeviceAddr1 = j.at("McpDeviceAddr1").get<unsigned char>();
    const auto it_valueUdpLogServer = j.find("UdpLogServer");
    if (it_valueUdpLogServer != j.end()) {
        p._udpLogServer = j.at("UdpLogServer").get<std::string>();
    } else {
        p._udpLogServer = "";
    }
}

ServiceConfig::ServiceConfig(std::string filename) {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    filenameConfig_ = filename;
}

ServiceConfig::~ServiceConfig() {
}

void ServiceConfig::Load() {
    if (utils::FileExists(filenameConfig_)) {
        try {
            std::ifstream ifs(filenameConfig_);
            const auto jConfig = json::parse(ifs);
            configFile_ = jConfig;
            ifs.close();
        }
        catch (std::domain_error& exp) {
            LOG(ERROR) << "Loadconfig  " << exp.what();
            sd_journal_print(LOG_ERR, "Loadconfig %s", exp.what());
        }
        catch (std::exception& exp) {
            LOG(ERROR) << "Loadconfig  " << exp.what();
            sd_journal_print(LOG_ERR, "Loadconfig %s", exp.what());
        }
    } else {
        sd_journal_print(LOG_DEBUG, "Create new config file");
        configFile_.powerSupplyDevice = "/dev/ttyAMA0";
        configFile_.nemaDevice = "/dev/ttyUSB0";
        configFile_.tcpPort = 5022;
        configFile_.remoteBroadcastPort_ = 6000;
        configFile_.remoteSslPort_ = 8080;
        configFile_.hardwareVolumeControl_ = true;
        configFile_._nmeaFileName = "";
        configFile_._mcpDeviceAddr1 = 0x00;
        std::ofstream o(filenameConfig_);
        const json jConfig = configFile_;
        o << std::setw(4) << jConfig << std::endl;
        o.close();
    }
    
    auto basePath = SDL_GetPrefPath("MiNeSoftware", "CarNiNe");
    if (basePath) {
        appDataPath_ = basePath;
        SDL_free(basePath);
    }
}

void ServiceConfig::Save() {
    std::ofstream o(filenameConfig_);
    const json jConfig = configFile_;
    o << std::setw(4) << jConfig << std::endl;
    o.close();
}

std::string ServiceConfig::PowerSupplyDevice() const {
    return configFile_.powerSupplyDevice;
}

std::string ServiceConfig::NmeaDevice() const {
    return configFile_.nemaDevice;
}

std::string ServiceConfig::GetAppDataPath() const {
    return appDataPath_;
}

int32_t ServiceConfig::GetRemoteBroadcastPort() const {
    return configFile_.remoteBroadcastPort_;
}

int32_t ServiceConfig::GetRemoteSslPort() const {
    return configFile_.remoteSslPort_;
}

bool ServiceConfig::GetHardwareVolumeControl() const {
    return configFile_.hardwareVolumeControl_;
}

std::string ServiceConfig::CertFile() const {
    return configFile_._certFile;
}

std::string ServiceConfig::KeyFile() const {
    return configFile_._keyFile;
}

std::string ServiceConfig::NmeaFile() const {
    return configFile_._nmeaFileName;
}

unsigned char ServiceConfig::GetMcpDeviceAddr1() const {
    return configFile_._mcpDeviceAddr1;
}

std::string ServiceConfig::GetUdpLogServer() const {
    return configFile_._udpLogServer;
}