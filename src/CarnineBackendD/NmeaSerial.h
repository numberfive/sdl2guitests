#pragma once
#include "MNSerial.h"
#include <fstream>

class NmeaSerial : public MNSerial
{
    std::ofstream _nmeaFile;
    std::string _nmeaFileName;
    void AnalyseBuffer(int count) override;
public:
    NmeaSerial(const std::string& portname);
    ~NmeaSerial();
    void SetNMEARawFile(const std::string& fileName);
};

