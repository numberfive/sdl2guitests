#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "NmeaSerial"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include <string>
#include <istream>
#include "NmeaSerial.h"
#include <systemd/sd-journal.h>
#include "../common/utils/easylogging++.h"

NmeaSerial::NmeaSerial(const std::string& portname):
    MNSerial(portname) {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    _nmeaFileName = "";
}

NmeaSerial::~NmeaSerial()
{
  if(_nmeaFile.is_open()) {
      _nmeaFile.close();
  }
}

void NmeaSerial::AnalyseBuffer(int count) {
    currentReadPos_ += count;
    auto lastpos = 0;
    for(auto pos = 0; pos < currentReadPos_; pos++) {
        if(readBuffer_[pos] == '\n') { //New Line Found Text Message
            if(pos-1 > 0) {
                std::string received(readBuffer_, lastpos, pos- 1 - lastpos);
                LOG(DEBUG) << received << " from serial";
                if(!_nmeaFileName.empty()) {
                    if(!_nmeaFile.is_open ()) {
                        std::ofstream nmeaFile(_nmeaFileName, std::ios::out | std::ios::app);
                    }
                    _nmeaFile << received;
                }
                SendWorkerMessage("NMEAData", received);
            }
            lastpos = pos + 1;
        }
    }
    if(lastpos > 0) {
        if(lastpos == currentReadPos_) {
            memset(readBuffer_, 0, MAXBUFFERSIZE);
            currentReadPos_ = 0;
        } else {
            memcpy(&readBuffer_[0], &readBuffer_[lastpos], currentReadPos_ - lastpos);
            currentReadPos_ = currentReadPos_ - lastpos;
            memset(&readBuffer_[currentReadPos_], 0, MAXBUFFERSIZE - currentReadPos_);
            LOG(DEBUG) << currentReadPos_ << " Bytes in buffer";
        }
    }
}

void NmeaSerial::SetNMEARawFile(const std::string& fileName) {
  _nmeaFileName = fileName;
}
