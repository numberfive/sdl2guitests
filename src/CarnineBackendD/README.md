# CarNine backend

This backend needs to be run along the front-end and is an interface to the
hardware and other programs.

## Dependencies

`udisks2 taglib libssl`

sudo apt-get install libudisks2-dev libtag1-dev libssl-dev

## USB Hotplugging

This backends relies on udisks2 to detect new USB Mass Storage devices and mount
them. To allow it to request udisks to mount the new drive, it needs to have
permission. Here is a sample permission file that can be put in
/etc/polkit-1/rules.d/XX-storage-mount.rules to allow any user in the storage
group to call udisks to mount drives.

```javascript
polkit.addRule(function(action, subject) {
  var YES = polkit.Result.YES;
  // NOTE: there must be a comma at the end of each line except for the last:
  var permission = {
    // required for udisks1:
    "org.freedesktop.udisks.filesystem-mount": YES,
    "org.freedesktop.udisks.luks-unlock": YES,
    "org.freedesktop.udisks.drive-eject": YES,
    "org.freedesktop.udisks.drive-detach": YES,
    // required for udisks2:
    "org.freedesktop.udisks2.filesystem-mount": YES,
    "org.freedesktop.udisks2.encrypted-unlock": YES,
    "org.freedesktop.udisks2.eject-media": YES,
    "org.freedesktop.udisks2.power-off-drive": YES,
    // required for udisks2 if using udiskie from another seat (e.g. systemd):
    "org.freedesktop.udisks2.filesystem-mount-other-seat": YES,
    "org.freedesktop.udisks2.filesystem-unmount-others": YES,
    "org.freedesktop.udisks2.encrypted-unlock-other-seat": YES,
    "org.freedesktop.udisks2.eject-media-other-seat": YES,
    "org.freedesktop.udisks2.power-off-drive-other-seat": YES
  };
  if (subject.isInGroup("storage")) {
    return permission[action.id];
  }
});
```

## Sync Raspberry to Desktop (Crosscompile)
cd /home/punky/x-tools/rpiroot
rsync -rl --delete-after --safe-links --ignore-errors pi@192.168.2.50:/{lib,usr} .
cd /home/punky/x-tools/rpiroot/opt/vc/
rsync -rl --delete-after --safe-links --ignore-errors pi@192.168.2.50:/{opt/vc/lib,opt/vc/include} .

## Testing
pscp -batch -pw "raspberry" CarnineBackendD.bin pi@192.168.2.50:/home/pi/devtest/

