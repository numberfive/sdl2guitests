#include "FilesystemUtils.h"

#include "../../common/utils/easylogging++.h"
#include "../../common/filesystem/recursive_directory_iterator.h"

#include "../../common/metadata_decoders/MP3MetadataDecoder.h"
#include "../../common/metadata_decoders/FlacMetadataDecoder.h"
#include "../../common/metadata_decoders/OggMetadataDecoder.h"

// TODO: This shouldn't be in the filesystem directory...

struct MusicMetadataDecoder_ {
    std::function<bool (std::string file) > canHandle;
    std::function<MediaTitleFileEntry(std::string fileName) > getMetadata;
};

MusicMetadataDecoder_ decoders[] = {
    {MP3MetadataDecoder::canHandle, MP3MetadataDecoder::getMetadata},
    {FlacMetadataDecoder::canHandle, FlacMetadataDecoder::getMetadata},
    {OggMetadataDecoder::canHandle, OggMetadataDecoder::getMetadata}
};

MediaTitleFile listMusicFilesFrom(std::string path) {
    LOG(TRACE) << "Listing music files in '" << path << "'";
    MediaTitleFile sampleFile;
    sampleFile.List = "USB";

    for (std::string fileName : recursive_directory_iterator(path)) {
        LOG(TRACE) << "Saw file '" << fileName << "'";
        for (MusicMetadataDecoder_ decoder : decoders) {
            if (decoder.canHandle(fileName)) {
                LOG(TRACE) << "This file is handleable.";
                sampleFile.Files.push_back(decoder.getMetadata(fileName));
            }
        }
    }

    return sampleFile;
}