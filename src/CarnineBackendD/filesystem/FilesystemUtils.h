#ifndef FILESYSTEMUTILS_H
#define FILESYSTEMUTILS_H

#include "../../common/MediaFileFormat.h"

MediaTitleFile listMusicFilesFrom(std::string path);

#endif /* FILESYSTEMUTILS_H */

