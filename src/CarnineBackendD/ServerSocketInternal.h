#ifndef SERVERSOCKETINTERNAL_H
#define SERVERSOCKETINTERNAL_H

#include <thread>
#include <vector>
#include "../common/ClientSocketConnection.h"
#include "../common/utils/json.hpp"

using json = nlohmann::json;

class ServerSocketInternal
{
    IPaddress serverIp_;
    TCPsocket server_;
    bool run_;
    std::thread loop_thread_;
    std::vector<std::unique_ptr<ClientSocketConnection>> clientConnections_;
    SDLNet_SocketSet socketSet_;
    int pipefdwrite_;
    
    void Loop();
    void UpdateSocketSet();
    void IncomingMessage(const std::string& MessageName, json const& Message);
    void SendWorkerMessage(const std::string& type);
    void SendWorkerMessage(const std::string& type, const bool value);
    void SendWorkerMessage(const std::string& type, const char* value);
    void SendWorkerMessage(const std::string& type, const std::string& data);
public:
    ServerSocketInternal();
    ~ServerSocketInternal();

    int Start(int pipefdwrite);
    void SendAll(json const& message);
    
};

#endif // SERVERSOCKETINTERNAL_H
