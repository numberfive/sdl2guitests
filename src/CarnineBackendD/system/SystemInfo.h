/*
 * Copyright (C) 2018 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   SystemInfo.h
 * Author: punky
 *
 * Created on 28. Oktober 2018, 17:21
 */

#pragma once
#ifndef SYSTEMINFO_H
#define SYSTEMINFO_H

#include "../../common/BackendMessages.h"

class ServiceConfig;
class GpioPin;
class SoftwarePWM;
class I2CBus;
class MCP23017;

class SystemInfo {
    int _pipefdwrite;
    const ServiceConfig* _config;
    GpioPin* _firstVolumePin;   
    GpioPin* _secondVolumePin;
    GpioPin* _pwmPin;
    SoftwarePWM* _pwmGenerator;
    std::string _firstVolumePinName;
    std::string _secondVolumePinName;
    std::string _fanPwmPinName;
    std::string _lastTempText;
    std::chrono::system_clock::time_point _startup;
    bool _firstVolumePinState;
    I2CBus* _i2cBus;
    MCP23017* _mcp1;
    void PinChange(const std::string& port, const int& value);
    void SendWorkerMessage(const std::string& type, const std::string& data);
    void SendWorkerMessage(const std::string& type, json const& data);
public:
    explicit SystemInfo(int pipefdwrite);   
    virtual ~SystemInfo();
    double GetCpuTemp();
    void Init(const ServiceConfig* config);
    void CheckFan();
    void DoSwitchAction(SwitchAction action);
    void SendIoStates();
};

#endif /* SYSTEMINFO_H */

