/*
 * Copyright (C) 2018 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   SystemInfo.cpp
 * Author: punky
 * 
 * Created on 28. Oktober 2018, 17:21
 */

#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "SystemInfo"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include <unistd.h>     // UNIX standard function definitions
#include <string.h>
#include "GpioPin.h"
#include "SoftwarePWM.h"
#include "../ServiceConfig.h"
#include "../../common/utils/easylogging++.h"
#include "I2CBus.h"
#include "MCP23017.h"
#include "../WorkerMessage.h"
#include <systemd/sd-journal.h>
#include "SystemInfo.h"

void SystemInfo::PinChange(const std::string& port, const int& value) {
  LOG(DEBUG) << port << " has value " << value;
  
  if(port == _firstVolumePinName ) {
      if(value == 1) {
          _firstVolumePinState = true;
      } else {
          _firstVolumePinState = false;
      }
  }
  
  if(port == _secondVolumePinName ) {
      auto volumeValue = 0;
      if(value == 1) {
          if(_firstVolumePinState) {
            volumeValue++;
          } else {
            volumeValue--;
          }
      } else {
          if(!_firstVolumePinState) {
            volumeValue++;
          } else {
            volumeValue--;
          }
      }
      
      if(volumeValue != 0) {
          //Send Volume Info to GUI
          LOG(DEBUG) << " VolumeUpdate " << std::to_string(volumeValue);
          SendWorkerMessage("VolumeUpdate", std::to_string(volumeValue));
      }
  }
  
}

/**
 * @brief SendWorkerMessage 
 * @param string Type
 * @param string Data
 */
void SystemInfo::SendWorkerMessage(const std::string& type, const std::string& data) {
    auto message = new WorkerMessage();
    message->_messageType = type;
    message->_messageData = data;
    int rc = write(_pipefdwrite, &message, sizeof(message));
    if(rc != sizeof(message)) {
        sd_journal_print(LOG_ERR, "error writing pipe %s", strerror(errno));
        LOG(ERROR) << "error writing pipe" << strerror(errno);
    }
}

void SystemInfo::SendWorkerMessage(const std::string& type, json const& data) {
    auto message = new WorkerMessage();
    message->_messageType = type;
    message->_messageJson = data;
    int rc = write(_pipefdwrite, &message, sizeof(message));
    if(rc != sizeof(message)) {
        sd_journal_print(LOG_ERR, "error writing pipe %s", strerror(errno));
        LOG(ERROR) << "error writing pipe" << strerror(errno);
    }
}

SystemInfo::SystemInfo(int pipefdwrite) {
  el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
  _firstVolumePin = nullptr;
  _secondVolumePin = nullptr;
  _pwmPin = nullptr;
  _pwmGenerator = nullptr;
  //Todo Put it to Config
  _firstVolumePinName = "23";
  _secondVolumePinName = "24";
  _fanPwmPinName = "26";
  _firstVolumePinState = false;
  _pipefdwrite = pipefdwrite;
  _lastTempText = "";
  _i2cBus = nullptr;
  _mcp1 = nullptr;
}

SystemInfo::~SystemInfo() {
  if(_firstVolumePin != nullptr) delete _firstVolumePin;
  if(_secondVolumePin != nullptr) delete _secondVolumePin;
  if(_pwmGenerator != nullptr) delete _pwmGenerator;
  if(_pwmPin != nullptr) delete _pwmPin;
  if(_mcp1 != nullptr) delete _mcp1;
  if(_i2cBus != nullptr) delete _i2cBus;
}

double SystemInfo::GetCpuTemp() {
    //Todo refactory this
    std::string val;
    std::string preparedTemp;
    double temperature;

    std::ifstream temperatureFile ("/sys/class/thermal/thermal_zone0/temp");

    if (!temperatureFile.is_open()) {
        LOG(ERROR) << "Failed to open temperatureFile!";
        return -1;
    }

    // The temperature is stored in 5 digits.  The first two are degrees in C.  The rest are decimal precision.
    temperatureFile >> val;

    temperatureFile.close();

    preparedTemp = val.insert(2, 1, '.');
    preparedTemp = preparedTemp.substr(0, 5); //Zwei stellen nach dem Comma reichen völlig
    temperature = std::stod(preparedTemp);

    if(preparedTemp != _lastTempText) {
      SendWorkerMessage("CpuTempUpdate", preparedTemp);
      _lastTempText = preparedTemp;
    }
    
    return temperature;
}

void SystemInfo::CheckFan() {
  if(_pwmPin == nullptr) {
    return;
  }

  if(_pwmGenerator == nullptr) {
    auto now = std::chrono::system_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(now - _startup).count();
    if(duration < 20) {
      return;
       
    }
    _pwmGenerator = new SoftwarePWM(_pwmPin, 50, 100);
    _pwmGenerator->ChangeSignal(300);
  }

  if(std::stod(_lastTempText) >= 48) {
    _pwmGenerator->ChangeSignal(1000);
  } else if(std::stod(_lastTempText) >= 35) {
    _pwmGenerator->ChangeSignal(650);
  } else{
    _pwmGenerator->ChangeSignal(300);
  }

}

void SystemInfo::Init(const ServiceConfig* config) {
    _config = config;
    _startup = std::chrono::system_clock::now();

    if(_config->GetHardwareVolumeControl()) {
        _firstVolumePin = new GpioPin(_firstVolumePinName, pin_direction::in, pin_trigger::both);
        _secondVolumePin = new GpioPin(_secondVolumePinName, pin_direction::in, pin_trigger::both);
        auto pinChangedelegate = std::bind(&SystemInfo::PinChange, this, std::placeholders::_1, std::placeholders::_2);
        _firstVolumePin->Register(pinChangedelegate);
        _secondVolumePin->Register(pinChangedelegate);
        LOG(INFO) << "PIN's for Hardware Volume Control registert";

        _pwmPin = new GpioPin(_fanPwmPinName, pin_direction::out, pin_trigger::none);
        *_pwmPin << 1; // Full Fan on

        LOG(INFO) << "Fan is switched on";
    } else {
        LOG(INFO) << "Hardware Volume Off";
    }

    if(_config->GetMcpDeviceAddr1() != 0x00) {
      //Todo Put I2C Device Address to Config
      _i2cBus = new I2CBus("/dev/i2c-1");
      _mcp1 = new MCP23017(_i2cBus, _config->GetMcpDeviceAddr1()); 
      //Todo Put MCP Pin Config to Config at now all Outputs
      for(unsigned char x = 0; x < 16; x++ ) {
        _mcp1->ConfigPin(x, pin_direction::out);
      }
    }
}

void SystemInfo::DoSwitchAction(SwitchAction action) {
  if(action.device == "MCP1" && _mcp1 != nullptr) {
    //Todo Check is OutPut
    if(action.event == Button_Event::On) {
      if(_mcp1->SetPin(action.id, pin_value::on) == 0){
        LOG(DEBUG) << "Switch on at MCP1";
        SwitchEvent message;
        message.device = action.device;
        message.id = action.id;
        message.event = action.event;
        SendWorkerMessage("JsonMessageOut",message);
      } else {
        LOG(ERROR) << "Switch on at MCP1 is not done";
      }
    } else if(action.event == Button_Event::Off) {
      if(_mcp1->SetPin(action.id, pin_value::off) == 0){
        LOG(DEBUG) << "Switch off at MCP1";
        SwitchEvent message;
        message.device = action.device;
        message.id = action.id;
        message.event = action.event;
        SendWorkerMessage("JsonMessageOut", message);
      } else {
        LOG(ERROR) << "Switch off at MCP1 is not done";
      }
    }
  } else {
    LOG(WARNING) << "Unkown Device or Config not Done";
  }


}

void SystemInfo::SendIoStates() {
  LOG(INFO) << "Send IO States to GUI";
  if(_mcp1 != nullptr) {
    for(auto i = 0; i < 16; i++) {
      pin_value currentValue;
      if(_mcp1->GetPin(i, currentValue) == 0) {
        LOG(DEBUG) << std::to_string(i) << " is " << currentValue;
        SwitchEvent message;
        message.device = "MCP1";
        message.id = i;
        if(currentValue == pin_value::on) {
          message.event = Button_Event::On;
        } else {
          message.event = Button_Event::Off;
        }
        SendWorkerMessage("JsonMessageOut", message);
      } else {
        LOG(ERROR) << "reading MCP1 failed";
      }
    }
  }
}