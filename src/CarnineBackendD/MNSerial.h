#pragma once

#include <string>
#include <termios.h>
#include <thread>

#define MAXBUFFERSIZE 2048

class MNSerial
{
    int _handle;
    std::string _portname;
    termios _oldtio;
    termios _newtio;
    struct epoll_event* _epoll_events;
    int _efd;
    std::thread loop_thread_;
    bool _run;
    void SetSpeed(speed_t speed);
    void SetParity();
    void Loop();
    
protected:
    int pipefdwrite_;
    int currentReadPos_;
    char readBuffer_[MAXBUFFERSIZE];
    virtual void AnalyseBuffer(int count) = 0;
    void SendWorkerMessage(const std::string& type);
    void SendWorkerMessage(const std::string& type, const std::string& data);
public:
    MNSerial(const std::string& portname);
    virtual ~MNSerial();
    
    bool Open(speed_t speed);
    int GetHandle() const;
    int Read();
    int Write(const std::string& message);
    int RegisterCallBack(int pipefdwrite);
    void Close();
};

