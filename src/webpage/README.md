# in this dir code ist used on Webpage for Online Maps

## Info Links

### Install PHP Test Server

https://www.bennetrichter.de/anleitungen/apache2-php7-mariadb-phpmyadmin/ 
https://www.rosehosting.com/blog/how-to-install-php-7-2-on-debian-9/

### Learning PHP

http://www.mywebsolution.de/workshops/2/page_3/show_PHP-Loginsystem-Passwort-vergessen-Funktion.html
https://github.com/Karry/osmscout-scripts/blob/master/web/lib/Utils.php
https://www.w3schools.com/php/php_file_upload.asp

### Testing local
set path=%PATH%;f:\Tools\php72
php -S localhost:8080
http://localhost:8080/