<?php
	$wwwDir = dirname(__FILE__);
	$rootDir = $wwwDir . '/';
	
	require_once $rootDir . 'common.php';
	
	if(isset($_POST['submit']) AND $_POST['submit']=='Einloggen'){
		if(loginOk(trim($_POST['Nickname']), trim($_POST['Passwort'])) == 1) {
			$seconds = 3;
			if(isset($_SESSION['lastpage'])) {
				$url = $_SESSION['lastpage']; //urlencode(
				logText("redirect to: " . $url);
				header("refresh:$seconds;url=$url");
			}
			echo "Login Ok";
			echo "<div class='alert alert-info'>You Will Be Redirected to link After $seconds Seconds.</div>";
		} else {
			echo "<!doctype html>";
			echo "<html lang=\"de\">";
			echo "<body>";
			echo "<b>Error</b> Login not ok<br>";
			echo "</body>";
			echo "</html>";
		}
	} else {
		if(isset($_SERVER['HTTP_REFERER'])) {
			$url = $_SERVER['HTTP_REFERER'];
			$data = parse_url($url);
			if($data["host"] == "localhost") {
				if(isset($data["path"]) && isset($data["query"])) {
					$_SESSION['lastpage'] = $data["path"] . "?" . $data["query"];
				} elseif (isset($data["path"])) {
					$_SESSION['lastpage'] = $data["path"];
				}
				logText("lastpage to: " . $_SESSION['lastpage']);
			} else {
				unset($_SESSION['lastpage']);
			}
		}
		echo "<!doctype html>";
		echo "<html lang=\"de\">";
		echo "<body>";
		echo "<form ".
             " name=\"Login\" ".
             " action=\"".$_SERVER['PHP_SELF']."\" ".
             " method=\"post\" ".
             " accept-charset=\"UTF-8\">\n";
        echo "Nickname :\n";
        echo "<input type=\"text\" name=\"Nickname\" maxlength=\"32\">\n";
        echo "<br>\n";
        echo "Passwort :\n";
        echo "<input type=\"password\" name=\"Passwort\">\n";
        echo "<br>\n";
        echo "eingeloggt bleiben :\n";
        echo "<input type=\"checkbox\" name=\"Autologin\" value=\"1\">\n";
        echo "<br>\n";
        echo "<input type=\"submit\" name=\"submit\" value=\"Einloggen\">\n";
        echo "<br>\n";
        echo "<a href=\"passwort.php\">Passwort vergessen</a> oder noch nicht <a href=\"registrierung.php\">registriert</a>?\n";
        echo "</form>\n";
		echo "</body>";
		echo "</html>";
	}
?>

