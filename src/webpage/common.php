<?php
	$wwwDir = dirname(__FILE__);
	$rootDir = $wwwDir . '/';
	$logFilePath = $rootDir . 'logs/phplog.txt';
	$debug = 1; //Disable Logging
	
	function myErrorHandler($errno, $errstr, $errfile, $errline) {
		echo "<b>Custom error:</b> [$errno] $errstr<br>";
		echo " Error on line $errline in $errfile<br>";
		
		switch ($errno) {
			case E_NOTICE:
			case E_USER_NOTICE:
				$errors = "Notice";
				break;
			case E_WARNING:
			case E_USER_WARNING:
				$errors = "Warning";
				break;
			case E_ERROR:
			case E_USER_ERROR:
				$errors = "Fatal Error";
				break;
			default:
				$errors = "Unknown Error";
				break;
		}
	
		error_log(sprintf("PHP %s:  %s in %s on line %d", $errors, $errstr, $errfile, $errline));
		$msg = "ERROR: [$errno] $errstr\r\n" . "$errors on line $errline in file $errfile\r\n";
		logText($msg);
		
		if($errno > E_WARNING) {
			//sendErrorEmail($msg);
			//showErrorPage();
			echo "Error was reportet to admin<br>";
		}
	}
	
	function logText($logText) {
		global $debug, $logFilePath;
		if($debug == 1) {
			$logFile = fopen($logFilePath, 'a');
			if (is_array($logText)) {
				fwrite($logFile, implode( ',', $logText));
			} else {
				fwrite($logFile, $logText);
			}
			fwrite($logFile, "\n\r");
			fclose($logFile);
		}
	}
	function isBrowser() {
		$result = 0;
		$accept = getallheaders()['Accept'];
		$accept = explode( ',', $accept );
		logtext('IsBrowser gets:');
		logText($accept);
		foreach ($accept as &$value) {
			if($value == 'text/html') {
				$result = 1;
				break;
			}
		}
		return $result;
	}
	
	function isLoginOk() {
		if(isset($_SESSION['UserID'])) {
			//Todo Check in database
			return 1;
		} else {
			//it musst be an Post to get the Request body 
			$content = trim(file_get_contents("php://input"));
			$data = json_decode($content, true);
			logText('request body');
			logText($data);
			if(isset($data['user']) && isset($data['pass'])) {
				if(loginOk($data['user'], $data['pass']) == 1) {
					return 1;
				}
			}
			if(isBrowser() == 1) {
				header("Location: login.php");
			} else {
				http_response_code(401);
				$json["Error"] = "Unauthorized";
				sendJson($json);
			}
			return 0;
		}
	}
	
	function ErrorResponse($code, $text) {
		http_response_code($code);
		if(isBrowser() == 1) {
			header("Location: error.php");
			$_SESSION['LastErrorCode'] = $code;
			$_SESSION['LastErrorText'] = $text;
		} else {
			$json["Error"] = $text;
			sendJson($json);
		}
	}
	
	function sendJson($json) {
		header("Content-Type: application/json; charset=UTF-8");
		header("Cache-control: private");
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
	}
    
	function loginOk($userName, $password) {
		logText("Try login in with " . $userName . " " . $password);
		if($userName == "michael" && $password == "geheim"){
			logText("Login OK");
			return 1;
		}
		logText("Login not OK");
		return 0;
	}
	
	session_start();
	set_error_handler('myErrorHandler', E_ALL);
?>