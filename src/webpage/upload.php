<?php
	$wwwDir = dirname(__FILE__);
	$rootDir = $wwwDir . '/';
	
	require_once $rootDir . 'common.php';
	
	$content = trim(file_get_contents("php://input"));
	
	logText("Request Body: " . $content);
	
	if(isLoginOk() == 0) {
		exit;
	}
	
	if(!isset($_FILES["fileToUpload"])) {
		ErrorResponse(400, "Error no submit");
		exit;
	}
	
	$target_dir = $rootDir . 'uploads/';
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
		$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		if($check !== false) {
			echo "File is an image - " . $check["mime"] . ".<br>";
			$uploadOk = 1;
		} else {
			echo "File is not an image.";
			$uploadOk = 0;
		}
	} else {
		ErrorResponse(400, "Error no submit");
	}
	
	if (file_exists($target_file)) {
		echo "Sorry, file already exists.";
		$uploadOk = 0;
	}
	
	if ($_FILES["fileToUpload"]["size"] > 500000) {
		echo "Sorry, your file is too large.<br>";
		$uploadOk = 0;
	}
	
	if ($uploadOk == 0) {
		echo "Sorry, your file was not uploaded.<br>";
	} else {
		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		} else {
			echo "Sorry, there was an error uploading your file.";
		}
	}
?>
