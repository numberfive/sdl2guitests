#ifndef config_h
#define config_h

#include <Arduino.h>  // for type definitions
#define progdFlagDefault 0x5A02 // wenn config_t sich ändert hier eins hoch zählen (reinit EEPROM)

typedef struct 
{
  word progd_flag;
  byte enableAD;
  byte enableDIGI;
  byte outputDIGI;
  unsigned long interval;
} config_t;

#endif //DebugPrinter_h
