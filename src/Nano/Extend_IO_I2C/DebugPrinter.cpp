#include "DebugPrinter.h"
#include <Arduino.h>  // for type definitions

DebugPrinter_::DebugPrinter_() {
  enableDebug = true;
}

void DebugPrinter_::begin(unsigned long baud, bool enabled){
  Serial.begin(baud);
  enableDebug = enabled;
}

size_t DebugPrinter_::write(uint8_t k) {
  if(!enableDebug) return 0;
  return Serial.write(k);
}

void DebugPrinter_::printConfig(config_t config) {
   if(!enableDebug) return;
   
   print(F("progd: 0x"));
   println(config.progd_flag, HEX);

   print(F("enableAD: 0x"));
   println(config.enableAD, HEX);
   
   print(F("enableDIGI: 0x"));
   println(config.enableDIGI, HEX);

   print(F("outputDIGI: 0x"));
   println(config.outputDIGI, HEX);

   print(F("interval: "));
   println(config.interval);
}

bool DebugPrinter_::getEnabled(){
  return enableDebug;
}

void DebugPrinter_::setEnabled(bool enabled){
  enableDebug = enabled;
}

DebugPrinter_ DebugPrinter;
