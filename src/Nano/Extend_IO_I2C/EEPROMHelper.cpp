#include "EEPROMHelper.h"
  
unsigned long eeprom_crc32(int pos) {
  return eeprom_crc32(pos, EEPROM.length());
}

unsigned long eeprom_crc32(int pos, int length) {
  unsigned long crc = ~0L;
  length += pos;
  
  for (int index = pos ; index < length  ; ++index) {
    crc = crc_table[(crc ^ EEPROM[index]) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM[index] >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }

  return crc;
}
