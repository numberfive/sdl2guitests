#ifndef DebugPrinter_h
#define DebugPrinter_h

#include <Print.h>
#include "config.h"

class DebugPrinter_ : public Print
{
  private:
    bool enableDebug = true;
  public:
    DebugPrinter_();
    void begin(unsigned long baud, bool enabled = true);
    size_t write(uint8_t k);
    void printConfig(config_t config);
    bool getEnabled();
    void setEnabled(bool enabled);
};

extern DebugPrinter_ DebugPrinter;

#endif //DebugPrinter_h
