#pragma once
#include <string>
#include "utils/json.hpp"

#define BACKENDPROTOCOLVER "0.1"
#define SERVERPORT 5022

using json = nlohmann::json;

enum class Button_Type : int {
  Switch,
  PushButton
};

enum class Button_Event : int {
  On,
  Off,
  Impulse
};

struct BaseMessage {
    std::string Ver;
    std::string MessageType;
};

struct WelcomeMessage : BaseMessage {
    WelcomeMessage(){
        MessageType = "WelcomeMessage";
        Ver = BACKENDPROTOCOLVER;
    }
    std::string SystemName;
};

struct GPSMessage : BaseMessage {
    GPSMessage(){
        MessageType = "GPSMessage";
        Ver = BACKENDPROTOCOLVER;
        Longitude = 0;
        Latitude = 0;
        Compass = -1;
        Speed = -1;
    }
    double Longitude;
    double Latitude;
    double Compass;
    double Speed;
};

struct ShutdownMessage : BaseMessage {
    ShutdownMessage(){
        MessageType = "ShutdownMessage";
        Ver = BACKENDPROTOCOLVER;
        Powerdown = false;
    }
    ShutdownMessage(bool powerdown){
        MessageType = "ShutdownMessage";
        Ver = BACKENDPROTOCOLVER;
        Powerdown = powerdown;
    }
    bool Powerdown;
};

struct InitDoneMessage : BaseMessage {
    InitDoneMessage(){
        MessageType = "InitDoneMessage";
        Ver = BACKENDPROTOCOLVER;
    }
};

struct MediaFileUpdateMessage : BaseMessage {
    MediaFileUpdateMessage(){
        MessageType = "MediaFileUpdateMessage";
        Ver = BACKENDPROTOCOLVER;
    }
    
};

struct RemoteControlMessage : BaseMessage {
    RemoteControlMessage():
        info("add")
    {
        MessageType = "RemoteControlMessage";
        Ver = BACKENDPROTOCOLVER;
        
    }
    std::string info;
};

struct VolumeUpdateMessage : BaseMessage {
    VolumeUpdateMessage(){
        MessageType = "VolumeUpdateMessage";
        Ver = BACKENDPROTOCOLVER;
        value = "0";
    }
    std::string value;
};

struct CpuTempUpdate : BaseMessage {
    CpuTempUpdate(){
        MessageType = "CpuTempUpdate";
        Ver = BACKENDPROTOCOLVER;
        value = "0";
    }
    std::string value;
};

struct PowerSupplyOff : BaseMessage {
    PowerSupplyOff(){
        MessageType = "PowerSupplyOff";
        Ver = BACKENDPROTOCOLVER;
    }
};

/**
 *  \brief To Backend to Do this
 */
struct SwitchAction : BaseMessage {
    SwitchAction(){
        MessageType = "SwitchAction";
        Ver = BACKENDPROTOCOLVER;
    }
    std::string device;
    int id;
    Button_Type type;
    Button_Event event;
};

/**
 *  \brief from Backend to Show wath hapend
 */
struct SwitchEvent : BaseMessage {
    SwitchEvent(){
        MessageType = "SwitchEvent";
        Ver = BACKENDPROTOCOLVER;
    }
    std::string device;
    int id;
    Button_Event event;
};

/**
 *  \brief to Backend to get all IO States in Backend
 */
struct SendIoStates : BaseMessage {
    SendIoStates(){
        MessageType = "SendIoStates";
        Ver = BACKENDPROTOCOLVER;
    }
};

void from_json(const json& j, BaseMessage& p);
void to_json(json& j, const WelcomeMessage& p);

void from_json(const json& j, GPSMessage& p);
void to_json(json& j, const GPSMessage& p);

void from_json(const json& j, ShutdownMessage& p);
void to_json(json& j, const ShutdownMessage& p);

void from_json(const json& j, InitDoneMessage& p);
void to_json(json& j, const InitDoneMessage& p);

void from_json(const json& j, MediaFileUpdateMessage& p);
void to_json(json& j, const MediaFileUpdateMessage& p);

void from_json(const json& j, RemoteControlMessage& p);
void to_json(json& j, const RemoteControlMessage& p);

void from_json(const json& j, VolumeUpdateMessage& p);
void to_json(json& j, const VolumeUpdateMessage& p);

void from_json(const json& j, CpuTempUpdate& p);
void to_json(json& j, const CpuTempUpdate& p);

void from_json(const json& j, PowerSupplyOff& p);
void to_json(json& j, const PowerSupplyOff& p);

void from_json(const json& j, SwitchAction& p);
void to_json(json& j, const SwitchAction& p);

void from_json(const json& j, SwitchEvent& p);
void to_json(json& j, const SwitchEvent& p);

void from_json(const json& j, SendIoStates& p);
void to_json(json& j, const SendIoStates& p);