#include <string>
#include "utils/json.hpp"
#include "BackendMessages.h"

void from_json(const json& j, BaseMessage& p) {
    p.Ver = j.at("Ver").get<std::string>();
    auto it_value = j.find("MessageType");
    if(it_value != j.end())
        p.MessageType = j.at("MessageType").get<std::string>();
}

void to_json(json& j, const WelcomeMessage& p) {
     j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType},
        {"SystemName", p.SystemName}
    };
}

void from_json(const json& j, GPSMessage& p) {
    p.Ver = j.at("Ver").get<std::string>();
    p.MessageType = j.at("MessageType").get<std::string>();
    p.Longitude = j.at("Longitude").get<double>();
    p.Latitude = j.at("Latitude").get<double>();
    p.Compass = j.at("Compass").get<double>();
    p.Speed = j.at("Speed").get<double>();
}

void to_json(json& j, const GPSMessage& p) {
    j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType},
        {"Latitude", p.Latitude},
        {"Longitude", p.Longitude},
        {"Compass", p.Compass},
        {"Speed", p.Speed}
    };
}

void from_json(const json& j, ShutdownMessage& p) {
    p.Ver = j.at("Ver").get<std::string>();
    auto it_value = j.find("MessageType");
    if(it_value != j.end())
        p.MessageType = j.at("MessageType").get<std::string>();
}

void to_json(json& j, const ShutdownMessage& p) {
    j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType}
    };
}

void from_json(const json& j, InitDoneMessage& p) {
    p.Ver = j.at("Ver").get<std::string>();
    auto it_value = j.find("MessageType");
    if(it_value != j.end())
        p.MessageType = j.at("MessageType").get<std::string>();
}

void to_json(json& j, const InitDoneMessage& p) {
    j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType}
    };
}

void from_json(const json& j, MediaFileUpdateMessage& p) {
    p.Ver = j.at("Ver").get<std::string>();
    auto it_value = j.find("MessageType");
    if(it_value != j.end())
        p.MessageType = j.at("MessageType").get<std::string>();
}

void to_json(json& j, const MediaFileUpdateMessage& p) {
    j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType}
    };
}

void from_json(const json& j, RemoteControlMessage& p) {
    p.Ver = j.at("Ver").get<std::string>();
    p.MessageType = j.at("MessageType").get<std::string>();
    p.info = j.at("Info").get<std::string>();
}

void to_json(json& j, const RemoteControlMessage& p) {
    j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType},
        {"Info", p.info}
    };
}

void from_json(const json& j, VolumeUpdateMessage& p) {
    p.Ver = j.at("Ver").get<std::string>();
    p.MessageType = j.at("MessageType").get<std::string>();
    p.value = j.at("Value").get<std::string>();
}

void to_json(json& j, const VolumeUpdateMessage& p) {
    j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType},
        {"Value", p.value}
    };
}

void from_json(const json& j, CpuTempUpdate& p) {
    p.Ver = j.at("Ver").get<std::string>();
    p.MessageType = j.at("MessageType").get<std::string>();
    p.value = j.at("Value").get<std::string>();
}

void to_json(json& j, const CpuTempUpdate& p) {
    j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType},
        {"Value", p.value}
    };
}

void from_json(const json& j, PowerSupplyOff& p) {
    p.Ver = j.at("Ver").get<std::string>();
    p.MessageType = j.at("MessageType").get<std::string>();
}

void to_json(json& j, const PowerSupplyOff& p) {
     j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType}
    };
}

void from_json(const json& j, SwitchAction& p) {
    p.Ver = j.at("Ver").get<std::string>();
    p.MessageType = j.at("MessageType").get<std::string>();
    p.device = j.at("Device").get<std::string>();
    p.id = j.at("Id").get<int>();
    p.type = j.at("Type").get<Button_Type>();
    p.event = j.at("Event").get<Button_Event>();
}

void to_json(json& j, const SwitchAction& p) {
      j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType},
        {"Device", p.device},
        {"Id", p.id},
        {"Type", p.type},
        {"Event", p.event}
    };
}

void from_json(const json& j, SwitchEvent& p) {
    p.Ver = j.at("Ver").get<std::string>();
    p.MessageType = j.at("MessageType").get<std::string>();
    p.device = j.at("Device").get<std::string>();
    p.id = j.at("Id").get<int>();
    p.event = j.at("Event").get<Button_Event>();
}

void to_json(json& j, const SwitchEvent& p) {
    j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType},
        {"Device", p.device},
        {"Id", p.id},
        {"Event", p.event}
    };
}

void from_json(const json& j, SendIoStates& p) {
    p.Ver = j.at("Ver").get<std::string>();
    p.MessageType = j.at("MessageType").get<std::string>();
}

void to_json(json& j, const SendIoStates& p){
     j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType}
    };
}