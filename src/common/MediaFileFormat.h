/*
 * Copyright (C) 2018 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   MediaFileFormat.h
 * Author: punky
 *
 * Created on 7. Juli 2018, 09:54
 */

#ifndef MEDIAFILEFORMAT_H
#define MEDIAFILEFORMAT_H
#pragma once

#include "./utils/json.hpp"

using json = nlohmann::json;

typedef struct MediaTitleFileEntry_{
    std::string Name;
    std::string Album;
    std::string Artist;
    std::string Genre;
    std::string MediaFile;
    std::string ImageFile;
    int track;
    int year;
	double duration;
} MediaTitleFileEntry;

struct MediaTitleFile {
    std::string List;
    std::vector<MediaTitleFileEntry> Files;
};

void to_json(json& j, const MediaTitleFileEntry& fileentry);
void from_json(const json& j, MediaTitleFileEntry& fileentry);
void to_json(json& j, const MediaTitleFile& p);
void from_json(const json& j, MediaTitleFile& p);

#endif /* MEDIAFILEFORMAT_H */

