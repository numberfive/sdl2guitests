#pragma once
#include <vector>
#include <string>
#include <functional>
#include <cstdarg>
#ifdef USESDL
    #include <SDL.h>
#endif


#ifdef _WIN32
#define DIRCHAR '\\'
#include <tchar.h>
#else 
#define DIRCHAR '/'
#endif

namespace utils
{
#ifdef USESDL
    typedef std::function<void(void *userdata, int category, SDL_LogPriority priority, const char *message)> LOGCALLBACK;
    void LogOutputFunction(void *userdata, int category, SDL_LogPriority priority, const char *message);
#endif

	class stringf {
		char* chars_;
	public:
		stringf(const char* format, std::va_list arg_list);
		~stringf();
		const char* get() const;
	};

	bool FileExists(const std::string &Filename);
	bool hasEnding(std::string const &fullString, std::string const &ending);
	std::string getFileName(std::string const &pathString);
	std::string exec(const char* cmd, int& resultCode);
	FILE *MNFmemopen(const void *buffer, size_t size, const char* mode);
	std::string GetHomePath();
}
