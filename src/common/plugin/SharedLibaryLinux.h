/*
 * Copyright (C) 2019 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   SharedLibaryLinux.h
 * Author: punky
 *
 * Created on 15. Februar 2019, 07:47
 */

#include <dlfcn.h>
#include <errno.h>

#ifndef SHAREDLIBARYLINUX_H
#define SHAREDLIBARYLINUX_H

typedef struct {
    void* handle;
    char* errmsg;
} uv_lib_t;

static int uv__dlerror(uv_lib_t* lib);

int uv_dlopen(const char* filename, uv_lib_t* lib) {
  dlerror(); /* Reset error status. */
  lib->errmsg = nullptr;
  lib->handle = dlopen(filename, RTLD_LAZY);
  return lib->handle ? 0 : uv__dlerror(lib);
}

void uv_dlclose(uv_lib_t* lib) {
 if(lib->errmsg != nullptr) {
      delete[] lib->errmsg;
      lib->errmsg = nullptr;
  }

  if (lib->handle) {
    /* Ignore errors. No good way to signal them without leaking memory. */
    dlclose(lib->handle);
    lib->handle = nullptr;
  }
}

int uv_dlsym(uv_lib_t* lib, const char* name, void** ptr) {
  dlerror(); /* Reset error status. */
  *ptr = dlsym(lib->handle, name);
  return uv__dlerror(lib);
}

const char* uv_dlerror(const uv_lib_t* lib) {
  return lib->errmsg ? lib->errmsg : "no error";
}

char* uv__strdup(const char* s) {
  size_t len = strlen(s) + 1;
  char* m = new char[len];
  if (m == nullptr)
    return nullptr;
  return reinterpret_cast<char*>(memcpy(m, s, len));
}

static int uv__dlerror(uv_lib_t* lib) {
  const char* errmsg;

  if(lib->errmsg != nullptr) {
      delete[] lib->errmsg;
      lib->errmsg = nullptr;
  }

  errmsg = dlerror();

  if (errmsg) {
    lib->errmsg = uv__strdup(errmsg);
    return -1;
  }
  else {
    lib->errmsg = nullptr;
    return 0;
  }
}

#endif /* SHAREDLIBARYLINUX_H */

