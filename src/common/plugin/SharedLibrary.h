/*
 * Copyright (C) 2019 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * Based on https://sourcey.com/building-a-simple-cpp-cross-platform-plugin-system/
 * 
 * File:   SharedLibrary.h
 * Author: punky
 *
 * Created on 15. Februar 2019, 07:37
 */

#include <stdarg.h>  // For va_start, etc.
#include <memory>    // For std::unique_ptr

#ifndef SHAREDLIBRARY_H
#define SHAREDLIBRARY_H

// Shared library exports
#if defined(CARNINE_WIN) //&& defined(SCY_SHARED_LIBRARY)
    #include "SharedLibaryWindows.h"
#else
    #include "SharedLibaryLinux.h"
#endif

std::string string_format(const std::string fmt_str, ...) {
    int final_n, n = ((int)fmt_str.size()) * 2; /* Reserve two times as much as the length of the fmt_str */
    std::unique_ptr<char[]> formatted;
    va_list ap;
    while(1) {
        formatted.reset(new char[n]); /* Wrap the plain char array into the unique_ptr */
        strcpy(&formatted[0], fmt_str.c_str());
        va_start(ap, fmt_str);
        final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
        va_end(ap);
        if (final_n < 0 || final_n >= n)
            n += abs(final_n - n + 1);
        else
            break;
    }
    return std::string(formatted.get());
}

struct SharedLibrary
{
    /// Opens a shared library.
    /// The filename is in utf-8. Returns true on success and false on error.
    /// Call `SharedLibrary::error()` to get the error message.
    bool open(const std::string& path)
    {
        if (uv_dlopen(path.c_str(), &_lib)) {
            setError("Cannot load library");
            return false;
        }
        return true;
    }

    /// Closes the shared library.
    void close()
    {
        uv_dlclose(&_lib);
    }

    /// Retrieves a data pointer from a dynamic library.
    /// It is legal for a symbol to map to nullptr.
    /// Returns 0 on success and -1 if the symbol was not found.
    bool sym(const char* name, void** ptr)
    {
        if (uv_dlsym(&_lib, name, ptr)) {
            setError(string_format("Symbol '%s' not found.", name));
            return false;
        }
        return true;
    }

    void setError(const std::string& prefix)
    {
        std::string err(uv_dlerror(&_lib));
        //assert(!err.empty());
        _error = prefix + ": " + err;
        throw std::runtime_error(prefix + ": " + err);
    }

    std::string error() const { return _error; }

protected:
    uv_lib_t _lib;
    std::string _error;
};




#endif /* SHAREDLIBRARY_H */

