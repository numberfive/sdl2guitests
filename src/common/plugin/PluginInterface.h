/*
 * Copyright (C) 2018 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   PluginInterface.h
 * Author: punky
 *
 * Created on 26. November 2018, 07:09
 */

#ifndef PLUGININTERFACE
#define PLUGININTERFACE
#pragma once

// NOTE: When using STL containers and other complex types you
// will need to ensure plugins are always built using the same
// compiler, since system libraries aren't ABI compatible.
#define PLUGIN_ENABLE_STL 1

#include "Plugin.h"

namespace PLUGIN {
  
/// Virtual base class for plugins
class IPlugin
{
public:
    IPlugin(){};
    virtual ~IPlugin(){};

    //
    /// Commands

    /// Handle a command from the application.
    virtual bool onCommand(const char* node, const char* data,
                           unsigned int size) = 0;

    /*
    /// Return the last error message as a char pointer.
    /// If no error is set a nullptr will be returned.
    virtual const char* lastError() const = 0;


    //
    /// String accessors

    // Sets the internal string from the given value.
    /// The given value must be null terminated.
    virtual void setValue(const char* value) = 0;

    // Return the internal string value as a char pointer.
    /// Since we are returning a POD type plugins will be ABI agnostic.
    virtual const char* cValue() const = 0;


#if PLUGIN_ENABLE_STL
    /// Return the internal string value as an STL string.
    /// This method breaks ABI agnosticity.
    /// See the PLUGU_ENABLE_STL definition above.
    virtual std::string sValue() const = 0;
#endif
     */
};

} /*namespace PLUGIN*/

#endif /* PLUGININTERFACE_H */

