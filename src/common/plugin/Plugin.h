/*
 * Copyright (C) 2018 punky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * Based on https://sourcey.com/building-a-simple-cpp-cross-platform-plugin-system/
 * File:   PluginInterface.h
 * Author: punky
 *
 * Created on 26. November 2018, 06:45
 */

#ifndef PLUGIN
#define PLUGIN
#pragma once

// Shared library exports
#if defined(CARNINE_WIN) //&& defined(SCY_SHARED_LIBRARY)
    #if defined(PLUGIN_EXPORTS)
        #define Pluga_API __declspec(dllexport)
    #else
        #define Pluga_API __declspec(dllimport)
    #endif
#else
    #define PLUGIN_API // nothing
#endif

namespace PLUGIN {

// Forward declare the plugin class which must be defined externally.
class PLUGIN_API IPlugin;

// Define the API version.
// This value is incremented whenever there are ABI breaking changes.
#define CARNINE_PLUGIN_API_VERSION 1

#ifdef CARNINE_WIN
    #define CARNINE_PLUGIN_EXPORT __declspec(dllexport)
#else
    #define CARNINE_PLUGIN_EXPORT // empty
#endif
  
// Define a type for the static function pointer.
PLUGIN_API typedef IPlugin* (*GetPluginFunc)();

// Plugin details structure that's exposed to the application.
struct PluginDetails
{
    int apiVersion;
    const char* fileName;
    const char* className;
    const char* pluginName;
    const char* pluginVersion;
    GetPluginFunc initializeFunc;
};

#define CARNINE_STANDARD_PLUGIN_STUFF CARNINE_PLUGIN_API_VERSION, __FILE__

#define CARNINE_PLUGIN(classType, pluginName, pluginVersion)                      \
    extern "C" {                                                                  \
        CARNINE_PLUGIN_EXPORT PLUGIN::IPlugin* getPlugin()                        \
        {                                                                         \
            static classType singleton;                                           \
            return &singleton;                                                    \
        }                                                                         \
        CARNINE_PLUGIN_EXPORT PLUGIN::PluginDetails exports = {                   \
            CARNINE_STANDARD_PLUGIN_STUFF,                                        \
            #classType,                                                           \
            pluginName,                                                           \
            pluginVersion,                                                        \
            getPlugin,                                                            \
        };                                                                        \
    }

} /* namespace plugin */

#endif /* PLUGIN */

