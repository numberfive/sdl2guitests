﻿#include "stdafx.h"
#include "utils/easylogging++.h"
#include "IllegalStateException.h"

IllegalStateException::IllegalStateException() {
}

IllegalStateException::IllegalStateException(const char* message) {
	mMessage = message;
	LOG(ERROR) << "NullPointerException " << message;
}

IllegalStateException::~IllegalStateException() throw() {
}