message(STATUS "working on filesystem dir")

FILE (GLOB_RECURSE SRCS "*.cpp" "*.cxx" "*.cc" "*.c")

set(common_filesystem_SRCS ${SRCS} PARENT_SCOPE)