#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "directory_iterator"
#endif

#include "directory_iterator.h"

#ifdef _WIN32
#include <sys/stat.h>
#endif // _WIN32

#include "../../common/utils/easylogging++.h"

directory_iterator::directory_iterator() : dir(nullptr), entry(nullptr) {
    // This is a past-the-end iterator
    // Past-the-end iterators have entry == nullptr
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
}

directory_iterator::directory_iterator(std::string path_) : entry(nullptr), path(path_) {
    if (path.back() != '/') path.push_back('/'); // Needed to return a pretty path ;)
    dir = opendir(path.c_str());
    if (dir == nullptr) {
        LOG(ERROR) << "Unable to open library directory '" << path << "'.";
        throw std::runtime_error("Unable to open library directory " + path);
    }
    //    entry = readdir(dir);
    operator++();
    LOG(TRACE) << "ctor Entry:" << entry << " dir: " << dir << "/" << telldir(dir);
}

directory_iterator::directory_iterator(const directory_iterator& other) : path(other.path) {
    if (other.dir == nullptr) { // Cover the end iterator case
        dir = nullptr;
        entry = nullptr;
        return;
    }
    dir = opendir(path.c_str());
    if (dir == NULL) {
        LOG(ERROR) << "Unable to open directory '" << path << "'.";
        throw std::runtime_error("Unable to open directory");
    }
    LOG(TRACE) << "cctor Entry:" << entry << " dir: " << telldir(dir) << "other.dir: " << telldir(other.dir);
    seekdir(dir, telldir(other.dir));
    rewinddir(dir);
    operator++();
    LOG(TRACE) << "cctor Entry:" << entry << " dir: " << telldir(dir) << "other.dir: " << telldir(other.dir);
}

directory_iterator::directory_iterator(directory_iterator&& other) : dir(other.dir), entry(other.entry), path(other.path) {
    other.path = {};
    other.dir = nullptr;
    other.entry = nullptr;
}

bool directory_iterator::operator!=(const directory_iterator& right) const {
    bool result = !(*this == right); // Reuse equals operator
    return result;
}

const directory_iterator::value_type directory_iterator::operator*() const {
    LOG(TRACE) << "Entry:" << entry;
    return path + value_type(entry->d_name);
}

directory_iterator& directory_iterator::operator++() {
    LOG(TRACE) << "++ Entry:" << entry;
    while ((entry = readdir(dir)) != nullptr) {
        // Ensure we have a regular file
#ifdef __linux__
        if (entry->d_type == DT_REG) {
#elif _WIN32
        struct stat stats;
        stat((libraryFolder + entry->d_name).c_str(), &stats);

        if (S_ISREG(stats.st_mode)) {
#endif
            break;
        }
        LOG(TRACE) << "++ Skipping file '" << entry->d_name << "'" << " dir: " << dir << "/" << telldir(dir);
    }
    return *this;
}

directory_iterator directory_iterator::operator++(int) {
    directory_iterator result(*this); // Make a copy of myself.
    operator++(); // Reuse prefix operator
    return result;
}

directory_iterator& directory_iterator::operator=(const directory_iterator& right) {
    // Check for self-assignment!
    if (this == &right) // Same object?
        return *this; // Yes, so skip assignment, and just return *this.
    // Deallocate, allocate new space, copy values...
    path = right.path;
    dir = opendir(path.c_str());
    if (dir == NULL) {
        LOG(ERROR) << "Unable to open library directory '" << path << "'.";
        throw std::runtime_error("Unable to open library directory");
    }
    seekdir(dir, telldir(right.dir));
    return *this;
}

bool directory_iterator::operator==(const directory_iterator& right) const {
    if (entry == nullptr || right.entry == nullptr) {
        return entry == right.entry; // This covers the past-the-end iterator
    }
    return telldir(dir) == telldir(right.dir) && path == right.path;
}

directory_iterator::~directory_iterator() {
    LOG(TRACE) << "dtor " << dir;
    if (dir)
        closedir(dir);
}

directory_iterator begin(directory_iterator it) {
    return it;
}

directory_iterator end(const directory_iterator&) {
    return directory_iterator();
}
