#ifndef RECURSIVE_DIRECTORY_ITERATOR_H
#define RECURSIVE_DIRECTORY_ITERATOR_H

#include <string>
// Filesystem crawling
#include <dirent.h>

#include <deque>

class recursive_directory_iterator {
    std::deque<DIR*> dir;
    struct dirent* entry;
    std::string path;
    std::string subPath;

public:
    typedef int difference_type;
    typedef std::string value_type;
    typedef const std::string& reference;
    typedef const std::string* pointer;
    typedef std::input_iterator_tag iterator_category;

    /// Build a past-the-end iterator
    recursive_directory_iterator();

    // Build an iterator for a specific directory
    recursive_directory_iterator(std::string path);

    recursive_directory_iterator(const recursive_directory_iterator& other);
    recursive_directory_iterator(recursive_directory_iterator&& other);

    virtual ~recursive_directory_iterator();

    recursive_directory_iterator& operator=(const recursive_directory_iterator& right);
    recursive_directory_iterator& operator++();
    recursive_directory_iterator operator++(int);
    bool operator!=(const recursive_directory_iterator& right) const;
    bool operator==(const recursive_directory_iterator& right) const;
    const value_type operator*() const;

};
// Enable usage of the iterator with range-based for loops
recursive_directory_iterator begin(recursive_directory_iterator it);
recursive_directory_iterator end(const recursive_directory_iterator&);
#endif /* RECURSIVE_DIRECTORY_ITERATOR_H */

