#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "recursiveDdirectoryIterator"
#endif

#include "recursive_directory_iterator.h"

#ifdef _WIN32
#include <sys/stat.h>
#endif // _WIN32

#include "../../common/utils/easylogging++.h"

recursive_directory_iterator::recursive_directory_iterator() : entry(nullptr) {
    // This is a past-the-end iterator
    // Past-the-end iterators have entry == nullptr
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
}

recursive_directory_iterator::recursive_directory_iterator(std::string path_) : path(path_), subPath("") {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);

    if (path.back() == '/') path.pop_back();
    DIR* newDir = opendir(path.c_str());
    if (newDir == nullptr) {
        LOG(ERROR) << "Unable to open library directory '" << path << "'.";
        throw std::runtime_error("Unable to open library directory");
    }
    //    entry = readdir(newDir);
    dir.push_back(newDir);
    operator++();
   
}

recursive_directory_iterator::recursive_directory_iterator(const recursive_directory_iterator& other) : path(other.path), subPath(other.subPath) {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);

    if (other.dir.empty()) { // Cover the end iterator case
        dir = std::deque<DIR*>();
        entry = nullptr;
        return;
    }

    DIR* newDir = opendir(other.path.c_str());
    //    seekdir(newDir, telldir(other.dir.back()));
    //    rewinddir(newDir);
    dir.push_back(newDir);

    std::string currentPath = ""; //readdir(newDir)->d_name;
    while (dir.size() < other.dir.size()) {
        seekdir(dir.back(), telldir(other.dir.at(dir.size() - 1)));
        rewinddir(dir.back());
        currentPath += std::string("/") + readdir(dir.back())->d_name;
        dir.push_back(opendir((path + currentPath).c_str())); // Don't check, this HAS to be a directory (if I didn't fail the copy)
    }

    seekdir(dir.back(), telldir(other.dir.at(dir.size() - 1)));
    rewinddir(dir.back());
    std::cout << "currentPath '" << currentPath << "'" << std::endl;
    
    //dir = other.dir;

    operator++();
}

recursive_directory_iterator::recursive_directory_iterator(recursive_directory_iterator&& other) : dir(other.dir), entry(other.entry), path(other.path), subPath(other.subPath) {
    other.path = {};
    other.dir = {};
    other.entry = nullptr;
}

bool recursive_directory_iterator::operator!=(const recursive_directory_iterator& right) const {
    bool result = !(*this == right); // Reuse equals operator
    return result;
}

const recursive_directory_iterator::value_type recursive_directory_iterator::operator*() const {
    if (subPath.back() == '/')return path + subPath + entry->d_name; // TODO: figure out a way to remove this one?
    return path + subPath + '/' + entry->d_name;
}

recursive_directory_iterator& recursive_directory_iterator::operator++() {
    // Ensure we have a regular file
    while (!dir.empty()) {
        while ((entry = readdir(dir.back())) != nullptr) {
            //LOG(DEBUG) << "Processing '" << subPath << "/" << entry->d_name << "'";
            std::cout << "Processing '" << subPath << "/" << entry->d_name << "'" << std::endl;
#ifdef __linux__
            if (entry->d_type == DT_REG) {
#elif _WIN32
            struct stat stats;
            stat((libraryFolder + entry->d_name).c_str(), &stats);

            if (S_ISREG(stats.st_mode)) {
#endif
                //LOG(TRACE) << "This is a file.";
                std::cout << "This is a file." << std::endl;
                return *this; // We found a file, yield
            }

#ifdef __linux__
            if (entry->d_type == DT_DIR) {
#elif _WIN32
            struct stat stats;
            stat((libraryFolder + entry->d_name).c_str(), &stats);

            if (S_ISDIR(stats.st_mode)) {
#endif
                // Skip '.' and '..'
                if (std::strcmp(entry->d_name, ".") == 0 || std::strcmp(entry->d_name, "..") == 0) {
                    continue;
                }

                // Open the newly discovered directory
                if (subPath.back() != '/') subPath += '/';
                subPath += entry->d_name;
                DIR* newDir = opendir((path + subPath).c_str());
                if (newDir != nullptr) {
                    dir.push_back(newDir);
                }
                //LOG(DEBUG) << "Entering directory '" << subPath << "'";
                std::cout << "Entering directory '" << subPath << "'" << std::endl;
            }
        }
        //LOG(DEBUG) << "Reached end of directory, going back up...";
        closedir(dir.back());
        dir.pop_back();
        subPath = subPath.substr(0, subPath.find_last_of('/'));
    }
    LOG(TRACE) << "Reached end of iterator.";
    return *this;
}

recursive_directory_iterator recursive_directory_iterator::operator++(int) {
    recursive_directory_iterator result(*this); // Make a copy of myself.
    operator++(); // Reuse prefix operator
    return result;
}

recursive_directory_iterator& recursive_directory_iterator::operator=(const recursive_directory_iterator& right) {
    // Check for self-assignment!
    if (this == &right) // Same object?
        return *this; // Yes, so skip assignment, and just return *this.
    // Deallocate, allocate new space, copy values...
    for (DIR* d : dir)
        closedir(d);
    dir.clear();

    path = right.path;
    subPath = right.subPath;

    DIR* newDir = opendir(right.path.c_str());
    seekdir(newDir, telldir(right.dir.back()));
    //    rewinddir(newDir);
    dir.push_back(newDir);

    subPath = ""; //readdir(newDir)->d_name;
    while (dir.size() < right.dir.size()) {
        seekdir(dir.back(), telldir(right.dir.at(dir.size() - 1)));
        //        rewinddir(dir.back());
        subPath += std::string("/") + readdir(dir.back())->d_name;
        dir.push_back(opendir((path + subPath).c_str())); // Don't check, this HAS to be a directory (if I didn't fail the copy)
    }

    seekdir(dir.back(), telldir(right.dir.at(dir.size() - 1)));
    rewinddir(dir.back());
    operator++();
    return *this;
}

bool recursive_directory_iterator::operator==(const recursive_directory_iterator& right) const {
    if (entry == nullptr || right.entry == nullptr) {
        return entry == right.entry; // This covers the past-the-end iterator
    }
    // TODO: maybe using telldir on every DIR* is faster than comparing the full path
    return dir.size() == right.dir.size() && telldir(dir.back()) == telldir(right.dir.back()) && path == right.path && subPath == right.subPath;
}

recursive_directory_iterator::~recursive_directory_iterator() {
    for (DIR* d : dir)
        closedir(d);
}

recursive_directory_iterator begin(recursive_directory_iterator it) {
    return it;
}

recursive_directory_iterator end(const recursive_directory_iterator&) {
    return recursive_directory_iterator();
}
