#include "stdafx.h"
#include "FileNotFoundException.h"
#include "utils/easylogging++.h"
#ifdef NVWAMEMCHECK
#	include "nvwa/debug_new.h"
#endif

FileNotFoundException::FileNotFoundException() {
}

FileNotFoundException::FileNotFoundException(const char* message) {
	mMessage = message;
	LOG(ERROR) << "FileNotFoundException " << message;
}

FileNotFoundException::FileNotFoundException(const std::string& message) {
	mMessage = message;
	LOG(ERROR) << "FileNotFoundException " << message;
}

FileNotFoundException::~FileNotFoundException() throw() {
}
