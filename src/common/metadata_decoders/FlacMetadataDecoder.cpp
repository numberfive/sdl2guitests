#include <taglib/tag.h>
#include <taglib/fileref.h>
#include "FlacMetadataDecoder.h"
#include "../commonutils.h"
#include "../FileNotFoundException.h"

bool FlacMetadataDecoder::canHandle(const std::string& file) {
    return utils::hasEnding(file, ".flac");
}

MediaTitleFileEntry FlacMetadataDecoder::getMetadata(const std::string& fileName) {
    if(!utils::FileExists(fileName)) {
        throw FileNotFoundException(fileName);
    }

    TagLib::FileRef f(fileName.c_str());
    MediaTitleFileEntry data;
    data.MediaFile = fileName;

    if (f.isNull() || f.tag() == nullptr) {
        return data;
    }
    
    const auto tag = f.tag();
    data.Album = tag->album().to8Bit();
    data.Artist = tag->artist().toCString(true);
    data.Name = tag->title().toCString(true);
    data.Genre = tag->genre().toCString(true);
    data.duration = f.audioProperties()->lengthInSeconds();
    data.track = tag->track();
	data.year = tag->year();

    return data;
}