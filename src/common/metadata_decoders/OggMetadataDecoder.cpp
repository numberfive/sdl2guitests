#include "OggMetadataDecoder.h"

#include "../../common/utils/easylogging++.h"
#include <taglib/fileref.h>
#include <taglib/vorbisfile.h>
#include "../../../Externals/CImg/CImg.h"
#include "../commonutils.h"
#include <cstdlib>
#include "../FileNotFoundException.h"

bool OggMetadataDecoder::canHandle(const std::string& file) {
    return utils::hasEnding(file, ".ogg");
}

static std::string getPicture(std::string fileName) {
    TagLib::Ogg::Vorbis::File file(fileName.c_str());
    if (!file.isOpen() || file.tag() == nullptr) {
        return {};
    }

    // TODO: This should be externalized into the filesystem utilities
    std::string imageFile = "/tmp/cover.XXXXXX";
    int fd = mkstemp(&imageFile[0]);
    if (fd == -1) {
        LOG(ERROR) << "Failed to create temporary file '" << imageFile << "' with error:" << strerror(errno);
        return {};
    }
    // Make it readable by everybody
    if (fchmod(fd, 0644) == -1) {
        LOG(ERROR) << "Failed to change permission of temporary file '" << imageFile << "' with error:" << strerror(errno);
        return {};
    }

    const auto tag = file.tag();
    TagLib::List<TagLib::FLAC::Picture*> pictureList = tag->pictureList();
    LOG(TRACE) << "There is " << pictureList.size() << " pictures in this file.";
    for (TagLib::FLAC::Picture* picture : pictureList) {
        // TODO: Use the type to determine which picture to use?
        unsigned int size = picture->data().size();
        LOG(TRACE) << "picture is an " + picture->mimeType() + " with size " << size << " and color depth " << picture->colorDepth();

        char* srcImage = new char[size];
        memcpy(srcImage, picture->data().data(), size);
        FILE* stream = fmemopen(srcImage, size, "rb");
        cimg_library::CImg<int32_t> image;
        image.load_jpeg(stream);
        image.resize(32, 32); // TODO: Play with the interpolation param
        fclose(stream);
        stream = fopen(imageFile.c_str(), "w+b");
        if (!stream) {
            LOG(ERROR) << "Failed to save image to file '" << imageFile << "' with error: " << strerror(errno);
            imageFile = {};
        } else {
            image.save_jpeg(stream);
            fclose(stream);
            LOG(TRACE) << "Successfuly saved the image to '" << imageFile << "'";
        }
        break; // Stop on the first loadable image
    }
    return imageFile;
}

MediaTitleFileEntry OggMetadataDecoder::getMetadata(const std::string& fileName) {
    if(!utils::FileExists(fileName)) {
        throw FileNotFoundException(fileName);
    }

    TagLib::Ogg::Vorbis::File file(fileName.c_str());
   
    MediaTitleFileEntry data;
    data.MediaFile = fileName;

    LOG(TRACE) << "Processing metadata of " << fileName;
    if (!file.isOpen() || file.tag() == nullptr) {
        return data;
    }

    const auto tag = file.tag();
    data.Album = tag->album().to8Bit();
    data.Artist = tag->artist().toCString(true);
    data.Name = tag->title().toCString(true);
    data.Genre = tag->genre().toCString(true);
    data.duration = 0; //Todo data.duration = f.audioProperties()->lengthInSeconds();
    data.track = tag->track();
	data.year = tag->year();

    // Retrieve the cover
    data.ImageFile = getPicture(fileName);

    return data;
}

