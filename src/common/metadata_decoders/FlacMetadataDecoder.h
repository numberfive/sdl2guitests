#ifndef FLACMETADATADECODER_H
#define FLACMETADATADECODER_H

#include <string>
#include "../../common/MediaFileFormat.h"

namespace FlacMetadataDecoder {
    bool canHandle(const std::string& file);
    MediaTitleFileEntry getMetadata(const std::string& fileName);
};

#endif /* FLACMETADATADECODER_H */

