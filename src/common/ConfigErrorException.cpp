#include "stdafx.h"
#include "ConfigErrorException.h"
#include "utils/easylogging++.h"
#ifdef NVWAMEMCHECK
#	include "nvwa/debug_new.h"
#endif

ConfigErrorException::ConfigErrorException() {
}

ConfigErrorException::ConfigErrorException(const char* message) {
	mMessage = message;
	LOG(ERROR) << "ConfigErrorException " << message;
}

ConfigErrorException::ConfigErrorException(const std::string& message) {
	mMessage = message;
	LOG(ERROR) << "ConfigErrorException " << message;
}


ConfigErrorException::~ConfigErrorException() throw() {
}
