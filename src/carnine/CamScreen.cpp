#include "CamScreen.h"
#include "framework/gui/GUIElementManager.h"
#include "framework/gui/GUITextLabel.h"

CamScreen::CamScreen(GUIElementManager* manager)
{
    manager_ = manager;
}

CamScreen::~CamScreen()
{
}

GUIElement* CamScreen::Create() {
    auto screenBackground = new GUITestElement(GUIPoint(0, 0), GUISize(100, 100, sizeType::relative), transparent_white_color, "camScreen");
    manager_->AddElement(screenBackground);
    screenBackground->Invisible();

    auto screenTextlabel = new GUITextLabel(GUIPoint(30, 35), GUISize(904, 300), "camScreenText", lightgray_t_color, own_blue_color);
    manager_->AddElement(screenBackground, screenTextlabel);
    screenTextlabel->FontHeight(30);
    screenTextlabel->Rotate(-8);
    screenTextlabel->Text("No Cam installed");

    return screenBackground;
}