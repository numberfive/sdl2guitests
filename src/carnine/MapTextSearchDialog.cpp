#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "MapTextSearchDialog"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif
#include "../common/utils/easylogging++.h"
#include "MapTextSearchDialog.h"
#include "framework/gui/GUI.h"
#include "framework/gui/GUITexture.h"
#include "framework/gui/GUIElement.h"
#include "framework/gui/GUIOnClickDecorator.h"
#include "framework/gui/GUITestElement.h"
#include "framework/gui/GUITextButton.h"
#include "framework/gui/GUIElementManager.h"
#include "framework/SDLEventManager.h"
#include "framework/AppEvents.h"
#include "framework/gui/GUITextEdit.h"
#include "framework/gui/GUIKeyboard.h"
#include "framework/map/MapManager.h"

MapTextSearchDialog::MapTextSearchDialog(GUIElementManager* manager, MapManager* mapManager):
    IPopupDialog(manager) {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    mapManager_ = mapManager;
}

MapTextSearchDialog::~MapTextSearchDialog()
{
}

void MapTextSearchDialog::CreateIntern() {
    screenElement_ = new GUITestElement(GUIPoint(112, 30), GUISize(800, 520), white_color, "MapTextSearchDialog");
    manager_->AddElement(screenElement_);
    
    auto input = new GUITextEdit(GUIPoint(10, 10), GUISize(760, 35), white_color, "MapSearchTextInput");
    manager_->AddElement(screenElement_, input);
    input->Select();
    
    if(SDL_HasScreenKeyboardSupport() == SDL_FALSE){
        //Create OwnScreenBoard but ist better to do in Edit?!?
        auto keyboard = new GUIKeyboard(GUIPoint(10, 135), GUISize(780, 340), lightgray_color, "MapSearchKeyboard");
        manager_->AddElement(screenElement_, keyboard);
    }

    auto test5 = new GUITextButton(GUIPoint(10, 480), GUISize(250, 35), "MapMenuBackButton", own_blue_color, white_color);
    manager_->AddElement(screenElement_, test5);
    test5->FontHeight(30);
    test5->Text(u8"Zurück");
    test5->RegisterOnClick([this](IGUIElement* sender) {
        sender->EventManager()->PushApplicationEvent(AppEvent::ClosePopup, this, nullptr);
    });
    
    auto searchButton = new GUITextButton(GUIPoint(540, 480), GUISize(250, 35), "MapMenuSearchButton", own_blue_color, white_color);
    manager_->AddElement(screenElement_, searchButton);
    searchButton->FontHeight(30);
    searchButton->Text(u8"Suchen");
    searchButton->RegisterOnClick([this, input](IGUIElement* sender) {
      this->Search(input->GetText());
    });
}

void MapTextSearchDialog::Search(const std::string& text) {
  LOG(DEBUG) << "Search for " << text;
  
  std::vector<std::string> result;
  mapManager_->SearchForText(text, result);
  
}