#pragma once
#include "framework/gui/GUIElement.h"
#include "framework/gui/GUITestElement.h"

class CamScreen
{
private:
    GUIElementManager* manager_;
public:
    CamScreen(GUIElementManager* manager);
    ~CamScreen();

    GUIElement* Create();
};


