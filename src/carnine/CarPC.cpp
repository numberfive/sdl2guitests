#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "CarPC"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif
#include "CarPC.h"
#include "framework/AppEvents.h"
#include "framework/kernel.h"
#include "framework/sound/audio.h"
#include "framework/gui/GUITextLabel.h"
#include "framework/gui/GUIRoundPictureButton.h"
#include "framework/gui/GUITextButton.h"
#include "framework/gui/GUITestElement.h"
#include "framework/gui/GUIElementManager.h"
#include "framework/gui/GUIListview.h"
#include "framework/gui/GUIMapview.h"
#include "framework/gui/GUIMediaDisplay.h"
#include "framework/media/MediaManager.h"
#include "framework/gui/GUIIconview.h"
#include "framework/gui/IPopupDialog.h"
#include "MapMenuDialog.h"
#include "ErrorMessageDialog.h"
#include "MapTextSearchDialog.h"

#ifdef NVWAMEMCHECK
	#include "../common/nvwa/debug_new.h"
#endif

extern std::ostream& operator<<(std::ostream& os, const AppEvent c);

std::ostream& operator<<(std::ostream& os, const UiState c)
{
	switch (c)
	{
		case UiState::home: os << "Home";    
			break;
		case UiState::map: os << "Map"; 
			break;
		case UiState::exit: os << "Exit"; 
			break;
		case UiState::player: os << "Player"; 
			break;
		case UiState::settings: os << "Settings"; 
			break;
		case UiState::undefined: os << "Undefined"; 
			break;
		case UiState::mediathek: os << "Mediathek";
			break;
		case UiState::cam: os << "Cam";
			break;
		case UiState::switches: os << "Switches";
			break;
		default: os.setstate(std::ios_base::failbit);
			os << "Error";
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, const MusikPlayerState c)
{
	switch (c)
	{
	case MusikPlayerState::stop : os << "Stop";
		break;
	case MusikPlayerState::loading: os << "Loading";
		break;
	case  MusikPlayerState::play: os << "Play";
		break;
	case MusikPlayerState::pause: os << "Pause";
		break;
	case MusikPlayerState::undefined: os << "Undefined";
		break;
	default: os.setstate(std::ios_base::failbit);
	}
	return os;
}


void CarPC::StartAudio() const {
    kernel_->StartAudio();
}

void CarPC::StartServices() const {
    kernel_->StartServices();
}

void CarPC::KernelstateChanged(KernelState state)
{
	if (state == KernelState::Startup) {
        LOG(INFO) << "Start Services";

#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
    	TIMED_SCOPE_IF(timerBlkObjclear, "StartServices", VLOG_IS_ON(4));
#endif
        StartServices();
        
        LOG(INFO) << "Kernel is up Create Screen";

#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
    	TIMED_SCOPE_IF(timerCreateScreen, "CreateScreen", VLOG_IS_ON(4));
#endif
        manager_ = kernel_->CreateScreen("CarNine an other CarPC GUI");

#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
    	TIMED_SCOPE_IF(timerStartAudio, "StartAudio", VLOG_IS_ON(4));
#endif		
        StartAudio();
		
#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
    	TIMED_SCOPE_IF(timerBuildFirstScreen, "BuildFirstScreen", VLOG_IS_ON(4));
#endif	
		BuildFirstScreen();

		mediaManager_ = kernel_->GetMediaManger();
    }
}

void CarPC::ShowErrorMessage(const std::string& message) {
    auto errorMessageDialog =  new ErrorMessageDialog(manager_);
    errorMessageDialog->SetMessage(message);
    errorMessageDialog->Create(nullptr);
    lastPopup_ = errorMessageDialog;
}

void CarPC::ApplicationEvent(const AppEvent event, void* data1, void* data2) {
	UNUSED(data2);

	LOG(INFO) << "ApplicationEvent " << event << " UiState " << appUiState_ << " PlayerState " << playerState_;

	if (event == AppEvent::CloseButtonClick) {
		if(appUiStateCurrent_ == UiState::mediathek) {
			appUiState_ = UiState::player;
			UpdateUI();
		}
		else {
			auto element = manager_->GetElementByName("PowerPopup");
			if (element != nullptr) manager_->Close(element);
			appUiState_ = UiState::home;
			UpdateUI();
		}
	}
    else if (event == AppEvent::PlayButtonClick) {
		if(playerState_ == MusikPlayerState::play) {
			current_playlist_.is_running = false;
			kernel_->StopMusik();
		}
		if (playerState_ == MusikPlayerState::stop) {
			current_playlist_.is_running = true;
			kernel_->PlayMusik(currentMediaSource_);
			playerState_ = MusikPlayerState::loading;
		}
	} else if (event == AppEvent::MusikStreamPlay) {
		playerState_ = MusikPlayerState::play;
		auto element = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("playButton"));
		if(element != nullptr) {
			element->Select();
		}
		const auto streamInfo = static_cast<AudioStreamInfo*>(data1);
		if(streamInfo != nullptr) {
			auto elementInfo = static_cast<GUITextLabel*>(manager_->GetElementByName("playerInfoTextLabel"));
			if (elementInfo != nullptr) {
                if (current_playlist_.is_running && current_playlist_.ids.size() > 0) {
	                const auto playAddInfo = "\nPlaylist " + std::to_string(current_playlist_.current_pos + 1) + "/" + std::to_string(current_playlist_.ids.size());
                    elementInfo->Text("Album: " + streamInfo->metaAlbum_ + "\nInterpret: " + streamInfo->metaArtist_ + "\nTitel: " + streamInfo->metaTitle_ + playAddInfo);
                } else {
                    elementInfo->Text("Album: " + streamInfo->metaAlbum_ + "\nInterpret: " + streamInfo->metaArtist_ + "\nTitel: " + streamInfo->metaTitle_);
                }
			}
			delete streamInfo;
		}
		auto elementDisplay = static_cast<GUIMediaDisplay*>(manager_->GetElementByName("mediaDisplay1"));
		if (elementDisplay != nullptr) {
			elementDisplay->StreamStatus(true);
		}
		auto elementPlayerStateIcon = static_cast<GUIIconview*>(manager_->GetElementByName("PlayerStateIcon"));
		if (elementPlayerStateIcon != nullptr) {
			elementPlayerStateIcon->SetCurrentIcon("cd-play.png");
		}
	} else if (event == AppEvent::MusikStreamStopp) {
		playerState_ = MusikPlayerState::stop;
		auto element = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("playButton"));
		if (element != nullptr) {
			element->Unselect();
		}
        if (current_playlist_.is_running) {
			if (current_playlist_.ids.size() > 0 && (size_t)(current_playlist_.current_pos + 1) < current_playlist_.ids.size()) {
				current_playlist_.current_pos++;
				PlaySongId(manager_->GetElementByName("mediaDisplay1"), current_playlist_.ids[current_playlist_.current_pos]);
			} else {
                current_playlist_.is_running = false;
            }
		}
		auto elementInfo = static_cast<GUITextLabel*>(manager_->GetElementByName("playerInfoTextLabel"));
		if (elementInfo != nullptr) {
            if (current_playlist_.is_running) {
                elementInfo->Text("load next titel");
            } else {
                elementInfo->Text("no musik loaded");
            }
		}
		auto elementDisplay = static_cast<GUIMediaDisplay*>(manager_->GetElementByName("mediaDisplay1"));
		if (elementDisplay != nullptr) {
			elementDisplay->StreamStatus(false);
		}
		auto elementPlayerStateIcon = static_cast<GUIIconview*>(manager_->GetElementByName("PlayerStateIcon"));
		if (elementPlayerStateIcon != nullptr) {
			elementPlayerStateIcon->SetCurrentIcon("cd-pause.png");
		}
	} else if (event == AppEvent::MusikStreamError) {
		playerState_ = MusikPlayerState::stop;
		ShowErrorMessage("Musik Datei konnte nicht geladen werden\n" + currentMediaSource_);
	} else if (event == AppEvent::AlbenClick) {
		auto element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelTitel"));
		if (element != nullptr) {
			element->Unselect();
		}
		auto songListview = static_cast<GUITextLabel*>(manager_->GetElementByName("songListview"));
		if (songListview != nullptr) {
			songListview->Invisible();
		}
		element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelAlben"));
		if (element != nullptr) {
			element->Select();
		}
		auto albenListview = static_cast<GUITextLabel*>(manager_->GetElementByName("albenListview"));
		if (albenListview != nullptr) {
			albenListview->Visible();
		}
		element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelListe"));
		if (element != nullptr) {
			element->Unselect();
		}
		auto listListview = static_cast<GUITextLabel*>(manager_->GetElementByName("ListListview"));
		if (listListview != nullptr) {
			listListview->Invisible();
		}
        element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekFileSource"));
		if (element != nullptr) {
			element->Unselect();
		}
        auto fileListview = static_cast<GUITextLabel*>(manager_->GetElementByName("fileListview"));
		if (fileListview != nullptr) {
			fileListview->Invisible();
		}
	} else if (event == AppEvent::TitelClick) {
		auto element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelTitel"));
		if (element != nullptr) {
			element->Select();
		}
		auto songListview = static_cast<GUITextLabel*>(manager_->GetElementByName("songListview"));
		if (songListview != nullptr) {
			songListview->Visible();
		}
		element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelAlben"));
		if (element != nullptr) {
			element->Unselect();
		}
		auto albenListview = static_cast<GUITextLabel*>(manager_->GetElementByName("albenListview"));
		if (albenListview != nullptr) {
			albenListview->Invisible();
		}
		element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelListe"));
		if (element != nullptr) {
			element->Unselect();
		}
		auto listListview = static_cast<GUITextLabel*>(manager_->GetElementByName("ListListview"));
		if (listListview != nullptr) {
			listListview->Invisible();
		}
		element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekFileSource"));
		if (element != nullptr) {
			element->Unselect();
		}
		auto fileListview = static_cast<GUITextLabel*>(manager_->GetElementByName("fileListview"));
		if (fileListview != nullptr) {
			fileListview->Invisible();
		}
	} else if (event == AppEvent::PlaylistClick) {
		auto element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelTitel"));
		if (element != nullptr) {
			element->Unselect();
		}
		auto songListview = static_cast<GUITextLabel*>(manager_->GetElementByName("songListview"));
		if (songListview != nullptr) {
			songListview->Invisible();
		}
		element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelAlben"));
		if (element != nullptr) {
			element->Unselect();
		}
		auto albenListview = static_cast<GUITextLabel*>(manager_->GetElementByName("albenListview"));
		if (albenListview != nullptr) {
			albenListview->Invisible();
		}
		element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelListe"));
		if (element != nullptr) {
			element->Select();
		}
		auto listListview = static_cast<GUITextLabel*>(manager_->GetElementByName("ListListview"));
		if (listListview != nullptr) {
			listListview->Visible();
		}
        element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekFileSource"));
		if (element != nullptr) {
			element->Unselect();
		}
		auto fileListview = static_cast<GUITextLabel*>(manager_->GetElementByName("fileListview"));
		if (fileListview != nullptr) {
			fileListview->Invisible();
		}
    } else if (event == AppEvent::FilelistClick) {
		auto element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelTitel"));
		if (element != nullptr) {
			element->Unselect();
		}
		auto songListview = static_cast<GUITextLabel*>(manager_->GetElementByName("songListview"));
		if (songListview != nullptr) {
			songListview->Invisible();
		}
		element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelAlben"));
		if (element != nullptr) {
			element->Unselect();
		}
		auto albenListview = static_cast<GUITextLabel*>(manager_->GetElementByName("albenListview"));
		if (albenListview != nullptr) {
			albenListview->Invisible();
		}
		element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelListe"));
		if (element != nullptr) {
			element->Unselect();
		}
                element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekLabelListe"));
		if (element != nullptr) {
			element->Unselect();
		}
                element = static_cast<GUITextLabel*>(manager_->GetElementByName("mediathekFileSource"));
		if (element != nullptr) {
			element->Select();
		}
                auto fileListview = static_cast<GUITextLabel*>(manager_->GetElementByName("fileListview"));
		if (fileListview != nullptr) {
			fileListview->Visible();
		}
	} else if (event == AppEvent::CloseGuiElement) {
        const auto element = static_cast<GUIElement*>(data1);
		if (element != nullptr) {
			manager_->Close(element);
		}
	} else if (event == AppEvent::ChangeUiState) {
        UpdateUI();
    } else if (event == AppEvent::BackendConnected) {
        auto backendStateIcon = static_cast<GUIIconview*>(manager_->GetElementByName("BackendStateIcon"));
        if (backendStateIcon != nullptr) {
            backendStateIcon->SetCurrentIcon("service_online.png");
        }
        kernel_->SendInitDoneBackend();
		//Todo Backend and Amp Need Moment to Power Up
		SDL_Delay(500);
        kernel_->PlaySound("startup.wav");
    } else if (event == AppEvent::BackendDisconnected) {
        auto backendStateIcon = static_cast<GUIIconview*>(manager_->GetElementByName("BackendStateIcon"));
        if (backendStateIcon != nullptr) {
            backendStateIcon->SetCurrentIcon("service_offline.png");
        }
    } else if (event == AppEvent::MapMenuOpen) {
        ShowMapMenu();
    } else if (event == AppEvent::ClosePopup) {
        const auto element = static_cast<IPopupDialog*>(data1);
        if (element != nullptr) {
            element->Close();
            if(lastPopup_ == element) {
                lastPopup_ = nullptr;
            }
            delete element;
        }
    } else if (event == AppEvent::OpenMapTextSearch) {
        const auto element = static_cast<IPopupDialog*>(data1);
        if (element != nullptr) {
            element->Close();
            if(lastPopup_ == element) {
                lastPopup_ = nullptr;
            }
            delete element;
        }
        
        auto mapMenuButton = manager_->GetElementByName("MapMenuButton");
        mapMenuButton->Invisible();
        
        auto mapTextSearchDialog = new MapTextSearchDialog(manager_, kernel_->GetMapManager());
        mapTextSearchDialog->Create( [this]() { 
            auto mapMenuButton = manager_->GetElementByName("MapMenuButton");
            mapMenuButton->Visible();
        });
        lastPopup_ = mapTextSearchDialog;
    } else if (event == AppEvent::MediaFileUpdate) {
        //Todo Check if List is on screen when Update Screen
        for (auto &resultEntry : _loadedFileMediaTitle) {
            delete resultEntry;
        }
        _loadedMediaAlben.clear();
        filelistNeedUpdate_ = true;
    } else if (event == AppEvent::PowerSupplyOff) {
        ShowErrorMessage("Ausschalten");
		kernel_->GetEventManager()->PushKernelEvent(KernelEvent::Shutdown);
    } else if (event == AppEvent::VolumeUp) {
       kernel_->VolumeUp();
    } else if (event == AppEvent::VolumeDown) {
       kernel_->VolumeDown();
    }
    else {
		LOG(WARNING) << "Appevent is not implemented";
    }
}

void CarPC::PowerButtonClick(IGUIElement* sender) {
	UNUSED(sender);
	appUiState_ = UiState::exit;
	sender->EventManager()->PushApplicationEvent(AppEvent::ChangeUiState, nullptr, nullptr);
}

void CarPC::Button1LongClick(IGUIElement* sender) {
	UNUSED(sender);
	LOG(INFO) << "Button1LongClick";
}

void CarPC::ShutdownButtonClick(IGUIElement* sender) {
	LOG(DEBUG) << "ShutdownButtonClick";
	kernel_->SendShutdownBackend(false);
	sender->EventManager()->PushKernelEvent(KernelEvent::Shutdown);
}

void CarPC::CloseButtonClick(IGUIElement* sender) {
	LOG(DEBUG) << "CloseButtonClick";
	sender->EventManager()->PushApplicationEvent(AppEvent::CloseButtonClick, nullptr, nullptr);
}

void CarPC::PowerdownButtonClick(IGUIElement* sender) {
    LOG(DEBUG) << "PowerdownButtonClick";
    kernel_->SendShutdownBackend(true);
    sender->EventManager()->PushKernelEvent(KernelEvent::Shutdown);
}

void CarPC::HomeButtonClick(IGUIElement* sender) {
	UNUSED(sender);
	LOG(DEBUG) << "HomeButtonClick";
	appUiState_ = UiState::home;
	sender->EventManager()->PushApplicationEvent(AppEvent::ChangeUiState, nullptr, nullptr);
}

void CarPC::MapButtonClick(IGUIElement* sender) {
	UNUSED(sender);
	LOG(DEBUG) << "MapButtonClick";
	appUiState_ = UiState::map;
	sender->EventManager()->PushApplicationEvent(AppEvent::ChangeUiState, nullptr, nullptr);
}

void CarPC::PlayerButtonClick(IGUIElement* sender) {
	UNUSED(sender);
	appUiState_ = UiState::player;
	sender->EventManager()->PushApplicationEvent(AppEvent::ChangeUiState, nullptr, nullptr);
}

void CarPC::CamButtonClick(IGUIElement* sender) {
	appUiState_ = UiState::cam;
	sender->EventManager()->PushApplicationEvent(AppEvent::ChangeUiState, nullptr, nullptr);
}

void CarPC::SwitchButtonClick(IGUIElement* sender) {
	UNUSED(sender);
	appUiState_ = UiState::switches;
	sender->EventManager()->PushApplicationEvent(AppEvent::ChangeUiState, nullptr, nullptr);
}

void CarPC::SettingsButtonClick(IGUIElement* sender) {
	UNUSED(sender);
	appUiState_ = UiState::settings;
	sender->EventManager()->PushApplicationEvent(AppEvent::ChangeUiState, nullptr, nullptr);
}

void CarPC::PlayButtonClick(IGUIElement* sender) {
	LOG(INFO) << "PlayButtonClick";
	sender->EventManager()->PushApplicationEvent(AppEvent::PlayButtonClick, nullptr, nullptr);
}

void CarPC::EjectButtonClick(IGUIElement* sender) {
	LOG(INFO) << "EjectButtonClick";
    appUiState_ = UiState::mediathek;
    sender->EventManager()->PushApplicationEvent(AppEvent::ChangeUiState, nullptr, nullptr);
}

void CarPC::MapMenuButtonClick(IGUIElement* sender) {
    LOG(INFO) << "MapMenuButtonClick";
    sender->EventManager()->PushApplicationEvent(AppEvent::MapMenuOpen, nullptr, nullptr);
}

void CarPC::DrawUIPower() {
    try
    {
        if (lastScreen_ != nullptr) manager_->InvisibleElement(lastScreen_);

        GUIElement* popup = new GUITestElement(GUIPoint(35, 100), GUISize(900, 400), lightgray_t_color, "PowerPopup");
        manager_->AddElement(popup);
        auto test5 = new GUITextButton(GUIPoint(50, 10), GUISize(800, 50), "ShutdownButton", own_blue_color, lightgray_color);
        manager_->AddElement(popup, test5);
        test5->Text("Herunterfahren und ausschalten");
        auto ShutdownButtonClicklegate = std::bind(&CarPC::PowerdownButtonClick, this, std::placeholders::_1);
        test5->RegisterOnClick(ShutdownButtonClicklegate);

        auto test6 = new GUITextButton(GUIPoint(50, 70), GUISize(800, 50), "ExitButton", own_blue_color, lightgray_color);
        manager_->AddElement(popup, test6);
        test6->Text("Herunterfahren");

        auto CloseButtonClicklegate = std::bind(&CarPC::ShutdownButtonClick, this, std::placeholders::_1);
        test6->RegisterOnClick(CloseButtonClicklegate);

        auto test7 = new GUITextButton(GUIPoint(50, 130), GUISize(800, 50), "BackButton", own_blue_color, lightgray_color);
        manager_->AddElement(popup, test7);
        test7->Text(u8"Zurück zum Programm");

        auto CloseMeClicklegate = std::bind(&CarPC::CloseButtonClick, this, std::placeholders::_1);
        test7->RegisterOnClick(CloseMeClicklegate);
    }
    catch (std::exception &exp)
    {
        LOG(ERROR) << exp.what();
    }
}

void CarPC::DisableActionBar() const {
	auto element = manager_->GetElementByName("actionBar");
	if (element != nullptr) manager_->DisableElement(element);
}

void CarPC::EnbleActionBar() const {
	auto element = manager_->GetElementByName("actionBar");
	if (element != nullptr) manager_->EnableElement(element);
}

void CarPC::DrawUiHome() {
	if (lastScreen_ != nullptr) manager_->InvisibleElement(lastScreen_);

	auto element = manager_->GetElementByName("homeScreen");
	if (element != nullptr) {
		manager_->VisibleElement(element);
	} else {
		auto homeScreenTextlabel = new GUITextLabel(GUIPoint(30, 35), GUISize(904, 300), "homeScreen", lightgray_t_color, own_blue_color);
		manager_->AddElement(homeScreenTextlabel);
        homeScreenTextlabel->FontHeight(30);
		homeScreenTextlabel->Rotate(-8);
		homeScreenTextlabel->Text("No Widgets installed");
		element = homeScreenTextlabel;
	}
	lastScreen_ = element;
    LOG(DEBUG) << "lastScreen is set to homeScreen";
}

GUIElement* CarPC::BuildMapScreen(){
    auto mapScreen = new GUITestElement(GUIPoint(0, 0), GUISize(100, 100, sizeType::relative), transparent_white_color, "mapScreen");
    manager_->AddElement(mapScreen);
    mapScreen->Invisible();
    
    auto map = new GUIMapview(GUIPoint(0, 0), GUISize(100, 100, sizeType::relative), "map", accent_color, white_color);
    manager_->AddElement(mapScreen, map);
    map->Visible();
    
    auto menuButton = new GUIRoundPictureButton(GUIPoint(15, 550), GUISize(48, 48), "MapMenuButton", own_blue_color, white_color);
	manager_->AddElement(mapScreen, menuButton);
	menuButton->Image("map_menu.png");
	menuButton->Border(false);
	menuButton->PictureSize(GUISize(30, 30));
	menuButton->Visible();

    auto CloseMeClicklegate = std::bind(&CarPC::MapMenuButtonClick, this, std::placeholders::_1);
    menuButton->RegisterOnClick(CloseMeClicklegate);
    
    manager_->InvisibleElement(mapScreen);
    
    return mapScreen;
}

void CarPC::DrawUIMap() {
	if (lastScreen_ != nullptr) manager_->InvisibleElement(lastScreen_);

	auto element = manager_->GetElementByName("mapScreen");
	if (element != nullptr) {
		manager_->VisibleElement(element);
	} else {
        element = BuildMapScreen();
	}
	lastScreen_ = element;
}

GUIElement* CarPC::BuildPlayerScreen() {
    auto playerScreenBackground = new GUITestElement(GUIPoint(0, 0), GUISize(100, 100, sizeType::relative), transparent_white_color, "playerScreen");
    manager_->AddElement(playerScreenBackground);
    playerScreenBackground->Invisible();
    
	auto playButton = new GUIRoundPictureButton(GUIPoint(460, 500), GUISize(80, 80), "playButton", own_blue_color, black_color);
	manager_->AddElement(playerScreenBackground, playButton);
	playButton->Image("play_w.png");
	playButton->ImageSelected("stop_w.png");
	playButton->PictureSize(GUISize(61, 61));
	playButton->Border(false);
	playButton->Visible();

	auto playButtonDelegate = std::bind(&CarPC::PlayButtonClick, this, std::placeholders::_1);;
	playButton->RegisterOnClick(playButtonDelegate);

	auto ejectButton = new GUIRoundPictureButton(GUIPoint(100, 512), GUISize(48, 48), "ejectButton", own_blue_color, black_color);
	manager_->AddElement(playerScreenBackground, ejectButton);
	ejectButton->Image("eject_w.png");
	ejectButton->Border(false);
	ejectButton->PictureSize(GUISize(30, 30));
	ejectButton->Visible();

	auto ejectButtonDelegate = std::bind(&CarPC::EjectButtonClick, this, std::placeholders::_1);
	ejectButton->RegisterOnClick(ejectButtonDelegate);

	auto backwardButton = new GUIRoundPictureButton(GUIPoint(390, 510), GUISize(60, 60), "backwardButton", own_blue_color, black_color);
	manager_->AddElement(playerScreenBackground, backwardButton);
	backwardButton->Image("fast_rewind_w.png");
	backwardButton->Border(false);
	backwardButton->PictureSize(GUISize(41, 41));

	auto forwardButton = new GUIRoundPictureButton(GUIPoint(545, 510), GUISize(60, 60), "forwardButton", own_blue_color, black_color);
	manager_->AddElement(playerScreenBackground, forwardButton);
	forwardButton->Image("fast_forward_w.png");
	forwardButton->Border(false);
	forwardButton->PictureSize(GUISize(41, 41));

	auto playerInfoTextLabel = new GUITextLabel(GUIPoint(30, 30), GUISize(542, 100), "playerInfoTextLabel", lightgray_t_color, own_blue_color);
	playerInfoTextLabel->Text("kein Musik Titel gelanden");
	playerInfoTextLabel->FontHeight(20);
	manager_->AddElement(playerScreenBackground, playerInfoTextLabel);

	auto mediaDisplay = new GUIMediaDisplay(GUIPoint(30, 140), GUISize(904, 330), "mediaDisplay1", lightgray_t_color, own_blue_color);
	mediaDisplay->FontHeight(20);
	manager_->AddElement(playerScreenBackground, mediaDisplay);
	mediaDisplay->SetAudioManager(kernel_->GetAudioManager());

	manager_->InvisibleElement(playerScreenBackground);

	return playerScreenBackground;
}

void CarPC::DrawUIPlayer() {
	if (lastScreen_ != nullptr) manager_->InvisibleElement(lastScreen_);

	auto element = manager_->GetElementByName("playerScreen");
	if (element != nullptr) {
		manager_->VisibleElement(element);
	}
	else {
		element = BuildPlayerScreen();
	}
	lastScreen_ = element;
}

void CarPC::DrawUICam() {
	if (lastScreen_ != nullptr) manager_->InvisibleElement(lastScreen_);

	auto element = manager_->GetElementByName("camScreen");
	if (element != nullptr) {
		manager_->VisibleElement(element);
	}
	else {
		// If we call this the Main Switches are not workig because the transparen Background is over the butoons;
		element = camScreen_->Create();
	}
	lastScreen_ = element;
}


void CarPC::DrawUISwitches() {
	if (lastScreen_ != nullptr) manager_->InvisibleElement(lastScreen_);

	auto element = manager_->GetElementByName("switchesScreen");
	if (element != nullptr) {
		manager_->VisibleElement(element);
	}
	else {
		// If we call this the Main Switches are not workig because the transparen Background is over the butoons;
		element = switchesScreen_->Create();
	}
	lastScreen_ = element;
}


void CarPC::DrawUISettings() {
	if (lastScreen_ != nullptr) manager_->InvisibleElement(lastScreen_);

	auto element = manager_->GetElementByName("settingsScreen");
	if (element != nullptr) {
		manager_->VisibleElement(element);
	}
	else {
		auto homeScreenTextlabel = new GUITextLabel(GUIPoint(30, 35), GUISize(904, 300), "settingsScreen", lightgray_t_color, own_blue_color);
		manager_->AddElement(homeScreenTextlabel);
		homeScreenTextlabel->FontHeight(16);
		auto text = kernel_->GetConfigText();
		homeScreenTextlabel->Text(text);
		element = homeScreenTextlabel;
	}
	lastScreen_ = element;
}

void CarPC::TitelClick(IGUIElement* sender) {
	LOG(INFO) << "TitelClick";
	sender->EventManager()->PushApplicationEvent(AppEvent::TitelClick, nullptr, nullptr);
}

void CarPC::AlbenClick(IGUIElement* sender) {
	LOG(INFO) << "AlbenClick";
	sender->EventManager()->PushApplicationEvent(AppEvent::AlbenClick, nullptr, nullptr);
}

void CarPC::PlaylistClick(IGUIElement* sender) {
    LOG(INFO) << "PlaylistClick";
    sender->EventManager()->PushApplicationEvent(AppEvent::PlaylistClick, nullptr, nullptr);
}

void CarPC::FilelistClick(IGUIElement* sender) {
    LOG(INFO) << "FilelistClick";
    sender->EventManager()->PushApplicationEvent(AppEvent::FilelistClick, nullptr, nullptr);
}

void CarPC::SongListLongClick(IGUIElement* sender, int row, void* tag) {
    LOG(INFO) << "SongListLongClick";

    current_playlist_.current_pos = -1;
    current_playlist_.ids.clear();

    const auto info = _loadedMediaTitle[row];
    if (info != nullptr) {
        PlaySongId(sender, info->Id);
    }
}

void CarPC::PlaySongId(IGUIElement* sender, const int id) {
    auto file = mediaManager_->GetSourceForTitel(id);
    if (file.size() != 0) {
            currentMediaSource_ = file;
            if (playerState_ == MusikPlayerState::play) {
                    kernel_->StopMusik();
            }
            appUiState_ = UiState::player;
            UpdateUI();
            sender->EventManager()->PushApplicationEvent(AppEvent::PlayButtonClick, nullptr, nullptr);
    } else {
		current_playlist_.current_pos = -1;
        current_playlist_.ids.clear();
        LOG(ERROR) << "No filename get";
    }
}

void CarPC::AlbenListLongClick(IGUIElement* sender, int row, void* tag) {
    LOG(INFO) << "AlbenListLongClick";

    current_playlist_.current_pos = -1;
    current_playlist_.ids.clear();

    const auto info = _loadedMediaAlben[row];
    if (info != nullptr) {
        for(const auto &infoTitel: info->songList) {
                current_playlist_.ids.push_back(infoTitel.id);
        }
        current_playlist_.current_pos = 0;
        current_playlist_.is_running = true;
        PlaySongId(sender, current_playlist_.ids[0]);
    }
}

void CarPC::ListListviewLongClick(IGUIElement* sender, int row, void* tag) {
	LOG(INFO) << "ListListviewLongClick";
}

void CarPC::FileListviewLongClick(IGUIElement* sender, int row, void* tag) {
    LOG(INFO) << "FileListviewLongClick";

    current_playlist_.current_pos = -1;
    current_playlist_.ids.clear();

    const auto info = _loadedFileMediaTitle[row];
    if (info != nullptr) {
        currentMediaSource_ = info->MediaFile;
        if (playerState_ == MusikPlayerState::play) {
            kernel_->StopMusik();
        }
        appUiState_ = UiState::player;
        UpdateUI();
        sender->EventManager()->PushApplicationEvent(AppEvent::PlayButtonClick, nullptr, nullptr);
    }
}

GUIElement* CarPC::BuildMediathekScreen() {
	auto mediathekScreenBackground = new GUITestElement(GUIPoint(0, 0), GUISize(100, 100, sizeType::relative), accent_color, "mediathekScreen");
	manager_->AddElement(mediathekScreenBackground);
	mediathekScreenBackground->Invisible();

	auto element1 = new GUITextLabel(GUIPoint(30, 25), GUISize(150, 30), "mediathekLabelTitel", own_blue_color, white_color, false, own_blue_color, white_color);
	element1->TextAnchor(AnchorFlags::Left & AnchorFlags::Right);
	manager_->AddElement(mediathekScreenBackground, element1);
	element1->Text("Titel");
	element1->Select();
	auto titelClickdelegate = std::bind(&CarPC::TitelClick, this, std::placeholders::_1);
	element1->RegisterOnClick(titelClickdelegate);

	auto songListview = new GUIListview(GUIPoint(30, 55), GUISize(776, 508),"songListview", lightgray_t_color,lightblack_color);
	auto songlistLongClickdelegate = std::bind(&CarPC::SongListLongClick, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
	songListview->RegisterOnLongClick(songlistLongClickdelegate);
	manager_->AddElement(mediathekScreenBackground, songListview);

	auto element2 = new GUITextLabel(GUIPoint(182, 25), GUISize(150, 30), "mediathekLabelAlben", own_blue_color, white_color, true, own_blue_color, white_color);
    element2->TextAnchor(AnchorFlags::Left & AnchorFlags::Right);
	manager_->AddElement(mediathekScreenBackground, element2);
	element2->Text("Alben");
	auto AlbenClicklegate = std::bind(&CarPC::AlbenClick, this, std::placeholders::_1);
	element2->RegisterOnClick(AlbenClicklegate);

	auto albenListview = new GUIListview(GUIPoint(30, 55), GUISize(776, 508), "albenListview", lightgray_t_color, lightblack_color);
	auto albenListLongClickdelegate = std::bind(&CarPC::AlbenListLongClick, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
	albenListview->RegisterOnLongClick(albenListLongClickdelegate);
	manager_->AddElement(mediathekScreenBackground, albenListview);
	albenListview->Invisible();

	auto element3 = new GUITextLabel(GUIPoint(334, 25), GUISize(240, 30), "mediathekLabelListe", own_blue_color, white_color, true, own_blue_color, white_color);
    element3->TextAnchor(AnchorFlags::Left & AnchorFlags::Right);
	manager_->AddElement(mediathekScreenBackground, element3);
	element3->Text("Wiedergabe Listen");
    auto playlistClicklegate = std::bind(&CarPC::PlaylistClick, this, std::placeholders::_1);
	element3->RegisterOnClick(playlistClicklegate);

	auto listListview = new GUIListview(GUIPoint(30, 55), GUISize(776, 508), "ListListview", lightgray_t_color, lightblack_color);
	auto listListviewLongClickdelegate = std::bind(&CarPC::ListListviewLongClick, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
	listListview->RegisterOnLongClick(listListviewLongClickdelegate);
	manager_->AddElement(mediathekScreenBackground, listListview);
	listListview->Invisible();

    auto element4 = new GUITextLabel(GUIPoint(576, 25), GUISize(230, 30), "mediathekFileSource", own_blue_color, white_color, true, own_blue_color, white_color);
    element4->TextAnchor(AnchorFlags::Left & AnchorFlags::Right);
	manager_->AddElement(mediathekScreenBackground, element4);
	element4->Text("Audio Dateien");
    auto filelistClicklegate = std::bind(&CarPC::FilelistClick, this, std::placeholders::_1);
	element4->RegisterOnClick(filelistClicklegate);
        
    auto fileListview = new GUIListview(GUIPoint(30, 55), GUISize(776, 508), "fileListview", lightgray_t_color, lightblack_color);
	auto fileListviewLongClickdelegate = std::bind(&CarPC::FileListviewLongClick, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
	fileListview->RegisterOnLongClick(fileListviewLongClickdelegate);
	manager_->AddElement(mediathekScreenBackground, fileListview);
	fileListview->Invisible();

	auto test7 = new GUITextButton(GUIPoint(30, 565), GUISize(766, 30), "mediathekBackButton", own_blue_color, white_color);
	manager_->AddElement(mediathekScreenBackground, test7);
	test7->Text(u8"Zurück");

	auto CloseClicklegate = std::bind(&CarPC::CloseButtonClick, this, std::placeholders::_1);
	test7->RegisterOnClick(CloseClicklegate);

	return mediathekScreenBackground;
}

void CarPC::CheckUpdateMediathekScreen() {
    if (songlistNeedUpdate_) {

#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
	TIMED_SCOPE_IF(timerBlkObjNeedUpdate, "songlist update", VLOG_IS_ON(2));
#endif

        LOG(DEBUG) << "Songlist NeedUpdate";

        auto element = static_cast<GUIListview*>(manager_->GetElementByName("songListview"));
        if (element != nullptr) {
                //Todo Put it on Thread if is possible (renderer)
                element->BeginUpdate();
                element->Clear();
                _loadedMediaTitle = mediaManager_->GetAllMusikTitle(0);
                for(auto &resultEntry : _loadedMediaTitle) {
                        GUIListviewRow row;
                        std::shared_ptr<GUIListviewColumn> column(new GUIListviewColumn(resultEntry->Name));
                        column->SetDetailText(resultEntry->Album + " " + resultEntry->Artist);
                        column->SetImageData(resultEntry->smallImage, resultEntry->smallImageSize);
                        row.AddColumn(column);
                        row.Tag = resultEntry;
                        element->AddRow(row);
                }
                element->EndUpdate();
                LOG(DEBUG) << "Songlist Updated";
        }

        element = static_cast<GUIListview*>(manager_->GetElementByName("albenListview"));
        if (element != nullptr) {
                //Todo Put it on Thread if is possible (renderer)
                element->BeginUpdate();
                element->Clear();
                _loadedMediaAlben = mediaManager_->GetAllMusikAlben(0);
                for (auto &resultEntry : _loadedMediaAlben) {
                        GUIListviewRow row;
                        std::shared_ptr<GUIListviewColumn> column(new GUIListviewColumn(resultEntry->Name));
                        column->SetDetailText(resultEntry->Artist);
                        column->SetImageData(resultEntry->smallImage, resultEntry->smallImageSize);
                        row.AddColumn(column);
                        row.Tag = resultEntry;
                        element->AddRow(row);
                }
                element->EndUpdate();
                LOG(DEBUG) << "Albenlist Updated";
        }
        songlistNeedUpdate_ = false;
    }
    
    if(filelistNeedUpdate_) {
#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
	TIMED_SCOPE_IF(timerBlkObjNeedUpdate, "filelist update", VLOG_IS_ON(2));
#endif        
        auto element = static_cast<GUIListview*>(manager_->GetElementByName("fileListview"));
        if (element != nullptr) {
                //Todo Put it on Thread if is possible (renderer)
                element->BeginUpdate();
                element->Clear();
                _loadedFileMediaTitle = mediaManager_->GetFileMusikTitle(0);
                for(auto &resultEntry : _loadedFileMediaTitle) {
					    if(resultEntry->Name.size() == 0) {
							LOG(WARNING) << "FileMediaTitle in filelist is empty not shown";
							continue;
						}
                        GUIListviewRow row;
                        std::shared_ptr<GUIListviewColumn> column(new GUIListviewColumn(resultEntry->Name));
                        column->SetDetailText(resultEntry->Album + " " + resultEntry->Artist);
                        column->SetImageData(resultEntry->smallImage, resultEntry->smallImageSize);
                        row.AddColumn(column);
                        row.Tag = resultEntry;
                        element->AddRow(row);
                }
                element->EndUpdate();
                LOG(DEBUG) << "Filelist Updated";
        }
        filelistNeedUpdate_ = false;
    }
}

void CarPC::DrawUIMediathek() {
	if (lastScreen_ != nullptr) manager_->InvisibleElement(lastScreen_);

	auto element = manager_->GetElementByName("mediathekScreen");
	if (element != nullptr) {
		manager_->VisibleElement(element);
	}
	else {
		element = BuildMediathekScreen();
	}
	CheckUpdateMediathekScreen();
	lastScreen_ = element;
}

void CarPC::UpdateUI() {

    LOG(INFO) << "Change UI from " << appUiStateCurrent_ << " to " << appUiState_;

    if(lastPopup_ != nullptr) {
        lastPopup_->Close();
        delete lastPopup_;
        lastPopup_ = nullptr;
    }
        
	if(appUiState_ == appUiStateCurrent_) return;

	switch (appUiState_) {
		case UiState::home:
			DrawUiHome();
			EnbleActionBar();
			{
				auto homeButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("homeButton"));
				if (homeButton != nullptr) {
					homeButton->Select();
				}
				auto mapButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("mapButton"));
				if (mapButton != nullptr) {
					mapButton->Unselect();
				}
				auto playerButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("playerButton"));
				if (playerButton != nullptr) {
					playerButton->Unselect();
				}
				auto camButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("camButton"));
				if (camButton != nullptr) {
					camButton->Unselect();
				}
				auto switchesButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("switchesButton"));
				if (switchesButton != nullptr) {
					switchesButton->Unselect();
				}
				auto settingsButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("settingsButton"));
				if (settingsButton != nullptr) {
					settingsButton->Unselect();
				}
			}
			break;
		case UiState::map:
			DrawUIMap();
			EnbleActionBar();
			{
				auto homeButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("homeButton"));
				if (homeButton != nullptr) {
					homeButton->Unselect();
				}
				auto mapButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("mapButton"));
				if (mapButton != nullptr) {
					mapButton->Select();
				}
				auto playerButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("playerButton"));
				if (playerButton != nullptr) {
					playerButton->Unselect();
				}
				auto camButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("camButton"));
				if (camButton != nullptr) {
					camButton->Unselect();
				}
				auto switchesButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("switchesButton"));
				if (switchesButton != nullptr) {
					switchesButton->Unselect();
				}
				auto settingsButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("settingsButton"));
				if (settingsButton != nullptr) {
					settingsButton->Unselect();
				}
			}
			break;
		case UiState::player:
			DrawUIPlayer();
			EnbleActionBar();
			{
				auto homeButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("homeButton"));
				if (homeButton != nullptr) {
					homeButton->Unselect();
				}
				auto mapButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("mapButton"));
				if (mapButton != nullptr) {
					mapButton->Unselect();
				}
				auto playerButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("playerButton"));
				if (playerButton != nullptr) {
					playerButton->Select();
				}
				auto camButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("camButton"));
				if (camButton != nullptr) {
					camButton->Unselect();
				}
				auto switchesButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("switchesButton"));
				if (switchesButton != nullptr) {
					switchesButton->Unselect();
				}
				auto settingsButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("settingsButton"));
				if (settingsButton != nullptr) {
					settingsButton->Unselect();
				}
			}
			break;
		case UiState::cam:
			DrawUICam();
			EnbleActionBar();
			{
				auto homeButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("homeButton"));
				if (homeButton != nullptr) {
					homeButton->Unselect();
				}
				auto mapButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("mapButton"));
				if (mapButton != nullptr) {
					mapButton->Unselect();
				}
				auto playerButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("playerButton"));
				if (playerButton != nullptr) {
					playerButton->Unselect();
				}
				auto camButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("camButton"));
				if (camButton != nullptr) {
					camButton->Select();
				}
				auto switchesButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("switchesButton"));
				if (switchesButton != nullptr) {
					switchesButton->Unselect();
				}
				auto settingsButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("settingsButton"));
				if (settingsButton != nullptr) {
					settingsButton->Unselect();
				}
			}
			break;
		case UiState::switches:
			DrawUISwitches();
			EnbleActionBar();
			{
				auto homeButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("homeButton"));
				if (homeButton != nullptr) {
					homeButton->Unselect();
				}
				auto mapButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("mapButton"));
				if (mapButton != nullptr) {
					mapButton->Unselect();
				}
				auto playerButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("playerButton"));
				if (playerButton != nullptr) {
					playerButton->Unselect();
				}
				auto camButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("camButton"));
				if (camButton != nullptr) {
					camButton->Unselect();
				}
				auto switchesButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("switchesButton"));
				if (switchesButton != nullptr) {
					switchesButton->Select();
				}
				auto settingsButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("settingsButton"));
				if (settingsButton != nullptr) {
					settingsButton->Unselect();
				}
			}
			break;
		case UiState::settings:
			DrawUISettings();
			EnbleActionBar();
			{
				auto homeButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("homeButton"));
				if (homeButton != nullptr) {
					homeButton->Unselect();
				}
				auto mapButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("mapButton"));
				if (mapButton != nullptr) {
					mapButton->Unselect();
				}
				auto playerButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("playerButton"));
				if (playerButton != nullptr) {
					playerButton->Unselect();
				}
				auto camButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("camButton"));
				if (camButton != nullptr) {
					camButton->Unselect();
				}
				auto switchesButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("switchesButton"));
				if (switchesButton != nullptr) {
					switchesButton->Unselect();
				}
				auto settingsButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("settingsButton"));
				if (settingsButton != nullptr) {
					settingsButton->Select();
				}
			}
			break;
		case UiState::mediathek:
			DrawUIMediathek();
			DisableActionBar();
			{
				auto homeButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("homeButton"));
				if (homeButton != nullptr) {
					homeButton->Unselect();
				}
				auto mapButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("mapButton"));
				if (mapButton != nullptr) {
					mapButton->Unselect();
				}
				auto playerButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("playerButton"));
				if (playerButton != nullptr) {
					playerButton->Unselect();
				}
				auto camButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("camButton"));
				if (camButton != nullptr) {
					camButton->Unselect();
				}
				auto switchesButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("switchesButton"));
				if (switchesButton != nullptr) {
					switchesButton->Unselect();
				}
				auto settingsButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("settingsButton"));
				if (settingsButton != nullptr) {
					settingsButton->Unselect();
				}
			}
			break;
		case UiState::exit: 
			DrawUIPower();
			DisableActionBar();
			{
				auto homeButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("homeButton"));
				if (homeButton != nullptr) {
					homeButton->Unselect();
				}
				auto mapButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("mapButton"));
				if (mapButton != nullptr) {
					mapButton->Unselect();
				}
				auto playerButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("playerButton"));
				if (playerButton != nullptr) {
					playerButton->Unselect();
				}
				auto camButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("camButton"));
				if (camButton != nullptr) {
					camButton->Unselect();
				}
				auto switchesButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("switchesButton"));
				if (switchesButton != nullptr) {
					switchesButton->Unselect();
				}
				auto settingsButton = static_cast<GUIRoundPictureButton*>(manager_->GetElementByName("settingsButton"));
				if (settingsButton != nullptr) {
					settingsButton->Unselect();
				}
			}
			break;
		default: 
			LOG(ERROR) << "GuiState unknown";
	}
	appUiStateCurrent_ = appUiState_;
}

void CarPC::BuildFirstScreen() {
	// This mus be done to geht the right flow off Gui Find a better way
    BuildPlayerScreen();
    BuildMediathekScreen();
    BuildMapScreen();
	switchesScreen_ = new SwitchesScreen(manager_,  kernel_->GetConfigManager());
	switchesScreen_->Create();
	camScreen_ = new CamScreen(manager_);
	camScreen_->Create();
	
    //const auto actionBar = new GUITestElement(GUIPoint(964, 0), GUISize(60, 600), lightgray_t_color, "actionBar");
	const auto actionBar = new GUITestElement(GUIPoint(964, 0), GUISize(6, 100, sizeType::relative), lightgray_t_color, "actionBar");
    manager_->AddElement(actionBar);
    actionBar->Anchor(AnchorFlags::Left, AnchorFlags::Right);

	//auto statusLine = new GUITestElement(GUIPoint(0, 0), GUISize(964, 26), lightgray_t_color, "statusLine");
    auto statusLine = new GUITestElement(GUIPoint(0, 0), GUISize(94, 4, sizeType::relative), lightgray_t_color, "statusLine");
    manager_->AddElement(statusLine);
    //statusLine->Transparent();
	statusLine->Anchor(AnchorFlags::Right, AnchorFlags::Left);

    auto uhrTextlabel = new GUITextLabel(GUIPoint(300, 0), GUISize(11, 100, sizeType::relative), "Uhrzeit", transparent_color, own_blue_color);
    manager_->AddElement(statusLine, uhrTextlabel);
    uhrTextlabel->Text("Hallo");
    uhrTextlabel->ShowTime(true);
    uhrTextlabel->Anchor(AnchorFlags::Left, AnchorFlags::Right);
    uhrTextlabel->TextAnchor(AnchorFlags::Left & AnchorFlags::Right);

    auto playerStateIcon = new GUIIconview(GUIPoint(0, 0), GUISize(26, 26), "PlayerStateIcon", transparent_color);
    manager_->AddElement(statusLine, playerStateIcon);
    playerStateIcon->SetCurrentIcon("cd-pause.png");

    auto backendStateIcon = new GUIIconview(GUIPoint(26, 0), GUISize(26, 26), "BackendStateIcon", transparent_color);
    manager_->AddElement(statusLine, backendStateIcon);
    backendStateIcon->SetCurrentIcon("service_offline.png");

    auto gpsStateIcon = new GUIIconview(GUIPoint(52, 0), GUISize(26, 26), "GPSStateIcon", transparent_color);
    manager_->AddElement(statusLine, gpsStateIcon);
    gpsStateIcon->SetCurrentIcon("gps_offline.png");

    auto homeButton = new GUIRoundPictureButton(GUIPoint(5, 30), GUISize(50, 50), "homeButton", lightgray_color, black_color);
    manager_->AddElement(actionBar, homeButton);
    homeButton->PictureSize(GUISize(32, 32));
    homeButton->Image("home_b.png");
    homeButton->ImageSelected("home_w.png");
    homeButton->ImageDisable("home_disable.png");
    homeButton->ImageBackground("ButtonBackground_w.png");
    homeButton->ImageSelectedBackground("ButtonBackground.png");
    homeButton->Border(false);

    auto homeButtondelegate = std::bind(&CarPC::HomeButtonClick, this, std::placeholders::_1);
    homeButton->RegisterOnClick(homeButtondelegate);

    auto mapButton = new GUIRoundPictureButton(GUIPoint(5, 90), GUISize(50, 50), "mapButton", lightgray_color, black_color);
    manager_->AddElement(actionBar, mapButton);
    mapButton->Image("map_b.png");
    mapButton->ImageSelected("map_w.png");
    mapButton->ImageDisable("map_disable.png");
    mapButton->ImageBackground("ButtonBackground_w.png");
    mapButton->ImageSelectedBackground("ButtonBackground.png");
    mapButton->Border(false);

    auto mapButtondelegate = std::bind(&CarPC::MapButtonClick, this, std::placeholders::_1);
    mapButton->RegisterOnClick(mapButtondelegate);

    auto playerButton = new GUIRoundPictureButton(GUIPoint(5, 150), GUISize(50, 50), "playerButton", lightgray_color, black_color);
    manager_->AddElement(actionBar, playerButton);
    playerButton->Image("note_b.png");
    playerButton->ImageSelected("note_w.png");
    playerButton->ImageDisable("note_disable.png");
    playerButton->ImageBackground("ButtonBackground_w.png");
    playerButton->ImageSelectedBackground("ButtonBackground.png");
    playerButton->Border(false);

    auto playerButtondelegate = std::bind(&CarPC::PlayerButtonClick, this, std::placeholders::_1);
    playerButton->RegisterOnClick(playerButtondelegate);

	auto camButton = new GUIRoundPictureButton(GUIPoint(5, 210), GUISize(50, 50), "camButton", lightgray_color, black_color);
    manager_->AddElement(actionBar, camButton);
    camButton->Image("cam_b.png");
    camButton->ImageSelected("cam_w.png");
    camButton->ImageDisable("settings_disable.png");
    camButton->ImageBackground("ButtonBackground_w.png");
    camButton->ImageSelectedBackground("ButtonBackground.png");
    camButton->Border(false);

    auto camButtondelegate = std::bind(&CarPC::CamButtonClick, this, std::placeholders::_1);
    camButton->RegisterOnClick(camButtondelegate);

	auto switchesButton = new GUIRoundPictureButton(GUIPoint(5, 270), GUISize(50, 50), "switchesButton", lightgray_color, black_color);
    manager_->AddElement(actionBar, switchesButton);
    switchesButton->Image("switches_b.png");
    switchesButton->ImageSelected("switches_w.png");
    switchesButton->ImageDisable("settings_disable.png");
    switchesButton->ImageBackground("ButtonBackground_w.png");
    switchesButton->ImageSelectedBackground("ButtonBackground.png");
    switchesButton->Border(false);

    auto switchButtondelegate = std::bind(&CarPC::SwitchButtonClick, this, std::placeholders::_1);
    switchesButton->RegisterOnClick(switchButtondelegate);

    auto settingsButton = new GUIRoundPictureButton(GUIPoint(5, 480), GUISize(50, 50), "settingsButton", lightgray_color, black_color);
    manager_->AddElement(actionBar, settingsButton);
    settingsButton->Image("settings_b.png");
    settingsButton->ImageSelected("settings_w.png");
    settingsButton->ImageDisable("settings_disable.png");
    settingsButton->ImageBackground("ButtonBackground_w.png");
    settingsButton->ImageSelectedBackground("ButtonBackground.png");
    settingsButton->Border(false);

    auto settingsButtondelegate = std::bind(&CarPC::SettingsButtonClick, this, std::placeholders::_1);
    settingsButton->RegisterOnClick(settingsButtondelegate);

    auto powerButton = new GUIRoundPictureButton(GUIPoint(5, 540), GUISize(50, 50), "powerButton", lightgray_color, black_color);
    manager_->AddElement(actionBar, powerButton);
    powerButton->Image("power_b.png");
    powerButton->ImageSelected("power_w.png");
    powerButton->ImageDisable("power_disable.png");
    powerButton->ImageBackground("ButtonBackground_w.png");
    powerButton->ImageSelectedBackground("ButtonBackground.png");
    powerButton->Border(false);

    auto button1delegate = std::bind(&CarPC::PowerButtonClick, this, std::placeholders::_1);
    powerButton->RegisterOnClick(button1delegate);

#ifdef _DEBUG
    manager_->PrintVisualTree();
#endif

    appUiState_ = UiState::home;
    playerState_ = MusikPlayerState::stop;
    currentMediaSource_ = "test.mp3";
    UpdateUI();
    
}

void CarPC::ShowMapMenu() {
    auto mapMenuButton = manager_->GetElementByName("MapMenuButton");
    mapMenuButton->Invisible();
    auto mapMenuDialog =  new MapMenuDialog(manager_);
    mapMenuDialog->Create( [this]() { 
            auto mapMenuButton = manager_->GetElementByName("MapMenuButton");
            mapMenuButton->Visible();
        });
    lastPopup_ = mapMenuDialog;
}

CarPC::CarPC(Kernel* kernel):
    manager_(nullptr),
    appUiState_(UiState::undefined),
    appUiStateCurrent_(UiState::undefined),
    playerState_(MusikPlayerState::undefined),
    lastScreen_(nullptr), 
    mediaManager_(nullptr),
    lastPopup_(nullptr) {

    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);

    if (kernel == nullptr) {
            throw NullPointerException("kernel can not be null");
    }
    kernel_ = kernel;
    songlistNeedUpdate_ = true;
    filelistNeedUpdate_ = true;
}

int CarPC::Startup() {
    auto statedelegate = std::bind(&CarPC::KernelstateChanged, this, std::placeholders::_1);
    kernel_->SetStateCallBack(statedelegate);
    auto eventdelegate = std::bind(&CarPC::ApplicationEvent, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    kernel_->RegisterApplicationEvent(eventdelegate);
    return 0;
}

void CarPC::Shutdown() {
    for (auto &resultEntry : _loadedMediaTitle) {
            delete resultEntry;
    }
    _loadedMediaTitle.clear();

    for (auto &resultEntry : _loadedMediaAlben) {
            delete resultEntry;
    }
    _loadedMediaAlben.clear();
        
    for (auto &resultEntry : _loadedFileMediaTitle) {
        delete resultEntry;
    }
    _loadedMediaAlben.clear();
        
}
