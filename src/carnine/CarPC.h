#pragma once
#ifndef CANINE_SRC_CARPC
#define CANINE_SRC_CARPC
#include "framework/media/MediaManager.h"
#include "SwitchesScreen.h"
#include "CamScreen.h"

enum class AppEvent;
class IGUIElement;
enum class KernelState : unsigned char;
class Kernel;
class GUIElementManager;
class GUIElement;
class MediaManager;
class IPopupDialog;

enum class UiState : unsigned char {
	undefined,
	home,
	map,
	player,
	settings,
	exit,
	mediathek,
	cam,
	switches
};

enum class MusikPlayerState : unsigned char {
	undefined,
	loading,
	play,
	pause,
	stop
};

typedef struct CurrentPlaylist_ {
	std::string name;
	std::vector<int> ids;
	int current_pos;
	bool is_running;
} CurrentPlaylist;

class CarPC {
	Kernel* kernel_;
	GUIElementManager* manager_;
	UiState appUiState_;
	UiState appUiStateCurrent_;
	MusikPlayerState playerState_;
	GUIElement* lastScreen_;
	MediaManager* mediaManager_;
	SwitchesScreen* switchesScreen_;
	CamScreen* camScreen_;
	bool songlistNeedUpdate_;
    bool filelistNeedUpdate_;
	std::vector<MediaTitleInfo_*> _loadedMediaTitle;
	std::vector<MediaAlbenInfo*> _loadedMediaAlben;
    std::vector<MediaTitleInfo_*> _loadedFileMediaTitle;
	std::string currentMediaSource_;
	CurrentPlaylist current_playlist_;
    IPopupDialog* lastPopup_;
    
	void StartAudio() const;
	void StartServices() const;
	void KernelstateChanged(KernelState state);
	void ShowErrorMessage(const std::string& message);
	void ApplicationEvent(AppEvent event, void* data1, void* data2);
	void Button1LongClick(IGUIElement* sender);
	void ShutdownButtonClick(IGUIElement* sender);
	void CloseButtonClick(IGUIElement* sender);
	void PowerdownButtonClick(IGUIElement* sender);

	//Actionbar Buttons
	void HomeButtonClick(IGUIElement* sender);
	void MapButtonClick(IGUIElement* sender);
	void PlayerButtonClick(IGUIElement* sender);
	void CamButtonClick(IGUIElement* sender);
	void SwitchButtonClick(IGUIElement* sender);
	void SettingsButtonClick(IGUIElement* sender);
	void PowerButtonClick(IGUIElement* sender);

	//Player Buttons
	void PlayButtonClick(IGUIElement* sender);
	void EjectButtonClick(IGUIElement* sender);

    void MapMenuButtonClick(IGUIElement* sender);
    
	void DrawUIPower();
	void DisableActionBar() const;
	void EnbleActionBar() const;
	void DrawUiHome();
    GUIElement* BuildMapScreen();
	void DrawUIMap();
	GUIElement* BuildPlayerScreen();
	void DrawUIPlayer();
	void DrawUICam();
	void DrawUISwitches();
	void DrawUISettings();
	void TitelClick(IGUIElement* sender);
	void AlbenClick(IGUIElement* sender);
	void PlaylistClick(IGUIElement* sender);
    void FilelistClick(IGUIElement* sender);
	void SongListLongClick(IGUIElement* sender, int row, void* tag);
	void PlaySongId(IGUIElement* sender, const int id);
	void AlbenListLongClick(IGUIElement* sender, int row, void* tag);
	void ListListviewLongClick(IGUIElement* sender, int row, void* tag);
    void FileListviewLongClick(IGUIElement* sender, int row, void* tag);
	GUIElement* BuildMediathekScreen();
	void CheckUpdateMediathekScreen();
	void DrawUIMediathek();
	void UpdateUI();
	void BuildFirstScreen();
    void ShowMapMenu();
public:
	explicit CarPC(Kernel* kernel);

	int Startup();
	void Shutdown();
};

#endif  //CANINE_SRC_CARPC
