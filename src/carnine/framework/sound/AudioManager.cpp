/**
* @file  AudioManager.cpp
*
* Implementation for Manager Audio Subsystem
*
* @date  2015-04-13
*
* https://sourceforge.net/p/karlyriceditor/code/HEAD/tree/src/audioplayerprivate.cpp
* http://stackoverflow.com/questions/21342194/sdl2-ffmpeg2-intermittent-clicks-instead-of-audio
* https://transcoding.wordpress.com/2011/11/16/careful-with-audio-resampling-using-ffmpeg/
* http://dranger.com/ffmpeg/
* http://ffmpeg.zeranoe.com/builds/ geht inculde an so on
* https://github.com/podshumok/libav-stream-plusplus/blob/master/stream_transcoder.cpp
* lexical_cast -> boost libary
*/

#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "AudioManager"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#ifdef NVWAMEMCHECK
#	include "../../../common/nvwa/debug_new.h"
#endif

#include "../../../common/utils/easylogging++.h"
#include "../../../common/commonutils.h"
#include "audio.h"
#include "AudioManager.h"
#include "../SDLException.h"
#include "MediaStream.h"
#include "../AppEvents.h"

#define FADING_TIME_MS 800

static void avlog_cb(void* ptr, int level, const char * fmt, va_list arg_list) {
    static std::string lastout;
	std::string prefix;

    //if(lastout == "") {
        auto avc = ptr ? *reinterpret_cast<AVClass**>(ptr) : nullptr;
        if (avc)
        {
            /*if (avc->parent_log_context_offset)
            {
                AVClass** parent = *reinterpret_cast<AVClass ***> (ptr) +
                    avc->parent_log_context_offset;
                if (parent && *parent)
                {
                    prefix += '[';
                    prefix += (*parent)->class_name;
                    prefix += ' ';
                    prefix += (*parent)->item_name(parent);
                    prefix += " @ ";
                    //prefix += lexical_cast<std::string>(parent);
                    prefix += "] ";
                }
            }*/
            prefix += '[';
            prefix += avc->class_name;
            prefix += ' ';
            prefix += avc->item_name(ptr);
            //prefix += " @ ";
            //prefix += lexical_cast<std::string>(ptr);
            prefix += "] ";
        }
    //}
    
    lastout += prefix + utils::stringf(fmt, arg_list).get();

	const auto found = lastout.find("\n") != std::string::npos;
   	if(!found) return;

    std::replace( lastout.begin(), lastout.end(), '\n', '*');
    std::replace( lastout.begin(), lastout.end(), '\r', '#');
	lastout = "avlog " + lastout;
    
	//Todo splitt/remove newline in log entry
	switch (level)
	{
	case AV_LOG_PANIC:
	case AV_LOG_FATAL:
		LOG(FATAL) << lastout;
		break;
	case AV_LOG_ERROR:
		LOG(ERROR) << lastout;
		break;
	case AV_LOG_WARNING:
		LOG(WARNING) << lastout;
		break;
	case AV_LOG_INFO:
		LOG(INFO) << lastout;
		break;
	case AV_LOG_VERBOSE:
		VLOG(3) << lastout;
		break;
	case AV_LOG_DEBUG:
		LOG(DEBUG) << lastout;
		break;
	default:
		LOG(INFO) << lastout;
	}
    lastout = "";
}

static void MusikMixerCallback(void *udata, Uint8 *stream, int len) {
	auto mixer = static_cast<AudioManager*>(udata);
	mixer->MixerCallback(stream, len);
}

static int decode_thread_static(void *arg){
    auto mixer = static_cast<AudioManager*>(arg);
    return mixer->DecoderThreadMain();
}

int AudioManager::DecoderThreadMain() {
    auto state = StreamStates::error;
    LOG(DEBUG) << "decoder thread started";
    
	current_musik_stream_ = new MediaStream();
    auto format = AV_SAMPLE_FMT_S32;
    if((hardwareFormat_ & 0xFF) == 16) {
        format = AV_SAMPLE_FMT_S16;
    }
	const auto result = current_musik_stream_->Init(musikfile_, format, hardwareChannels_, audioBufferSize_, hardwareRate_);
	if (result < 0) {
        delete current_musik_stream_;
        current_musik_stream_ = nullptr;
        
        LOG(ERROR) << "Error Loading Musik File";
        stopMedia_ = true;
        eventManager_->PushApplicationEvent(AppEvent::MusikStreamError, nullptr, nullptr);
		LOG(DEBUG) << "decoder thread stopped";
		return result;
	}
	
	Mix_HookMusic(&MusikMixerCallback, this);

	const auto streamInfo = new AudioStreamInfo();
	current_musik_stream_->GetMetaData(streamInfo);
	
	eventManager_->PushApplicationEvent(AppEvent::MusikStreamPlay, streamInfo, nullptr);

	while (!stopMedia_)
	{
		// seek stuff goes here
		//if (is->seek_req) {
		//current_musik_stream_->Seek(seek_pos);
		//is->seek_req = 0;
		//}

		state = current_musik_stream_->FillStreams();
		if (state == StreamStates::bufferfull) {
			//SDL_Delay(10); We wait in FillStreams now 
		}
		if (state == StreamStates::allloaded) {
			LOG(DEBUG) << "allloaded";
            //Todo find an way without delay
            SDL_Delay(10);
		}
		if (state == StreamStates::finish) {
			break;
		}
		if (state == StreamStates::error) {
			LOG(ERROR) << "Error in FillStreams";
			break;
		}
	}
    
    LOG(DEBUG) << "decoder thread exit loop";
    
	Mix_HookMusic(nullptr, nullptr);

	delete current_musik_stream_;
	current_musik_stream_ = nullptr;
	
	if (mixDataBufferSize_ > 0) {
		delete[] mixDataBuffer_;
		mixDataBufferSize_ = 0;
		mixDataBuffer_ = nullptr;
	}
    
	stopMedia_ = true;
	if(state == StreamStates::error) {
        if(!eventManager_->PushApplicationEvent(AppEvent::MusikStreamError, nullptr, nullptr)) { 
            LOG(ERROR) << "PushEvent failed";
        }
	} else {
        if(!eventManager_->PushApplicationEvent(AppEvent::MusikStreamStopp, nullptr, nullptr)) {
            LOG(ERROR) << "PushEvent failed";
        }
	}
    
    LOG(DEBUG) << "decoder thread stopped";
    
	return 0;
}

void AudioManager::StopMusik() {
	if (!stopMedia_) {
		stopMedia_ = true;
		int ret;
		SDL_WaitThread(musikdecoderthread_, &ret);
        musikdecoderthread_ = nullptr;
	}
}

AudioManager::AudioManager(const SDLEventManager* eventManager,const int musikVolume):
	hardwareChannels_(2),
	audioBufferSize_(0),
	hardwareFormat_(AUDIO_S16SYS), //AUDIO_S32SYS
	hardwareRate_(HW_SAMPE_RATE),
	musikVolume_(musikVolume),
	current_musik_stream_(nullptr),
	mixDataBuffer_(nullptr),
	mixDataBufferSize_(0),
	stopMedia_(true),
	musikdecoderthread_(nullptr), ms_per_step_(0) {
	logger_ = el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
	initDone_ = false;
	eventManager_ = eventManager;
    stream_state_.fading = MIX_NO_FADING;
}

AudioManager::~AudioManager() {
	if (initDone_) {
		Mix_CloseAudio();
		initDone_ = false;
	}
}

int AudioManager::Init() {
	LOG(DEBUG) << "Init Audio Manager";
               
	av_log_set_callback(avlog_cb);

#ifdef _DEBUG
	av_log_set_level(AV_LOG_DEBUG);
	//Todo get it from Config
#endif
#if ( LIBAVFORMAT_VERSION_INT < AV_VERSION_INT(58,9,100) )
	avcodec_register_all();
	av_register_all();
#endif
	avformat_network_init();

	SDL_version compiled;

	MIX_VERSION(&compiled);
	const auto linked = *Mix_Linked_Version();

	LOG(INFO) << "SDL_mixer Version compiled " << static_cast<int>(compiled.major) << "." << static_cast<int>(compiled.minor) << "." << static_cast<int>(compiled.patch);
	LOG(INFO) << "SDL_mixer Version linked " << static_cast<int>(linked.major) << "." << static_cast<int>(linked.minor) << "." << static_cast<int>(linked.patch);
	
	audioBufferSize_ = SDL_AUDIO_BUFFER_SIZE;
	
	/*if(Mix_Init(MIX_INIT_MP3) == 0) {
		
	}*/

	//Todo Device form Config (Multi SoundDevices)?
	//Mix_OpenAudioDevice(hardwareRate_, hardwareFormat_, hardwareChannels_, audioBufferSize_, nullptr, SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_CHANNELS_CHANGE)
	//Mix_OpenAudio(hardwareRate_, hardwareFormat_, hardwareChannels_, audioBufferSize_)
	if (Mix_OpenAudioDevice(hardwareRate_, hardwareFormat_, hardwareChannels_, audioBufferSize_, nullptr, SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_CHANNELS_CHANGE) < 0) {
		throw SDLException("Mix_OpenAudio");
	}
	initDone_ = true;

	if (Mix_QuerySpec(&hardwareRate_, &hardwareFormat_, &hardwareChannels_) != 1) {
		throw SDLException("Mix_QuerySpec");
	}
	const std::string soundText = (hardwareChannels_ > 2) ? "surround" : (hardwareChannels_ > 1) ? "stereo" : "mono";
	const std::string formatText = (hardwareFormat_ & 0x1000) ? " BE" : " LE";
	const std::string formatText2 = (hardwareFormat_ & 0x8000) ? " signed" : " unsigned";

	LOG(INFO) << "Opened Audio with " << hardwareRate_ << "Hz " << (hardwareFormat_ & 0xFF) << "bit " << soundText << formatText << formatText2 << " Buffersize " << audioBufferSize_;

    const char* driver_name = SDL_GetCurrentAudioDriver();
    if (driver_name) {
        LOG(INFO) << "Audio subsystem initialized; driver = " << driver_name;
    } else {
        LOG(ERROR) << "Audio subsystem not initialized.";
    }

	const auto n = Mix_GetNumMusicDecoders();
    LOG(INFO) << "There are " << n << " available music decoders (SDL2_mixer):";
    for(auto i=0; i<n; ++i) {
        LOG(INFO) << Mix_GetMusicDecoder(i);
    }
    	
	Mix_VolumeMusic(musikVolume_);

    int channelCount = Mix_AllocateChannels(4);
    if(channelCount != 4) {
        LOG(WARNING) << "we get not all Mixer Channles";
    }
    
    Callback<void(int)>::func = std::bind(&AudioManager::BackgroundChannelDone, this, std::placeholders::_1);
	void(*ChannelDone)(int) = static_cast<decltype(ChannelDone)>(Callback<void(int)>::callback);
	Mix_ChannelFinished(ChannelDone);
    
    ms_per_step_ = static_cast<int>(static_cast<float>(audioBufferSize_) * 1000.0 / hardwareRate_);
    
	return 0;
}

void AudioManager::Shutdown() {
    LOG(DEBUG) << "Shutdown";
    
	if (!stopMedia_) {
		stopMedia_ = true;
        int ret;
        LOG(DEBUG) << "Wait decoder Thread";
        SDL_WaitThread(musikdecoderthread_,&ret);
	}
	if(mixDataBufferSize_ > 0) {
		delete[] mixDataBuffer_;
		mixDataBufferSize_ = 0;
	}
	if (initDone_) {
		Mix_CloseAudio();
		initDone_ = false;
	}
}

int AudioManager::PlayMusik(const std::string& filename) {
    LOG(INFO) << "PlayMusik " << filename;
    
    if (!initDone_) return -9;

    if(!stopMedia_) {
        LOG(DEBUG) << "we are bussy we can only play one musik file at one time";
        return -1;
    }
    
    if(musikdecoderthread_ != nullptr) {
        int ret;
        LOG(DEBUG) << "Wait decoder Thread";
        SDL_WaitThread(musikdecoderthread_,&ret);
    }
    
    musikfile_ = filename;
    stopMedia_ = false;
	stream_state_.fade_step = 0;
	stream_state_.fade_steps = 0;
	stream_state_.fading = MIX_NO_FADING;
    musikdecoderthread_ = SDL_CreateThread(decode_thread_static, "mixerdecoder", this);
    
    LOG(DEBUG) << "Decoder Thread Started " << filename;
    return 0;
}

void AudioManager::MixerCallback(Uint8 *stream, const int len) {
	//Todo is wenn Fadeout the Fade flg ist set back but the Stream sends new packet we have an Blob Sound
	//SDL_memset(stream, 0, len) SDL_Mixer do this
	if (current_musik_stream_ && current_musik_stream_->IsReady()) {

		if (mixDataBufferSize_ < len) {
			if (mixDataBuffer_ != nullptr) delete[] mixDataBuffer_;
			mixDataBuffer_ = new Uint8[len];
			mixDataBufferSize_ = len;
		}
		auto musikCurrentVolume = musikVolume_;

		current_musik_stream_->GetAudioData(mixDataBuffer_, len);
		
		if (stream_state_.fading != MIX_NO_FADING) {
            const auto fadeStep = stream_state_.fade_step;
			const auto fadeSteps = stream_state_.fade_steps;
            
            VLOG(4) << "Fading " << stream_state_.fading << " " << fadeStep << " " << fadeSteps;
            
			if (stream_state_.fading == MIX_FADING_OUT) {
				musikCurrentVolume = (musikVolume_ * (fadeSteps - fadeStep)) / fadeSteps;
			}
			else { /* Fading in */
				musikCurrentVolume = (musikVolume_ * fadeStep) / fadeSteps;
			}
            
            VLOG(4) << "Fading " << stream_state_.fading << " " << fadeStep << " " << fadeSteps << " " << musikCurrentVolume;
             
			if (stream_state_.fade_step++ < stream_state_.fade_steps) {

			}
			else {
				if (stream_state_.fading == MIX_FADING_OUT) {
					if (stream_state_.pause_at_end) {
						current_musik_stream_->Pause();
					} else {
						stopMedia_ = true;
					}
				}
				stream_state_.fading = MIX_NO_FADING;
			}
		} 

		SDL_MixAudioFormat(stream, mixDataBuffer_, hardwareFormat_, len, musikCurrentVolume);
		const auto lastError = SDL_GetError();
		if (strlen(lastError) > 0) {
			LOG(ERROR) << "SDL_MixAudioFormat " << lastError;
			SDL_ClearError();
		}
	}
}

int AudioManager::PlayBackground(const std::string& fileName) {
	auto current_chunk = Mix_LoadWAV(fileName.c_str());
	if (current_chunk == nullptr) {
		LOG(ERROR) << "Couldn't load file: " << Mix_GetError();
		return -1;
	}

    int result;
    if(!stopMedia_) {
        FadeOutMusic(FADING_TIME_MS, true);
        result = Mix_PlayChannel(-1, current_chunk, 0);
    } else {
        result = Mix_FadeInChannel(-1, current_chunk, 0, FADING_TIME_MS);
    }
	// -1 means search free channel we have 4 see init
    // if 'loops' is greater than zero, loop the sound that many times.
    // If 'loops' is -1, loop inifinitely (~65000 times).
	if (result == -1) {
		// may be critical error, or maybe just no channels were free.
		// you could allocated another channel in that case...
		LOG(ERROR) << "Mix_FadeInChannel: " << Mix_GetError();
		return -1;
	}

	chunk_[result] = current_chunk;

	return 0;
}

bool AudioManager::UpdateUi(GUIRenderer* renderer, GUITexture* screen) const {
	if (current_musik_stream_ && current_musik_stream_->IsReady()) {
		return current_musik_stream_->UpdateUi(renderer, screen);
	}
	return false;
}

void AudioManager::PauseMusik() const {
	if (current_musik_stream_ && current_musik_stream_->IsReady()) {
		current_musik_stream_->Pause();
	}
}

void AudioManager::GetMediaPlayTimes(int64_t* totalTime, int64_t* currentTime) const {
	if (current_musik_stream_ && current_musik_stream_->IsReady()) {
		current_musik_stream_->GetPlayTimes(totalTime, currentTime);
	}
}

void AudioManager::MusikVolumeUp() {
	if (musikVolume_ + 5 <= SDL_MIX_MAXVOLUME) {
		musikVolume_ = musikVolume_ + 5;
	} else {
		musikVolume_ = SDL_MIX_MAXVOLUME;
	}
	LOG(DEBUG) << "Set Volume to " << std::to_string(musikVolume_);
}

void AudioManager::MusikVolumeDown() {
	if (musikVolume_ - 5 >= 0) {
		musikVolume_ = musikVolume_ - 5;
	} else {
		musikVolume_ = 0;
	}
	LOG(DEBUG) << "Set Volume to " << std::to_string(musikVolume_);
}

void AudioManager::FadeOutMusic(const int msec, const bool pauseAtEnd) {
	const auto fadeSteps = (msec + ms_per_step_ - 1) / ms_per_step_;
	if (stream_state_.fading == MIX_NO_FADING) {
		stream_state_.fade_step = 0;
	}
	else {
		int step;
		const auto oldFadeSteps = stream_state_.fade_steps;
		if (stream_state_.fading == MIX_FADING_OUT) {
			step = stream_state_.fade_step;
		}
		else {
			step = oldFadeSteps - stream_state_.fade_step + 1;
		}
		stream_state_.fade_step = step * fadeSteps / oldFadeSteps;
	}

	stream_state_.fading = MIX_FADING_OUT;
	stream_state_.fade_steps = fadeSteps;
	stream_state_.pause_at_end = pauseAtEnd;
}

void AudioManager::FadeInMusic(int msec) {
    const auto fading = stream_state_.fading;
    const auto fadeSteps = (msec + ms_per_step_ - 1) / ms_per_step_;
	if (stream_state_.fading == MIX_NO_FADING) {
		stream_state_.fade_step = 0;
	}
	else {
		int step;
		const auto oldFadeSteps = stream_state_.fade_steps;
		if (stream_state_.fading == MIX_FADING_OUT) {
			step = (fadeSteps - stream_state_.fade_step) + 2;
		}
		else {
			step = oldFadeSteps - stream_state_.fade_step + 1;
		}
		stream_state_.fade_step = step * fadeSteps / oldFadeSteps;
	}

	stream_state_.fading = MIX_FADING_IN;
	stream_state_.fade_steps = fadeSteps;
    if(fading == MIX_NO_FADING) {
       current_musik_stream_->Resume();
    }
}

void AudioManager::BackgroundChannelDone(const int channel){
	LOG(DEBUG) << "BackgroundChannelDone " << channel ;
	Mix_FreeChunk(chunk_[channel]);
	if(current_musik_stream_!= nullptr && stream_state_.pause_at_end) {
       FadeInMusic(FADING_TIME_MS);
    }
}

int AudioManager::GetVolume() const {
	return musikVolume_;
}