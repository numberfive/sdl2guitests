#pragma once
#include "../../common/utils/easylogging++.h"
#include "../CarPC.h"

el::base::type::StoragePointer sharedLoggingRepository();

#include "carpckernel.h"

class AudioManager;
class ConfigManager;
class MediaManager;
class DatabaseManager;
class GUIScreen;
class GUIElementManager;
class MapManager;
class BackendConnector;

typedef std::function<void(KernelState state)> KernelStateCallbackFunction;
typedef std::function<void(AppEvent code, void* data1, void* data2)> ApplicationEventCallbackFunction;

class Kernel
{
	SDLBase* base_;
	SDLEventManager* eventManager_;
	KernelStateCallbackFunction callbackState_;
	bool firstrun_;
	std::map<Uint32, GUIScreen*> screens_;
	ApplicationEventCallbackFunction applicationEventCallbackFunction_;
	el::Logger* logger_;
	AudioManager* audioManager_;
	ConfigManager* configManager_;
	MediaManager* mediaManager_;
	DatabaseManager* databaseManager_;
    MapManager* mapManager_;
    BackendConnector* backend_;
    float screenDpi_;
    void HandleEvent(const SDL_Event& event,bool& exitLoop);
    void StartMediaManager();
    void StartDatabase();
    void LoadPlugins();
public:
    Kernel();
    virtual ~Kernel();

    bool StartUp(int argc, char* argv[]);
    void SetStateCallBack(KernelStateCallbackFunction callback);
    void Run();
    void Shutdown();
    void SendShutdownBackend(bool power);
    void SendInitDoneBackend();

    GUIElementManager* CreateScreen(const std::string& title);
    void RegisterApplicationEvent(ApplicationEventCallbackFunction callbackFunction);
    int StartAudio();
    int PlayMusik(const std::string& filename) const;
    int PlaySound(const std::string& filename) const;
    void StopMusik() const;
    void VolumeUp();
    void VolumeDown();
    void StartServices();
    MediaManager* GetMediaManger() const;
    AudioManager* GetAudioManager() const;
    MapManager* GetMapManager() const;
    ConfigManager* GetConfigManager() const;
    SDLEventManager* GetEventManager() const;
    std::string GetConfigText() const;
    static el::base::type::StoragePointer SharedLoggingRepository();

};