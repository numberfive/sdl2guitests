#pragma once

#include "../../../common/utils/easylogging++.h"
#include "../../../common/gsl-lite.hpp"
#include "../../../common/MediaFileFormat.h"

enum class DatabaseState : unsigned char;
const std::string BLANKARTIST_FAKEMUSICBRAINZID = "[NotFound]";
const std::string BLANKARTIST_NAME = "[Empty]";
const long BLANKARTIST_ID = 0;

const std::string BLANKGENRE_NAME = "[Empty]";
const long BLANKGENRE_ID = 0;

class DatabaseManager;

typedef struct MediaTitleInfo_{
    std::string Name;
    std::string Album;
    std::string Artist;
    std::string MediaFile;
    int Id;
    char* smallImage;
    int smallImageSize;
    MediaTitleInfo_(): Id(-1) {
            smallImage = nullptr;
            smallImageSize = 0;
    }

    ~MediaTitleInfo_() {
            if (smallImage != nullptr) {
                    delete[] smallImage;
                    smallImage = nullptr;
            }
            smallImageSize = 0;
    }
} MediaTileInfo;

typedef struct ShortSongInfo_ {
	std::string Name;
	int id;
} ShortSongInfo;

typedef struct MediaAlbenInfo_ {
	std::string Name;
	std::string Artist;
	std::string InfoText;
	int Id;
	std::vector<ShortSongInfo> songList;
	char* smallImage;
	int smallImageSize;
	MediaAlbenInfo_() : Id(-1) {
		smallImage = nullptr;
		smallImageSize = 0;
	}

	~MediaAlbenInfo_() {
		if (smallImage != nullptr) {
			delete[] smallImage;
			smallImage = nullptr;
		}
		smallImageSize = 0;
	}
} MediaAlbenInfo;



class MediaManager {
    el::Logger* logger_;
    DatabaseManager* databaseManager_;
    std::string mediathekBasePath_;
    std::string appDataPath_;
    
    void CreateTablesMusikInfo() const;
    void OnDatabaseStartup(DatabaseState state) const;
    void GetSmallImageForSong(int songId, char*& image, int& imageSize) const;
    void GetSmallImageForAlbum(int id, char*& image, int& imageSize);
    std::string GetAlbumForSong(int songId) const;
    std::string GetArtistForSong(int songId) const;
    std::vector<ShortSongInfo> GetSongsFromAlabum(int id) const;
public:
	explicit MediaManager(gsl::not_null<DatabaseManager*> databaseManager, const std::string& mediathekBasePath, const std::string& appDataPath);
	~MediaManager();

	void Init();
	static void Deinit();
		
	std::vector<MediaTileInfo*> GetAllMusikTitle(int maxCount) const;
	
	std::vector<MediaAlbenInfo*> GetAllMusikAlben(int maxCount);
	std::string GetSourceForTitel(int songId) const;
        
        std::vector<MediaTileInfo*> GetFileMusikTitle(int maxCount);
};