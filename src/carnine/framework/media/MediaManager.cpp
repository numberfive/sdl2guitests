/**
* @file  MediaManager.cpp
*
* Implementation for Manage Read Media Information
* Audio Infodata in SQLite
* 
* @date  2018-02-20
*/
#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "MediaManager"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif
#include "../../../common/utils/easylogging++.h"

#ifdef DEBUG
	#include "../../../common/nvwa/debug_new.h"
#endif

#include "MediaManager.h"
#include "../../../common/NullPointerException.h"
#include "../../../common/database/TableField.h"
#include "../../../common/database/Statement.h"
#include "../../../common/database/DatabaseManager.h"
#include "../../../common/commonutils.h"
#include "../../../common/utils/json.hpp"

using json = nlohmann::json;

void MediaManager::CreateTablesMusikInfo() const {
	std::vector<TableField> fields;
	TableField field;

	field.name_ = "ID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.autoinc_ = true;
	fields.push_back(field);

	field.name_ = "NAME";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = true;
	field.autoinc_ = false;
	fields.push_back(field);

	field.name_ = "YEAR";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.notNull_ = false;
	fields.push_back(field);

	field.name_ = "IMAGE";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.size_ = 512;
	field.notNull_ = false;
	fields.push_back(field);

	field.name_ = "MusicBrainzAlbumId";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.size_ = 256;
	field.notNull_ = false;
	fields.push_back(field);

	field.name_ = "Rating";
	field.SetType(DatabaseFieldTypes::DOUBLE);
	field.size_ = -1;
	field.notNull_ = true;
	field.adddeftext_ = " DEFAULT 0";
	fields.push_back(field);

	field.name_ = "lastPlayed";
	field.SetType(DatabaseFieldTypes::TIMESTAMP);
	field.size_ = -1;
	field.notNull_ = false;
	field.adddeftext_ = "";
	fields.push_back(field);

	field.name_ = "Added";
	field.SetType(DatabaseFieldTypes::TIMESTAMP);
	field.size_ = -1;
	field.notNull_ = true;
	field.adddeftext_ = " DEFAULT CURRENT_TIMESTAMP";
	fields.push_back(field);

	databaseManager_->CreateTable("ALBUM", fields);

	fields.clear();

	field.name_ = "ID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.autoinc_ = true;
	field.adddeftext_ = "";
	fields.push_back(field);

	field.name_ = "NAME";
	field.NoKey();
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.adddeftext_ = " UNIQUE";
	field.size_ = 256;
	field.notNull_ = true;
	field.autoinc_ = false;
	fields.push_back(field);

	field.name_ = "Image";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.NoKey();
	field.size_ = 512;
	field.notNull_ = false;
	field.autoinc_ = false;
	field.adddeftext_ = "";
	fields.push_back(field);

	field.name_ = "MusicBrainzArtistId";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = false;
	field.autoinc_ = false;
	field.adddeftext_ = "";
	fields.push_back(field);

	field.name_ = "Added";
	field.SetType(DatabaseFieldTypes::TIMESTAMP);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = false;
	field.autoinc_ = false;
	field.adddeftext_ = " DEFAULT CURRENT_TIMESTAMP";
	fields.push_back(field);

	databaseManager_->CreateTable("ARTIST", fields);

	fields.clear();

	field.name_ = "ID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.autoinc_ = true;
	field.adddeftext_ = "";
	fields.push_back(field);

	field.name_ = "NAME";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = " UNIQUE";
	fields.push_back(field);

	databaseManager_->CreateTable("GENRE", fields);
		
	//Insert Default Value
	auto strSQL = databaseManager_->PrepareSQL("INSERT INTO GENRE (ID, NAME) VALUES( %i, '%s')", BLANKGENRE_ID, BLANKGENRE_NAME.c_str());
	databaseManager_->DoDml(strSQL);
	
	fields.clear();

	field.name_ = "ID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.autoinc_ = true;
	field.adddeftext_ = "";
	fields.push_back(field);

	field.name_ = "NAME";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = true;
	field.autoinc_ = false;
	fields.push_back(field);

	field.name_ = "DURATION";
	field.SetType(DatabaseFieldTypes::DOUBLE);
	field.NoKey();
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	fields.push_back(field);

	field.name_ = "TRACK";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.NoKey();
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	fields.push_back(field);

	field.name_ = "IMAGE";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.NoKey();
	field.size_ = 512;
	field.notNull_ = false;
	field.autoinc_ = false;
	fields.push_back(field);

	field.name_ = "YEAR";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.NoKey();
	field.size_ = -1;
	field.notNull_ = false;
	field.autoinc_ = false;
	fields.push_back(field);

	field.name_ = "LASTPLAYED";
	field.NoKey();
	field.SetType(DatabaseFieldTypes::TIMESTAMP);
	field.size_ = -1;
	field.notNull_ = false;
	field.autoinc_ = false;
	fields.push_back(field);

	field.name_ = "MUSICBRAINZTRACKID";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = false;
	field.autoinc_ = false;
	fields.push_back(field);

	field.name_ = "ADDED";
	field.SetType(DatabaseFieldTypes::TIMESTAMP);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = false;
	field.autoinc_ = false;
	field.adddeftext_ = " DEFAULT CURRENT_TIMESTAMP";
	fields.push_back(field);

	field.name_ = "RATING";
	field.SetType(DatabaseFieldTypes::DOUBLE);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = " DEFAULT 0";
	fields.push_back(field);

	databaseManager_->CreateTable("SONG", fields);

	fields.clear();

	field.name_ = "ID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.autoinc_ = true;
	field.adddeftext_ = "";
	fields.push_back(field);

	field.name_ = "SOURCETYPE";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = "";
	fields.push_back(field);

	field.name_ = "SOURCE";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.NoKey();
	field.size_ = 512;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = "";
	fields.push_back(field);

	field.name_ = "HASH";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = "";
	fields.push_back(field);

	databaseManager_->CreateTable("MEDIASOURCE", fields);
	
	strSQL = databaseManager_->PrepareSQL("CREATE UNIQUE INDEX MEDIASOURCE_SOURCE ON MEDIASOURCE(SOURCETYPE, SOURCE); ");
	databaseManager_->DoDml(strSQL);

	fields.clear();

	field.name_ = "ALBUMID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = " REFERENCES ALBUM(ID)";
	field.AddToKey();
	fields.push_back(field);

	field.name_ = "SONGID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = " REFERENCES SONG(ID)";
	field.AddToKey();
	fields.push_back(field);

	databaseManager_->CreateTable("ALBUM_SONG", fields);

	fields.clear();

	field.name_ = "ARTISTID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = " REFERENCES ARTIST(ID)";
	field.AddToKey();
	fields.push_back(field);

	field.name_ = "GENRESID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = " REFERENCES GENRE(ID)";
	field.AddToKey();
	fields.push_back(field);

	databaseManager_->CreateTable("ARTIST_GENRE", fields);
	
	fields.clear();
	field.name_ = "SONGID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = " REFERENCES SONG(ID)";
	field.AddToKey();
	fields.push_back(field);

	field.name_ = "ARTISTID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = " REFERENCES ARTIST(ID)";
	field.AddToKey();
	fields.push_back(field);

	field.name_ = "ORDERNR";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = "";
	fields.push_back(field);

	databaseManager_->CreateTable("SONG_ARTIST", fields);

	fields.clear();
	field.name_ = "SONGID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = " REFERENCES SONG(ID)";
	field.AddToKey();
	fields.push_back(field);

	field.name_ = "MEDIASOURCEID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = " REFERENCES MEDIASOURCE(ID)";
	field.AddToKey();
	fields.push_back(field);

	field.name_ = "ORDERNR";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = false;
	field.adddeftext_ = "";
	fields.push_back(field);

	databaseManager_->CreateTable("SONG_MEDIASOURCE", fields);

	fields.clear();
	field.name_ = "ID";
	field.SetType(DatabaseFieldTypes::INTEGER);
	field.NoKey();
	field.size_ = -1;
	field.notNull_ = true;
	field.autoinc_ = true;
	fields.push_back(field);

	field.name_ = "NAME";
	field.SetType(DatabaseFieldTypes::VARCHAR);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = false;
	field.autoinc_ = false;
	fields.push_back(field);

	field.name_ = "DATA";
	field.SetType(DatabaseFieldTypes::BLOB);
	field.NoKey();
	field.size_ = 256;
	field.notNull_ = false;
	field.autoinc_ = false;
	fields.push_back(field);

	databaseManager_->CreateTable("IMAGE32X32", fields);

	databaseManager_->DoDml("CREATE INDEX IMAGE_NAME ON Image32x32(NAME)");
	databaseManager_->DoDml("CREATE INDEX ARTISTSONG on Song_Artist(songId)");
	databaseManager_->DoDml("CREATE INDEX ALBUMSONG_SONG on ALBUM_SONG (songid);");
	
}

void MediaManager::OnDatabaseStartup(DatabaseState state) const {
	if(state == DatabaseState::StartupNew) {
		CreateTablesMusikInfo();
	}
}

MediaManager::MediaManager(gsl::not_null<DatabaseManager*> databaseManager, const std::string& mediathekBasePath, const std::string& appDataPath){
    logger_ = el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    databaseManager_ = databaseManager;
    mediathekBasePath_ = mediathekBasePath;
    appDataPath_ = appDataPath;
    if (databaseManager_ == nullptr) {
        throw NullPointerException("configManager can not be null");
    }
}

MediaManager::~MediaManager() {
}

void MediaManager::Init() {
    auto CDatabaseStartupdelegate = std::bind(&MediaManager::OnDatabaseStartup, this, std::placeholders::_1);
    databaseManager_->AddStateCallBack(CDatabaseStartupdelegate);
    LOG(DEBUG) << "Init";
    const auto filename = appDataPath_ + "playlist.json";
    LOG(INFO) << "filename " << filename;

    if (!utils::FileExists(filename)) {
        MediaTitleFile sampleFile;
        sampleFile.List = "Sample File";
        
        MediaTitleFileEntry entry1;
        entry1.Album = "1. Album";
        entry1.Artist = "1. Artist";
        entry1.Name = "1. Name";
        entry1.MediaFile = "/home/punky/Musik/test.mp3";
        
        MediaTitleFileEntry entry2;
        entry2.Album = "1. Album";
        entry2.Artist = "1. Artist";
        entry2.Name = "2. Name";
        
        sampleFile.Files.push_back(entry1);
        sampleFile.Files.push_back(entry2);
        
        std::ofstream o(filename);
	const json jConfig = sampleFile;
	o << std::setw(4) << jConfig << std::endl;
	o.close();
    }
}

void MediaManager::Deinit() {
	LOG(DEBUG) << "Deinit";
}

std::string MediaManager::GetAlbumForSong(const int songId) const {
	std::string sqlText = "select name from Album,Album_Song where Album.id = Album_Song.AlbumId and SongId = ";
	sqlText += std::to_string(songId);

	std::string resultText = "";
	auto query = databaseManager_->CreateQuery(sqlText);
	if (query->Eof()) {
		delete query;
		return resultText;
	}
    
	if (query->Execute() > 0) {
        while (!query->Eof()) {
        if(resultText.size() > 0) resultText += ";";
			resultText += query->GetColumnText(0);
			query->NextRow();
		}
	}
    if(resultText.size() == 0) resultText += "Album nicht gefunden";
	delete query;

	return resultText;
}

std::string MediaManager::GetArtistForSong(const int songId) const {
	const std::string sqlText = "select name from Artist,Song_Artist where Artist.id = Song_Artist.ArtistId and SongId = ?";

	std::string resultText = "";
	auto query = databaseManager_->CreateQuery(sqlText);
	query->BindInt(1, songId);
	if (query->Eof()) {
		delete query;
		return resultText;
	}
	if (query->Execute() > 0) {
		resultText = "";
		while (!query->Eof()) {
            if(resultText.size() > 0) resultText += ";";
			resultText += query->GetColumnText(0);
			query->NextRow();
		}
	}
	delete query;
    if(resultText.size() == 0) resultText += "Künstler nicht gefunden";
    
	return resultText;
}


std::vector<ShortSongInfo> MediaManager::GetSongsFromAlabum(const int id) const {
	const auto sqlText = databaseManager_->PrepareSQL("select song.name, song.id songid from Album_Song, song where Album_Song.AlbumId = %d and Album_Song.songId = song.id order by track;", id);

	std::vector<ShortSongInfo> result;
	auto query = databaseManager_->CreateQuery(sqlText);
	if (query->Eof()) {
		delete query;
		return result;
	}
	if (query->Execute() > 0) {
		while (!query->Eof()) {
			ShortSongInfo info;
			info.Name = query->GetColumnText(0);
			info.id = query->GetColumnInt(1);
			result.push_back(info);
			query->NextRow();
		}
	}

	return result;
}

void MediaManager::GetSmallImageForSong(const int songId, char*& image, int& imageSize) const {
	image = nullptr;
	imageSize = 0;

	std::string sqlText = "select data from Image32x32,song where song.image = Image32x32.name and song.Id = ";
	sqlText += std::to_string(songId);

	auto query = databaseManager_->CreateQuery(sqlText);
	if (query->Eof()) {
		delete query;
		return;
	}

	if (query->Execute() > 0) {
		if (!query->Eof()) {
			const void* tempData;
			imageSize = query->GetColumnBlob(0, tempData);
			image = new char[imageSize];
			memcpy(image, tempData, imageSize);
			query->NextRow();
		}
	}
	delete query;
}

void MediaManager::GetSmallImageForAlbum(int id, char*& image, int& imageSize) {
	image = nullptr;
	imageSize = 0;

	std::string sqlText = "select data from Image32x32,album where album.image = Image32x32.name and album.Id = ";
	sqlText += std::to_string(id);

	auto query = databaseManager_->CreateQuery(sqlText);
	if (query->Eof()) {
		delete query;
		return;
	}

	if (query->Execute() > 0) {
		if (!query->Eof()) {
			const void* tempData;
			imageSize = query->GetColumnBlob(0, tempData);
			image = new char[imageSize];
			memcpy(image, tempData, imageSize);
			query->NextRow();
		}
	}
	delete query;
}

std::vector<MediaTileInfo*> MediaManager::GetAllMusikTitle(const int maxCount) const {
#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
	TIMED_SCOPE_IF(timerBlkObjNeedUpdate, "GetAllMusikTitle", VLOG_IS_ON(2));
#endif
	std::vector<MediaTileInfo*> result;
	std::string sqlText = "select name, id from Song order by name";
	if(maxCount > 0 ) {
		sqlText += " limit " + std::to_string(maxCount);
	}
	auto query = databaseManager_->CreateQuery(sqlText);
	if(query->Eof()) {
		return result;
	}
	if(query->Execute() > 0) {
		while(!query->Eof()) {
			auto info = new MediaTileInfo();
			info->Name = query->GetColumnText(0);
			info->Id = query->GetColumnInt(1);
			info->Album = GetAlbumForSong(info->Id);
			info->Artist = GetArtistForSong(info->Id);
            info->MediaFile = "";
			GetSmallImageForSong(info->Id, info->smallImage, info->smallImageSize);
			result.push_back(info);
			query->NextRow();
		}
	}
	delete query;

	return result;
}

std::vector<MediaAlbenInfo*> MediaManager::GetAllMusikAlben(const int maxCount) {
#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
	TIMED_SCOPE_IF(timerBlkObjNeedUpdate, "GetAllMusikAlben", VLOG_IS_ON(2));
#endif

	std::vector<MediaAlbenInfo*> result;
	std::string sqlText = "select name, id from Album order by name";
	if (maxCount > 0) {
		sqlText += " limit " + std::to_string(maxCount);
	}
	auto query = databaseManager_->CreateQuery(sqlText);
	if (query->Eof()) {
		return result;
	}
	if (query->Execute() > 0) {
		while (!query->Eof()) {
			auto info = new MediaAlbenInfo();
			info->Name = query->GetColumnText(0);
			info->Id = query->GetColumnInt(1);
			info->songList = GetSongsFromAlabum(info->Id);
			for (const auto &entry : info->songList) {
				const auto artist = GetArtistForSong(entry.id);
				if (info->Artist.find(artist) == std::string::npos) {
                    if(info->Artist.size() > 0) info->Artist += ";";
					info->Artist += artist;
				}
			}
			GetSmallImageForAlbum(info->Id, info->smallImage, info->smallImageSize);
			result.push_back(info);
			query->NextRow();
		}
	}
	delete query;

	return result;
}

std::string MediaManager::GetSourceForTitel(const int songId) const {
	//Todo Handle more than one Source
	std::string sqlText = "select source from song_mediasource, mediasource where song_mediasource.mediasourceid = mediasource.id and song_mediasource.songid = ";
	sqlText += std::to_string(songId);
    //Todo handle source from other source or network stream

	std::string resultText = "";
	if (mediathekBasePath_.size() > 0) {
		resultText += mediathekBasePath_ + "/";
	}

	auto query = databaseManager_->CreateQuery(sqlText);
	if (query->Eof()) {
		delete query;
		return resultText;
	}
	if (query->Execute() > 0) {
		while (!query->Eof()) {
			resultText += query->GetColumnText(0);
			query->NextRow();
		}
	}
	delete query;

	return resultText;
}

std::vector<MediaTileInfo*> MediaManager::GetFileMusikTitle(int maxCount) {
    std::vector<MediaTileInfo*> result;
    const auto filename = appDataPath_ + "playlist.json";
    
    if (utils::FileExists(filename)) {
        try {
            std::ifstream ifs(filename);
            const auto jConfig = json::parse(ifs);
            MediaTitleFile sampleFile = jConfig;
            ifs.close();
            
            for(const auto &resultEntry : sampleFile.Files) {
                auto info = new MediaTileInfo();
                info->Album = resultEntry.Album;
                info->Artist = resultEntry.Artist;
                info->Id = -1;
                info->MediaFile = resultEntry.MediaFile;
                info->Name = resultEntry.Name;
                //Todo Read Image wenn Name not Empty
                result.push_back(info);
                
                if(maxCount > 0 && (int)result.size() >= maxCount) {
                    break;
                }
            }
        }
        catch (json::parse_error& exp) {
            LOG(ERROR) << exp.what();
            auto info = new MediaTileInfo();
            info->Name = exp.what();
            info->Id = -1;
            info->Album = "Ablum";
            info->Artist = "Artist";
            info->smallImage = nullptr;
            info->smallImageSize = 0;
            result.push_back(info);
        }
        catch (std::domain_error& exp) {
            LOG(ERROR) << exp.what();
            auto info = new MediaTileInfo();
            info->Name = exp.what();
            info->Id = -1;
            info->Album = "Ablum";
            info->Artist = "Artist";
            info->smallImage = nullptr;
            info->smallImageSize = 0;
            result.push_back(info);
        }
        catch (std::exception& exp) {
            LOG(ERROR) << exp.what();
            auto info = new MediaTileInfo();
            info->Name = exp.what();
            info->Id = -1;
            info->Album = "Ablum";
            info->Artist = "Artist";
            info->smallImage = nullptr;
            info->smallImageSize = 0;
            result.push_back(info);
        }
    }
    else {
        auto info = new MediaTileInfo();
        info->Name = "Datei wurde nicht gefunden";
        info->Id = -1;
        info->Album = "Ablum";
        info->Artist = "Artist";
        info->smallImage = nullptr;
        info->smallImageSize = 0;
        result.push_back(info);
    }
    
    return result;
}