#pragma once

#include <thread>
#include <SDL2/SDL_net.h>
#include "IBackendConnect.h"
#include "../../common/utils/json.hpp"

using json = nlohmann::json;

class SDLEventManager;
class ClientSocketConnection;

class BackendConnector : public IBackendConnect
{
    SDLEventManager* eventManager_;
    ClientSocketConnection* connection_;
    SDLNet_SocketSet socketSet_;
    bool run_;
    bool disconnectSend_;
    std::thread loop_thread_;
    long _retryCount;
    Uint32 _waitTime;
    std::vector<MessageFromBackendDelegate> _messageCallbacks;
    void Loop();
    void IncomingMessage(const std::string& MessageName, json const& Message);
public:
    BackendConnector(SDLEventManager* eventManager);
    ~BackendConnector();

    void Init();
    void Shutdown();
    void SendShutdown(bool power);
    void SendInitDone();
    void SendToBackend(json const& message);
    void RegisterMeForBackendMessage(MessageFromBackendDelegate callback);
};

