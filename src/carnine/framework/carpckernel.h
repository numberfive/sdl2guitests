#pragma once

//Todo build config.h
//include "config.h"

#include <string>
#include <iostream>
#include <functional>
#include <SDL.h>

#include "../../common/NullPointerException.h"
#include "SDLException.h"
#include "TTFException.h"
#include "SDLBase.h"

enum class KernelState : unsigned char {
	Startup,
	Shutdown,
};

enum class KernelEvent : Sint32 {
    Shutdown = 1
};

#include "SDLEventManager.h"


#if (!SDL_VERSION_ATLEAST(2,0,4))
SDL_FORCE_INLINE SDL_bool SDL_PointInRect(const SDL_Point *p, const SDL_Rect *r)
{
	return ((p->x >= r->x) && (p->x < (r->x + r->w)) &&
		(p->y >= r->y) && (p->y < (r->y + r->h))) ? SDL_TRUE : SDL_FALSE;
}
#endif

#include "gui/GUI.h"
