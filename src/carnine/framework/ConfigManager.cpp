/**
* @file  ConfigManager.cpp
*
* Implementation for Manage Config Read and write
*
* @date  2018-02-20
*/

#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "ConfigManager"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include "ConfigManager.h"
#include "../../common/utils/easylogging++.h"
#include "../../common/utils/json.hpp"
#include "../../common/commonutils.h"

#ifdef NVWAMEMCHECK
	#include "../../common/nvwa/debug_new.h"
#endif

using json = nlohmann::json;

void to_json(json& j, const IoButton& p) {
	j = json{
		{ "DisplayName", p.DisplayName },
        { "PosX", p.PosX },
        { "PosY", p.PosY },
        { "Width", p.Width },
        { "Height", p.Height },
        { "Device", p.Device },
        { "Id", p.Id },
        { "Type", p.Type }
	};
}

void to_json(json& j, const ConfigFile& p) {
	j = json{
		{ "MapDataPath", p.mapDataPath },
        { "MapStyle", p.mapStyle },
        { "MapIconPaths", p.mapIconPaths },
        { "MediathekBasePath", p.mediathekBasePath },
        { "LastMapLat", p.LastMapLat },
        { "LastMapLon", p.LastMapLon },
        { "Volume", p.Volume },
        { "VideoDriver", p.VideoDriver},
        { "AudioDriver", p.AudioDriver},
        { "IoButtons", p.IoButtons}
	};
}

void from_json(const json& j, IoButton& p) {
    p.DisplayName = j.at("DisplayName").get<std::string>();
	p.PosX = j.at("PosX").get<int>();
	p.PosY = j.at("PosY").get<int>();
    p.Width = j.at("Width").get<int>();
	p.Height = j.at("Height").get<int>();
    p.Device = j.at("Device").get<std::string>();
    p.Id = j.at("Id").get<int>();
    p.Type = j.at("Type").get<Button_Type>();
}

void from_json(const json& j, ConfigFile& p) {
	p.mapDataPath = j.at("MapDataPath").get<std::string>();
	p.mapStyle = j.at("MapStyle").get<std::string>();
	p.mapIconPaths = j.at("MapIconPaths").get<std::vector<std::string>>();
	const auto it_value = j.find("MediathekBasePath");
	if (it_value != j.end()) {
		p.mediathekBasePath = j.at("MediathekBasePath").get<std::string>();
	}
	else {
        p.mediathekBasePath = "D:\\Mine\\";
    }
    
    const auto it_valueMapLat = j.find("LastMapLat");
    if (it_valueMapLat != j.end()) {
        p.LastMapLat = j.at("LastMapLat").get<double>();
    } else {
        p.LastMapLat = 50.094;
    }
    
    const auto it_valueMapLon = j.find("LastMapLon");
    if (it_valueMapLon != j.end()) {
        p.LastMapLon = j.at("LastMapLon").get<double>();
    } else {
        p.LastMapLon = 8.49617;
    }

    const auto it_valueVolume = j.find("Volume");
    if (it_valueVolume != j.end()) {
        p.Volume = j.at("Volume").get<int>();
    } else {
        p.Volume = 127;
    }

    const auto it_valueVideoDriver = j.find("VideoDriver");
    if (it_valueVideoDriver != j.end()) {
        p.VideoDriver = j.at("VideoDriver").get<std::string>();
    } else {
        p.VideoDriver = "";
    }

    const auto it_valueAudioDriver = j.find("AudioDriver");
    if (it_valueAudioDriver != j.end()) {
        p.AudioDriver = j.at("AudioDriver").get<std::string>();
    } else {
        p.AudioDriver = "";
    }

    const auto it_IoButtons = j.find("IoButtons");
    if (it_IoButtons != j.end()) {
        p.IoButtons = j.at("IoButtons").get<std::vector<IoButton>>();
    } else {
        IoButton firstButton;
        firstButton.DisplayName = "Test Knopf";
        firstButton.PosX = 0;
        firstButton.PosY = 0;
        firstButton.Width = 50;
        firstButton.Height = 30;
        firstButton.Device = "MCP1";
        firstButton.Id = 0;
        firstButton.Type = Button_Type::Switch;
        p.IoButtons.push_back(firstButton);
    }
}

ConfigManager::ConfigManager():
    configFile_() {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    appBasePath_ = "";
    appDataPath_ = "";
}

ConfigManager::~ConfigManager() {
}

void ConfigManager::Init() {
    auto error = false;
    auto basePath = SDL_GetBasePath();
    if (basePath)
    {
        appBasePath_ = basePath;
        SDL_free(basePath);
    }

    basePath = SDL_GetPrefPath("MiNeSoftware", "CarNiNe");
    if (basePath) {
        appDataPath_ = basePath;
        SDL_free(basePath);
    }

    LOG(INFO) << "AppBasePath " << appBasePath_;
    LOG(INFO) << "AppDataPath " << appDataPath_;

    filenameConfig_ = appDataPath_ + "config.json";
    LOG(INFO) << "filenameConfig_ " << filenameConfig_;

    if (utils::FileExists(filenameConfig_)) {
        try {
            std::ifstream ifs(filenameConfig_);
            const auto jConfig = json::parse(ifs);
            configFile_ = jConfig;
            ifs.close();
        } catch(json::parse_error& exp) {
            LOG(ERROR) << exp.what();
            error = true;
        }
        catch (std::domain_error& exp) {
            LOG(ERROR) << exp.what();
            error = true;
        }
        catch (std::exception& exp) {
            LOG(ERROR) << exp.what();
            error = true;
        }
    }
    else {
        error = true;
        LOG(DEBUG) << "no config found generate default";
    }
    if(error){
        configFile_.mapDataPath = "D:\\Mine\\OpenSource\\libosmscout-code\\maps\\hessen-latest";
        configFile_.mapStyle = "D:\\Mine\\OpenSource\\libosmscout-code\\stylesheets\\standard.oss";
        configFile_.mapIconPaths.push_back("D:\\Mine\\OpenSource\\libosmscout-code\\libosmscout\\data\\icons\\14x14\\standard\\");
        configFile_.mediathekBasePath = "D:\\Mine\\";
        configFile_.LastMapLat = 50.094;
        configFile_.LastMapLon = 8.49617;
        configFile_.Volume = 127;
        configFile_.VideoDriver = "";
        configFile_.AudioDriver = "";
        IoButton firstButton;
        firstButton.DisplayName = "Test Knopf";
        firstButton.PosX = 0;
        firstButton.PosY = 0;
        firstButton.Width = 50;
        firstButton.Height = 30;
        firstButton.Device = "MCP1";
        firstButton.Id = 0;
        firstButton.Type = Button_Type::Switch;
        configFile_.IoButtons.push_back(firstButton);
    }
}

void ConfigManager::Shutdown() {
	std::ofstream o(filenameConfig_);
	const json jConfig = configFile_;
	o << std::setw(4) << jConfig << std::endl;
	o.close();
}

std::string ConfigManager::GetDataPath() const {
    return appDataPath_;
}

std::string ConfigManager::GetAppBasePath() const {
    return appBasePath_;
}

std::string ConfigManager::GetMapDataPath() const {
	return configFile_.mapDataPath;
}

std::string ConfigManager::GetMapStyle() const {
	return configFile_.mapStyle;
}

std::vector<std::string> ConfigManager::GetMapIconPaths() const {
	return configFile_.mapIconPaths;
}

std::string ConfigManager::GetMediathekBasePath() const {
    return configFile_.mediathekBasePath;
}

osmscout::GeoCoord ConfigManager::GetGetLastPosition() const {
    return osmscout::GeoCoord(configFile_.LastMapLat, configFile_.LastMapLon);
}

void ConfigManager::UpdateLastPosition(osmscout::GeoCoord position) {
    configFile_.LastMapLat = position.GetLat();
    configFile_.LastMapLon = position.GetLon();
}

int ConfigManager::GetVolume() const {
    return configFile_.Volume;
}

void ConfigManager::UpateVolume(int volume) {
    configFile_.Volume = volume;
}

std::string ConfigManager::GetVideoDriver() const {
    return configFile_.VideoDriver;
}

std::string ConfigManager::GetConfigText() const {
    std::string result = "DataPath:          " + appDataPath_ + "\n";
    result += "MediathekBasePath: " + configFile_.mediathekBasePath + "\n";
    result += "MapDataPath:       " + configFile_.mapDataPath + "\n";
    result += "MapStyle:          " + configFile_.mapStyle + "\n";
    result += "VideoDriver:       " + configFile_.VideoDriver + "\n";
    result += "AudioDriver:       " + configFile_.AudioDriver + "\n";
    result += "Volume:            " + std::to_string(configFile_.Volume) + "\n";

    return result;
}

std::vector<IoButton> ConfigManager::GetButtonConfig() const {
    return configFile_.IoButtons;
}

std::string ConfigManager::GetAudioDriver() const {
    return configFile_.AudioDriver;
}