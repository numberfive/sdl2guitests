#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "BackendConnector"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include "../../common/utils/easylogging++.h"
#include "../../common/BackendMessages.h"
#include "BackendConnector.h"
#include "SDLEventManager.h"
#include "../../common/ClientSocketConnection.h"
#include "AppEvents.h"

BackendConnector::BackendConnector(SDLEventManager* eventManager) {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    eventManager_ = eventManager;
    connection_ = new ClientSocketConnection("127.0.0.1", SERVERPORT);
    socketSet_ = nullptr;
    run_ = true;
    disconnectSend_ = true;
    _waitTime = 500;
    _retryCount = 0;

    auto sendToBackendDelegate = std::bind(&BackendConnector::SendToBackend, this, std::placeholders::_1);
    eventManager_->RegisterBackend(sendToBackendDelegate, this);
}

BackendConnector::~BackendConnector() {
     if(connection_ != nullptr){
        delete connection_;
    }
}

void BackendConnector::Init() {
    loop_thread_ = std::thread(&BackendConnector::Loop, this);
}

void BackendConnector::Shutdown() {
    if(!run_) return;
    
    LOG(DEBUG) << "Shutdown";
    run_ = false;
    connection_->CloseSocket();
    loop_thread_.join();
    delete connection_;
    connection_ = nullptr;
    SDLNet_FreeSocketSet(socketSet_);
}

void BackendConnector::Loop() {
    while(run_) {
        if(!connection_->IsConnected()){
            if(!connection_->OpenConnectionToServer()) {
                SDL_Delay(_waitTime);
                if(!disconnectSend_) {
                    eventManager_->PushApplicationEvent(AppEvent::BackendDisconnected, nullptr, nullptr);
                    disconnectSend_ = true;
                }
                if(_retryCount == 20) {
                    _retryCount = 0;
                    _waitTime += 500;
                } else {
                    _retryCount++;
                }
                continue;
            } else {
                if(socketSet_ != nullptr) {
                    SDLNet_FreeSocketSet(socketSet_);
                }
                socketSet_ = SDLNet_AllocSocketSet(1);
                connection_->AddToSocketSet(&socketSet_);
                auto newDatadelegate = std::bind(&BackendConnector::IncomingMessage, this, std::placeholders::_1, std::placeholders::_2);
                connection_->SetNewDataCallback(newDatadelegate);
                disconnectSend_ = false;
                _retryCount = 0;
                _waitTime = 500;
            }
        }
        
        //Todo I don#t like not wait Infinit (-1) but wenn wait infity i finde no way to wake up SDLNet_CheckSockets
        int numready = SDLNet_CheckSockets(socketSet_, 1000); //(Uint32)-1
        if(numready > 0 && run_) {
            auto needRead = false;
            if(connection_->CheckSocket(&needRead)) {
                if(needRead){
                    connection_->Read();
                }
            }
        }
    }
       
    LOG(INFO) << "TCP thread is stopped";
}

void BackendConnector::IncomingMessage(const std::string& MessageName, json const& Message) {
    LOG(DEBUG) << "Incoming Message " << MessageName;
    if(MessageName == "WelcomeMessage") {
        eventManager_->PushApplicationEvent(AppEvent::BackendConnected, nullptr, nullptr);
    } else if (MessageName == "GPSMessage") {
        GPSMessage messageJson = Message;
        auto intMessage = new KernelGPSMessage(messageJson);
        eventManager_->PushApplicationEvent(AppEvent::NewGeopos, intMessage, nullptr);
        
    } else if(MessageName == "MediaFileUpdateMessage") {
        MediaFileUpdateMessage messageJson = Message;
        eventManager_->PushApplicationEvent(AppEvent::MediaFileUpdate, nullptr, nullptr);
        
    } else if(MessageName == "RemoteControlMessage") {
        RemoteControlMessage messageJson = Message;
        auto internalMessage = new KernelRemoteControlMessage(messageJson);
        eventManager_->PushApplicationEvent(AppEvent::RemoteControl, internalMessage, nullptr);
    } else if(MessageName == "PowerSupplyOff") {
        eventManager_->PushApplicationEvent(AppEvent::PowerSupplyOff, nullptr, nullptr);
    } else if(MessageName == "CpuTempUpdate") {
        //Todo implement
    } else if(MessageName == "VolumeUpdateMessage") {
        VolumeUpdateMessage messageJson = Message;
        if(messageJson.value == "1") {
            eventManager_->PushApplicationEvent(AppEvent::VolumeUp, nullptr, nullptr);
        } else {
            eventManager_->PushApplicationEvent(AppEvent::VolumeDown, nullptr, nullptr);
        }
    }
    else {
        auto handled = false;
        for(auto callback :_messageCallbacks){
            if(callback(Message)){
                handled = true;
            }
        }
        
        if(!handled) LOG(WARNING) << MessageName << " Message Not Handled";
    }
}

void BackendConnector::SendShutdown(bool power) {
    if(connection_->IsConnected()) {
        ShutdownMessage message;
        message.Powerdown = power;
        connection_->Send(message);
    } else {
        LOG(WARNING) << "SendShutdown not send (not connected)";
    }
}

void BackendConnector::SendInitDone() {
    if(connection_->IsConnected()) {
        InitDoneMessage message;
        connection_->Send(message);
    } else {
        LOG(WARNING) << "SendInitDone not send (not connected)";
    }
}

void BackendConnector::SendToBackend(json const& message) {
     if(connection_->IsConnected()) {
        connection_->Send(message);
    } else {
        LOG(WARNING) << "not connected";
    }
}

void BackendConnector::RegisterMeForBackendMessage(MessageFromBackendDelegate callback) {
    _messageCallbacks.push_back(callback);
}