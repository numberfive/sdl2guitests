#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "GUIScreenCanvas"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif
#include "../../../common/utils/easylogging++.h"

#ifdef NVWAMEMCHECK
#	include "../../../common/nvwa/debug_new.h"
#endif

#include "GUI.h"
#include "GUIElement.h"
#include "GUIRenderer.h"
#include "GUIScreenCanvas.h"
#include "GUITexture.h"
#include "GUIImageManager.h"

GUIScreenCanvas::GUIScreenCanvas(GUISize size):
	GUIElement(GUIPoint(0, 0), size, "ScreenCanvas"), 
	imageTexture_(nullptr) {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
	//Todo: Make Change able -> Config
	backgroundColor_ = white_color;
	bachgroundImage_ = "Background.png";
}

void GUIScreenCanvas::Resize(GUISize size)
{
	size_ = size;
	renderer_->ResizeTexture(Texture(), size_);
	needRedraw_ = true;
}

void GUIScreenCanvas::Init() {
    LOG(DEBUG) << "GUIScreenCanvas Init";
	if(bachgroundImage_ != "") {
		imageTexture_ = imageManager_->GetImage(bachgroundImage_);
	}
}

void GUIScreenCanvas::Draw()
{
	if (imageTexture_ != nullptr) {
		auto centerX = Size().width / 2;
		auto centerY = Size().height / 2;
		auto size = imageTexture_->Size();
		auto x = centerX - size.width / 2;
		auto y = centerY - size.height / 2;
		renderer_->RenderCopy(imageTexture_, GUIPoint(x, y));
	}
	needRedraw_ = false;
}

void GUIScreenCanvas::HandleEvent(GUIEvent& event)
{
    UNUSED(event);
}

void GUIScreenCanvas::UpdateAnimation()
{
}

void GUIScreenCanvas::Close()
{
}
