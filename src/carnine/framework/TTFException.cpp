#include "stdafx.h"
#include "TTFException.h"

std::string TTFException::CreateText(std::string function, std::string errormsg)
{
	auto tmp(function);
	tmp += " failed with: ";
	tmp += errormsg;
	return tmp;
}	

TTFException::TTFException(const char* function)
{
	sdl_error_ = TTF_GetError();
	sdl_function_ = function;
	message_ = CreateText(sdl_function_, sdl_error_);
}

TTFException::TTFException(const std::string& function)
{
	sdl_error_ = TTF_GetError();
	sdl_function_ = function;
	message_ = CreateText(sdl_function_, sdl_error_);
}

TTFException::~TTFException() throw()
{

}
