#pragma once
#include <osmscout/GeoCoord.h>
#include "../../common/BackendMessages.h"

struct IoButton {
    std::string DisplayName;
    int PosX;
    int PosY;
    int Width;
    int Height;
    std::string Device;
    int Id;
    Button_Type Type;
};

struct ConfigFile {
    std::string mapDataPath;
    std::string mapStyle;
    std::vector<std::string> mapIconPaths;
    std::string mediathekBasePath;
    double LastMapLat;
    double LastMapLon;
    int Volume;
    std::string VideoDriver;
    std::string AudioDriver;
    std::vector<IoButton> IoButtons;
};

class ConfigManager {
    std::string appBasePath_;
    std::string appDataPath_;
    std::string filenameConfig_;
    ConfigFile configFile_;
public:
    ConfigManager();
    ~ConfigManager();
    void Init();
    void Shutdown();

    std::string GetDataPath() const;
    std::string GetAppBasePath() const;
    std::string GetMapDataPath() const;
    std::string GetMapStyle() const;
    std::vector<std::string> GetMapIconPaths() const;
    std::string GetMediathekBasePath() const;
    osmscout::GeoCoord GetGetLastPosition() const;
    void UpdateLastPosition(osmscout::GeoCoord position);
    int GetVolume() const;
    void UpateVolume(int volume);
    std::string GetVideoDriver() const;
    std::string GetConfigText() const;
    std::vector<IoButton> GetButtonConfig() const;
    std::string GetAudioDriver() const;
};