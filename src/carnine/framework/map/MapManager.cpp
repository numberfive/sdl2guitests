/**
* @file  MapManager.cpp
*
* Implementation for Manager of Map Data
*
* @date  2018-04-06
*/

#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "MapManager"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include "../../../common/utils/easylogging++.h"

#ifdef NVWAMEMCHECK
#   include "../../../common/nvwa/debug_new.h"
#endif

#include <osmscout/TypeFeatures.h>
#include <osmscout/TextSearchIndex.h>
#include "MapManager.h"


int MapManager::WorkerMain() {
    LOG(DEBUG) << "Map Worker Started";
    while (true) {
        try {
            auto jobInfo = jobQueue_.remove();
	    if (jobInfo->whattodo == "Exit") {
                delete jobInfo;
                break;
	    } else if (jobInfo->whattodo == "DrawMap") {
                delete jobInfo;
                osmscout::StopClock drawMapClock;
                DrawMap();
                drawMapClock.Stop();
                osmscout::StopClock searchMapDataClock;
                SearchCurrentWayInfo();
                searchMapDataClock.Stop();
                LOG(INFO) << "Draw Map in " << drawMapClock.GetMilliseconds() << " ms";
                LOG(INFO) << "Search Map Data in " << searchMapDataClock.GetMilliseconds() << " ms";
	    }
        }catch(std::exception& exp) {
            LOG(ERROR) << exp.what();
        }
    }
    
    database_->DumpStatistics();
    LOG(DEBUG) << "Map Worker Stopped";
    return 0;
}

void MapManager::DrawMap() {
    LoadMapData();
    
    //memset(mapPixels_, 0xFF,  mapWidth_ * mapHeight_ * 4);
    
    if(image_data_source_ == nullptr){
        image_data_source_ = cairo_image_surface_create_for_data(mapPixels_, CAIRO_FORMAT_ARGB32, mapWidth_, mapHeight_, cairo_format_stride_for_width (CAIRO_FORMAT_ARGB32, mapWidth_));
        cairoImage_ = cairo_create(image_data_source_);
        if (cairoImage_ == nullptr) {
            LOG(ERROR) << "cairoImage == nullptr";
            return;
        }
    }
    
    if (painter_->DrawMap(projectionDraw_, drawParameter_, data_, cairoImage_)) {
        LOG(DEBUG) << "Map drawed on surface";
        if(paintMarker_) {
            double x, y;
            projectionDraw_.GeoToPixel(projectionDraw_.GetCenter(), x, y);
            if(image_data_marker_ == nullptr) {
                image_data_marker_ = cairo_image_surface_create_from_png("auto-day-icon.png");
                auto status = cairo_surface_status(image_data_marker_);
                if(status != CAIRO_STATUS_SUCCESS) {
                    LOG(ERROR) << "cairo load png " << cairo_status_to_string(status);
                    paintMarker_ = false;
                }
            }
            if(paintMarker_){
                auto imageWidth = cairo_image_surface_get_width(image_data_marker_);
                auto imageHeight = cairo_image_surface_get_height(image_data_marker_);
                cairo_set_source_surface(cairoImage_, image_data_marker_, x - (imageWidth / 2), y - (imageHeight / 2));
                cairo_paint(cairoImage_);
                LOG(DEBUG) << "drawed marker on surface";
            }
        }
        cairo_surface_flush(image_data_source_);
    }
    
    LOG(DEBUG) << "Map Draw Done";
    if(callbackNewMapImage_ != nullptr) {
        callbackNewMapImage_(mapPixels_, mapWidth_, mapHeight_);
    }
}

void MapManager::LoadMapData() {
    projectionDraw_.Set(mapCenter_,
                    mapAngle_,
                    magnification_,
                    screenDpi_,
                    mapWidth_,
                    mapHeight_);

    if (magnification_.GetLevel()>=15) {
        searchParameter_.SetMaximumAreaLevel(6);
    }
    else {
        searchParameter_.SetMaximumAreaLevel(4);
    }

    projectionDraw_.SetLinearInterpolationUsage(magnification_.GetLevel() >= 10);
    data_.ClearDBData();
    
    mapService_->LookupTiles(projectionDraw_, mapTiles_);
    auto count = 0;
    for (auto it = mapTiles_.begin(); it != mapTiles_.end(); ++it) {
        if (!it->get()->IsComplete()) {
            count++;
        }
    }
    LOG(INFO) << "titles " << mapTiles_.size() << " size need load " << count;
    
    if(mapService_->LoadMissingTileData(searchParameter_, *styleConfig_, mapTiles_)) {
        mapService_->AddTileDataToMapData(mapTiles_, data_);
    }
}

bool MapManager::FindWayInfoForCoord(const osmscout::GeoCoord& coord, std::string& name, int& maxSpeed) {
    name = "";
    
    auto routableResult = routingService_->GetClosestRoutableObject(coord, routingProfile_->GetVehicle(), osmscout::Distance::Of<osmscout::Meter>(100));
    LOG(DEBUG) << "Closest routable object: " << routableResult.GetObject().GetName() << " '" << routableResult.GetName() << "' " << routableResult.GetDistance().AsMeter() << "m";
    
    name = routableResult.GetName();
    maxSpeed = -1;
    auto way = routableResult.GetWay();
    
    if(way == nullptr) return false;
    
    if(name.empty()) {
        auto value = refFeatureReader_->GetValue(way->GetFeatureValueBuffer());
        if(value != nullptr){
            name = value->GetRef();
        } else {
            
        }
    }
    auto maxSpeedValue = maxSpeedReader_->GetValue(way->GetFeatureValueBuffer());
    if (maxSpeedValue != nullptr) {
        maxSpeed = maxSpeedValue->GetMaxSpeed();
    } else {
        LOG(DEBUG) << "Waytype " << way->GetType()->GetName();
        auto const speedEntry = carSpeedTable_.find(way->GetType()->GetName());
        if(speedEntry != carSpeedTable_.end()) {
            maxSpeed = speedEntry->second;
        }
    }
    return true;
}

void MapManager::SearchCurrentWayInfo() {
    std::string wayName;
    int maxSpeed;
    if(FindWayInfoForCoord(mapCenter_, wayName, maxSpeed)) {
        if(callbackNewName_){
            callbackNewName_(wayName, maxSpeed, currentSpeed_);
        }
    } else {
        if(callbackNewName_){
            callbackNewName_("", -1, currentSpeed_);
        }
    }
}

MapManager::MapManager() {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    screenDpi_ = 96.0;
    mapPixels_ = nullptr;
    mapAngle_ = 0;
    image_data_source_ = nullptr;
    paintMarker_ = false;
    callbackNewName_ = nullptr;
    image_data_marker_ = nullptr;
    currentSpeed_ = -1;
    routerFilenamebase_=osmscout::RoutingService::DEFAULT_FILENAME_BASE;
    maxSpeedReader_ = nullptr;
    cairoImage_ = nullptr;
    painter_ = nullptr;
    refFeatureReader_ = nullptr;
    width_ = 0;
    height_ = 0;
}

MapManager::~MapManager() {
    if(mapPixels_ != nullptr) delete [] mapPixels_;
    if(cairoImage_ != nullptr){
        cairo_destroy(cairoImage_);
    }
    if(image_data_source_ != nullptr){
        cairo_surface_destroy(image_data_source_);
    }
    if(image_data_marker_ != nullptr){
        cairo_surface_destroy(image_data_marker_);
    }
    if(maxSpeedReader_ != nullptr){
        delete maxSpeedReader_;
    }
}

int MapManager::Init(std::string dataPath, std::string mapStyle, std::vector<std::string> mapIconPaths, osmscout::GeoCoord position) {
    osmscout::log.SetLogger(new utils::osmsoutlogger());
    std::list<std::string> paths;
    std::copy(mapIconPaths.begin(), mapIconPaths.end(), std::back_inserter( paths ) );
    dataPath_ = dataPath;
    
    database_.reset(new osmscout::Database(databaseParameter_));
    if (!database_->Open(dataPath.c_str())) {
        LOG(ERROR) << "Cannot open database";
        return -1;
    }
    
    mapService_.reset(new osmscout::MapService(database_));
    styleConfig_.reset(new osmscout::StyleConfig(database_->GetTypeConfig()));
    if (!styleConfig_->Load(mapStyle)) {
        LOG(ERROR) << "Cannot open style";
        return -1;
    }
    
    drawParameter_.SetIconPaths(paths);
    drawParameter_.SetPatternPaths(paths);
    drawParameter_.SetFontSize(3.0);
    drawParameter_.SetDebugData(false);
    drawParameter_.SetDebugPerformance(true);
    // We want to get notified, if we have more than 1000 objects from a certain type (=> move type rendering to a higher zoom level?)
    //drawParameter_.SetWarningObjectCountLimit(1000);
    // We want to get notified, if we have more than 20000 coords from a certain type (=> move type rendering to a higher zoom level?)
    //drawParameter_.SetWarningCoordCountLimit(20000);
    drawParameter_.SetRenderBackground(true); // we draw background before MapPainter
    drawParameter_.SetRenderUnknowns(false); // it is necessary to disable it with multiple databases
    drawParameter_.SetFontName("Inconsolata");
    searchParameter_.SetUseMultithreading(true);
    //searchParameter_.SetUseLowZoomOptimization(true);
    breaker_ = std::make_shared<osmscout::ThreadedBreaker>();
    drawParameter_.SetBreaker(breaker_);

    magnification_.SetLevel(osmscout::Magnification::magBlock);
    mapCenter_ = position;
    
    painter_ = new osmscout::MapPainterCairo(styleConfig_);
    
    osmscout::RouterParameter              routerParameter;
    routingService_ = std::make_shared<osmscout::SimpleRoutingService>(database_, routerParameter, routerFilenamebase_);
    
    if (!routingService_->Open()) {
         LOG(ERROR) << "Cannot open routing database";
         return -1;
    }
    
    //Todo Put Parameter to Config
    osmscout::TypeConfigRef             typeConfig=database_->GetTypeConfig();
    routingProfile_ = std::make_shared<osmscout::FastestPathRoutingProfile>(database_->GetTypeConfig());
    GetCarSpeedTable(carSpeedTable_);
    if(!routingProfile_->ParametrizeForCar(*typeConfig, carSpeedTable_, 130.0)) {
        LOG(WARNING) << "Missing Speedlimits for Types";
    }
    
    maxSpeedReader_ = new osmscout::MaxSpeedFeatureValueReader(*typeConfig);
    refFeatureReader_ =  new osmscout::RefFeatureValueReader(*typeConfig);
    
    worker_ = std::thread(&MapManager::WorkerMain, this);
    
    return 0;
}

void MapManager::RegisterMe(int width, int height, NewMapImageDelegate callback, NewStreetNameOrSpeedDelegate callbackName) {
    width_ = width;
    height_ = height;

    mapWidth_ = width * 2;
    mapHeight_ = height * 2;
    
    mapPixels_ = new unsigned char[mapWidth_ * mapHeight_ * 4];
    callbackNewMapImage_ = callback;
    callbackNewName_ = callbackName;
    
    auto mydata2 = new ThreadJobData();
    mydata2->whattodo = "DrawMap";
    mydata2->data1 = nullptr;
    mydata2->data2 = nullptr;

    jobQueue_.add(mydata2);
}

void MapManager::Unregister() {
    callbackNewMapImage_ = nullptr;
    callbackNewName_ = nullptr;
}

void MapManager::Deinit() {
    callbackNewMapImage_ = nullptr;
    
    if(worker_.joinable()) {
        jobQueue_.clear();
        
        auto mydata2 = new ThreadJobData();
        mydata2->whattodo = "Exit";
        mydata2->data1 = nullptr;
        mydata2->data2 = nullptr;

        jobQueue_.add(mydata2);
        worker_.join();
    }
}

void MapManager::SetScreenDpi(float screenDpi) {
    screenDpi_ = screenDpi;
}

void MapManager::CenterMap(const osmscout::GeoCoord& coord, const double& compass, const double& currentSpeed) {
    LOG(DEBUG) << "CenterMap " << coord.GetDisplayText();
    if (compass > -1) {
        //double angleDeg = (360*(angle/(2*M_PI))); found in code
        // full turn is 2*PI
        mapAngle_ = -compass * (2.0 * M_PI) / 360.0;
    }
                
    mapCenter_ = coord;
    currentSpeed_ = currentSpeed;
    paintMarker_ = true;
    
    projectionCalc_.Set(mapCenter_,
                    mapAngle_,
                    magnification_,
                    screenDpi_,
                    mapWidth_,
                    mapHeight_);
    
    double x, y;
    
    // New Pos every second NMEA Standard i think
    auto speedToDistance = currentSpeed_/3.6; //m/sec wenn speed km/h
    
    auto posIThink = mapCenter_.Add(osmscout::Bearing::Degrees(mapAngle_), osmscout::Distance::Of<osmscout::Meter>(speedToDistance));
    
    if(projectionCalc_.GeoToPixel(posIThink, x, y)) {
        LOG(DEBUG) << "Neue Pos ist auf Karte X " << x << " Y " << y << " Geo " << posIThink.GetDisplayText();
    }
    
    if(jobQueue_.size() == 0) {
        auto mydata2 = new ThreadJobData();
        mydata2->whattodo = "DrawMap";
        mydata2->data1 = nullptr;
        mydata2->data2 = nullptr;

        jobQueue_.add(mydata2);
    } else {
        LOG(WARNING) << "Redraw lost";
    }
}

void MapManager::GetCarSpeedTable(std::map<std::string,double>& map) {
    //Todo put in to Config
    map["highway_motorway"]=110.0;
    map["highway_motorway_trunk"]=100.0;
    map["highway_motorway_primary"]=70.0;
    map["highway_motorway_link"]=60.0;
    map["highway_motorway_junction"]=60.0;
    map["highway_trunk"]=100.0;
    map["highway_trunk_link"]=60.0;
    map["highway_primary"]=70.0;
    map["highway_primary_link"]=60.0;
    map["highway_secondary"]=60.0;
    map["highway_secondary_link"]=50.0;
    map["highway_tertiary_link"]=55.0;
    map["highway_tertiary"]=55.0;
    map["highway_unclassified"]=50.0;
    map["highway_road"]=50.0;
    map["highway_residential"]=40.0;
    map["highway_roundabout"]=40.0;
    map["highway_living_street"]=10.0;
    map["highway_service"]=30.0;
}

size_t MapManager::SearchForText(const std::string& text, std::vector<std::string>& results) {
    osmscout::TextSearchIndex textSearch;
    results.clear();
    
    if (!textSearch.Load(dataPath_)) {
        return -1;
    }
    
    osmscout::TextSearchIndex::ResultsMap osmResults;

    textSearch.Search(text, true, true, true, true, osmResults);
    if (osmResults.empty()) {
        return 0;
    }
    
    for (auto it = osmResults.begin(); it != osmResults.end(); ++it) {
        results.push_back(it->first);
    }
    
    return osmResults.size();
}