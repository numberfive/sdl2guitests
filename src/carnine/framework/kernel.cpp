#include "stdafx.h"
#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "Kernel"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

//#define MILLESECONDS_PER_FRAME 1000.0/120.0       /* about 120 frames per second */
#define MILLESECONDS_PER_FRAME 1000.0/60.0       /* about 60 frames per second */

#ifdef NVWAMEMCHECK
#	include "../../common/nvwa/debug_new.h"
#endif

#include "ConfigManager.h"
#include "kernel.h"
#include "sound/AudioManager.h"
#include "media/MediaManager.h"
#include "../../common/database/SqliteException.h"
#include "../../common/database/DatabaseManager.h"
#include "../../common/commonutils.h"
#include "gui/GUIScreen.h"
#include "gui/GUIException.h"
#include "../../common/IllegalStateException.h"
#include "BackendConnector.h"
#include "AppEvents.h"
#include "../../common/plugin/PluginInterface.h"
#include "../../common/plugin/SharedLibrary.h"
#include "../../common/filesystem/directory_iterator.h"
#ifdef OSMSCOUT
    #include "map/MapManager.h"
#endif


std::ostream& operator<<(std::ostream& os, const KernelState c)
{
	switch (c)
	{
		case KernelState::Startup: os << "Startup";    break;
		case KernelState::Shutdown: os << "Shutdown"; break;
		default: os.setstate(std::ios_base::failbit);
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, const KernelEvent c)
{
	switch (c)
	{
		case KernelEvent::Shutdown: os << "Shutdown"; break;
		default: os.setstate(std::ios_base::failbit);
	}
	return os;
}

int Kernel::PlayMusik(const std::string& filename) const {
	if(audioManager_ == nullptr) {
		throw NullPointerException("No Audio Manager");
    }
    return audioManager_->PlayMusik(filename);
}

int Kernel::PlaySound(const std::string& filename) const {
    if(audioManager_ == nullptr) {
        throw NullPointerException("No Audio Manager");
    }

    if(!utils::FileExists(filename)) {
        throw NullPointerException("Audio File Not Exits");
    }

    return audioManager_->PlayBackground(filename);
}

void Kernel::VolumeUp() {
    if(audioManager_ == nullptr) {
        throw NullPointerException("No Audio Manager");
    }
    audioManager_->MusikVolumeUp();
}
    
void Kernel::VolumeDown() {
    if(audioManager_ == nullptr) {
        throw NullPointerException("No Audio Manager");
    }
    audioManager_->MusikVolumeDown();
}

void Kernel::StopMusik() const {
	audioManager_->StopMusik();
}

void Kernel::StartMediaManager() {
    mediaManager_ = new MediaManager(databaseManager_, configManager_->GetMediathekBasePath(), configManager_->GetDataPath());
    mediaManager_->Init();
}

void Kernel::StartDatabase() {
    databaseManager_ = new DatabaseManager(configManager_->GetDataPath());
}

void Kernel::StartServices() {
#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
   	TIMED_SCOPE_IF(timerStartDatabase, "StartDatabase", VLOG_IS_ON(4));
#endif
    StartDatabase();
#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
   	TIMED_SCOPE_IF(timerStartMediaManager, "StartMediaManager", VLOG_IS_ON(4));
#endif
    StartMediaManager();
    databaseManager_->Init();
#ifdef OSMSCOUT
#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
   	TIMED_SCOPE_IF(timerStartMapManager, "StartMapManager", VLOG_IS_ON(4));
#endif
    mapManager_ = new MapManager();
    if(mapManager_->Init(configManager_->GetMapDataPath(), configManager_->GetMapStyle(), configManager_->GetMapIconPaths(), configManager_->GetGetLastPosition()) != 0) {
        LOG(ERROR) << "Failed to start MapManager";
    }
#endif
    backend_ = new BackendConnector(eventManager_);
    backend_->Init();
    
    LoadPlugins();
}

void Kernel::LoadPlugins() {
  // Get plugin descriptor and exports
  auto path = configManager_->GetAppBasePath();
  path += "plugin/";

  try {
    for (std::string fileName : directory_iterator(path)) {
          LOG(TRACE) << "Saw file '" << fileName << "'";
          std::string extension(".so"); // TODO: Handle case insensitivity and windows .dll
          if (std::equal(extension.rbegin(), extension.rend(), fileName.rbegin())) {
              LOG(TRACE) << "This file can be an plugin.";
              SharedLibrary lib;
              lib.open(fileName);
              PLUGIN::PluginDetails* info = nullptr;
              lib.sym("exports", reinterpret_cast<void**>(&info));
              if(info != nullptr && info->apiVersion == CARNINE_PLUGIN_API_VERSION){
                  //Todo put in list an start it.
              }
          }
      }
    }catch(std::runtime_error &error) {
        LOG(ERROR) << "LoadPlugins failed " << error.what();
    }
      
}

MediaManager* Kernel::GetMediaManger() const {
    return mediaManager_;
}

AudioManager* Kernel::GetAudioManager() const {
    return audioManager_;
}

MapManager* Kernel::GetMapManager() const {
    if(mapManager_ == nullptr) {
        throw NullPointerException("No Map Manager");
    }
    return mapManager_;
}

SDLEventManager* Kernel::GetEventManager() const {
    if(eventManager_ == nullptr) {
        throw NullPointerException("No Event Manager");
    }
    return eventManager_;
}

ConfigManager* Kernel::GetConfigManager() const {
    if(configManager_ == nullptr) {
        throw NullPointerException("No Config Manager");
    }
    return configManager_;
}

std::string Kernel::GetConfigText() const {
    if(configManager_ == nullptr) {
        throw NullPointerException("No Config Manager");
    }

    return configManager_->GetConfigText();
}

void Kernel::HandleEvent(const SDL_Event& event,bool& exitLoop)
{
    GUIScreen* screen = nullptr;

    auto winId = event.window.windowID;
    if (winId == 0) {
        winId = event.user.windowID;
    }
    
    if (event.type == SDL_FINGERMOTION ||
        event.type == SDL_FINGERDOWN ||
        event.type == SDL_FINGERUP) {
        //Todo how to map touchId to Window
        LOG(DEBUG) << "Touch Id " << event.tfinger.touchId;
        winId = 1;
	}

	if (screens_.find(winId) != screens_.end()) {
		screen = screens_[winId];
	}
	
	if(screen) {
		screen->HandleEvent(&event);
	}
	
	KernelEvent type;
	if (eventManager_->IsKernelEvent(&event, type)) {
		switch (type)
		{
			case KernelEvent::Shutdown:
				{
					LOG(INFO) << "Kernel Event Shutdown";
					exitLoop = true;
					break;
				}
			default:
				{
					LOG(WARNING) << "Not Implemented Kernel Event " << type;
				}
		}
	}


	AppEvent code;
	void* data1;
	void* data2;
	if (eventManager_->IsApplicationEvent(&event, code, data1, data2)) {
        //better move to Kernel event ?
        if(code == AppEvent::NewGeopos && mapManager_ != nullptr) {
            KernelGPSMessage* message = (KernelGPSMessage*)data1;
            configManager_->UpdateLastPosition(message->coord);
            mapManager_->CenterMap(message->coord, message->compass, message->speed);
        } else if(code == AppEvent::LongClick || code == AppEvent::Click) {
            PlaySound("Click.wav");
        } else {
            if (applicationEventCallbackFunction_) {
                applicationEventCallbackFunction_(code, data1, data2);
            }
        }
	}

	switch (event.type)
	{
		case SDL_QUIT:
			exitLoop = true;
			break;
        case SDL_KEYDOWN: {
                const Uint8 *state = SDL_GetKeyboardState(NULL);
                switch (event.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        exitLoop = true;
                        break;
                    case SDLK_q:
                        if(state[SDL_SCANCODE_LCTRL] || state[SDL_SCANCODE_RCTRL]) { 
                            exitLoop = true;
                        }
                        break;
                    case SDLK_f:
                        if(state[SDL_SCANCODE_LCTRL] || state[SDL_SCANCODE_RCTRL]) { 
                            if(screen) {
                                screen->ToggleFullscreen();
                            }
                        }
                        break;
                }
            }
		default:
			//LOG(DEBUG) << event.type << " unhandelt event type";
			break;
	}
}

Kernel::Kernel():
	base_(nullptr),
	eventManager_(nullptr),
	firstrun_(true),
	audioManager_(nullptr),
	configManager_(nullptr),
	mediaManager_(nullptr), 
	databaseManager_(nullptr),
    mapManager_(nullptr),
    backend_(nullptr),
    screenDpi_(0) {
	logger_ = el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
}

Kernel::~Kernel() {
    if(mapManager_ != nullptr) delete mapManager_;
    if(mediaManager_ != nullptr) delete mediaManager_;
    if(databaseManager_ != nullptr) delete databaseManager_;
    if(eventManager_ != nullptr) delete eventManager_;
    if(base_ != nullptr) delete base_;
    if(audioManager_ != nullptr) delete audioManager_;
    if(backend_ != nullptr) delete backend_;
}

bool Kernel::StartUp(int argc, char* argv[])
{
	try
	{
		START_EASYLOGGINGPP(argc, argv);

		if(utils::FileExists("logger.conf")) {
			// Load configuration from file
			el::Configurations conf("logger.conf");
			// Now all the loggers will use configuration from file and new loggers
			el::Loggers::setDefaultConfigurations(conf, true);
		}
		
		LOG(INFO) << "Kernel is starting";

		configManager_ = new ConfigManager();
		configManager_->Init();

		base_ = new SDLBase();
		base_->Init();
		eventManager_ = new SDLEventManager();
		const auto result = eventManager_->Init();
		if(!result) {
			delete base_;
			delete eventManager_;
			return result;
		}
		return result;
	}
	catch(std::exception &error)
	{
        LOG(ERROR) << "Init failed " << error.what();
        return false;
	}
}

void Kernel::SetStateCallBack(KernelStateCallbackFunction callback) {
    callbackState_ = callback;
}

void Kernel::Run() {
    SDL_Event event;
    auto quit = false;
    auto delay = static_cast<int>(MILLESECONDS_PER_FRAME);

    while (!quit)
    {
        try
        {
            const auto startFrame = SDL_GetTicks();
            while (eventManager_->WaitEvent(&event, delay) != 0) {
                    HandleEvent(event, quit);
                    auto winId = event.window.windowID;
                    if (winId == 0) {
                            winId = event.user.windowID;
                    }

                    if (winId == 0) {
                            //Update all Windows
                            auto screenPtr = screens_.begin();
                            while (screenPtr != screens_.end()) {
                                    screenPtr->second->UpdateAnimationInternal();
                                    if (screenPtr->second->NeedRedraw())
                                    {
                                            screenPtr->second->Draw();
                                    }
                                    ++screenPtr;
                            }
                    }
                    else {
                            const auto screenPtr = screens_.find(winId);
                            if (screenPtr != screens_.end()) {
                                    screenPtr->second->UpdateAnimationInternal();
                                    if (screenPtr->second->NeedRedraw()) {
                                            screenPtr->second->Draw();
                                    }
                            }
                    }
            }

            auto screenPtr = screens_.begin();
            while (screenPtr != screens_.end()) {
                    screenPtr->second->UpdateAnimationInternal();
                    if (screenPtr->second->NeedRedraw()) {
                        screenPtr->second->Draw();
                    }
                    ++screenPtr;
            }

            const auto endFrame = SDL_GetTicks();

            /* figure out how much time we have left, and then sleep */
            delay = static_cast<int>(MILLESECONDS_PER_FRAME - (endFrame - startFrame));
            if (delay < 0) {
                delay = 0;
            }
            else if (delay > MILLESECONDS_PER_FRAME) {
                delay = static_cast<int>(MILLESECONDS_PER_FRAME);
            }

            if (callbackState_ && firstrun_) {
                firstrun_ = false;
                callbackState_(KernelState::Startup);
            }
        }
        catch (GUIException &exp)
        {
            LOG(ERROR) << "GUI Error " << exp.what();
            quit = true;
        }
        catch (SDLException &exp)
        {
            LOG(ERROR) << "SDL Error " << exp.what();
            quit = true;
        }
        catch (TTFException &exp)
        {
            LOG(ERROR) << "TTF Error " << exp.what();
            quit = true;
        }
        catch (SqliteException &exp)
        {
            LOG(ERROR) << "Sqlite Error " << exp.what();
            quit = true;
        }
        catch (IllegalStateException &exp)
        {
            LOG(ERROR) << "State Error " << exp.what();
            quit = true;
        }
        catch (std::exception &exp)
        {
            LOG(ERROR) << "Error " << exp.what();
            quit = true;
        }
    }

    callbackState_(KernelState::Shutdown);

}

void Kernel::Shutdown() {
    if(backend_ != nullptr) {
        backend_->Shutdown();
    }
    
	auto screenEntry = screens_.begin();
	while (screenEntry != screens_.end())
	{
		screenEntry->second->Shutdown();
		delete screenEntry->second;
		++screenEntry;
	}
	screens_.clear();
    
    if (mapManager_ != nullptr) {
        mapManager_->Deinit();
        delete mapManager_;
        mapManager_ = nullptr;
    }
    
    if(backend_ != nullptr) {
        delete backend_;
        backend_ = nullptr;
    }
    
    if(mediaManager_ != nullptr) {
		mediaManager_->Deinit();
		delete mediaManager_;
		mediaManager_ = nullptr;
	}

    if(audioManager_ != nullptr) {
        configManager_->UpateVolume(audioManager_->GetVolume());
		audioManager_->Shutdown();
	}
    
	if(configManager_ != nullptr) {
		configManager_->Shutdown();
		delete configManager_;
		configManager_ = nullptr;
	}
    
    if(databaseManager_ != nullptr) {
        databaseManager_->Deinit();
        delete databaseManager_;
		databaseManager_ = nullptr;
    }
    
	delete eventManager_;
	eventManager_ = nullptr;
    
	delete base_;
	base_ = nullptr;
	
}

void Kernel::SendShutdownBackend(bool power) {
    if(backend_ != nullptr) {
        backend_->SendShutdown(power);
    }
}

void Kernel::SendInitDoneBackend() {
    if(backend_ != nullptr) {
        backend_->SendInitDone();
    }
}

//todo build second Interface with all parameters
GUIElementManager* Kernel::CreateScreen(const std::string& title) {
#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
    	TIMED_SCOPE_IF(timerInitVideo, "InitVideo", VLOG_IS_ON(4));
#endif
    screenDpi_ = base_->InitVideo(configManager_->GetVideoDriver());
	auto screen = new GUIScreen();

#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
    	TIMED_SCOPE_IF(timerCreateScreen, "CreateScreen", VLOG_IS_ON(4));
#endif

	const auto manager = screen->Create(title, eventManager_, mapManager_);
	auto id = screen->GetId();

	screens_.insert(std::make_pair(id, screen));
    if(mapManager_ != nullptr) mapManager_->SetScreenDpi(screenDpi_);
	return manager;
}

void Kernel::RegisterApplicationEvent(ApplicationEventCallbackFunction callbackFunction)
{
	applicationEventCallbackFunction_ = callbackFunction;
}

int Kernel::StartAudio() {
    base_->InitAudio(configManager_->GetAudioDriver());
	audioManager_ = new AudioManager(eventManager_, configManager_->GetVolume());
	auto result = audioManager_->Init();
	if(result != 0) {
		LOG(ERROR) << "Audio Init Failed";
		//Todo show on screen
		return result;
	}

	LOG(INFO) << "AudioManager Started";
	return 0;
}

el::base::type::StoragePointer Kernel::SharedLoggingRepository() {
	return el::Helpers::storage();

	// There are two ways to share repository
	// one way is:
	// SHARE_EASYLOGGINGPP(sharedLoggingRepository())
	// Other way is
	// INITIALIZE_NULL_EASYLOGGINGPP
	// and in main function:
	// int main(int argc, char** argv) {
	//     el::Helpers::setStorage(sharedLoggingRepository());
}
