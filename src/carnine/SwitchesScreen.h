#pragma once
#include "framework/gui/GUIElement.h"
#include "framework/gui/GUITestElement.h"
#include "framework/ConfigManager.h"

class SwitchesScreen
{
private:
    GUIElementManager* manager_;
    ConfigManager* _configManager;
    std::map<std::string,Button_Event> _buttonState;
    bool MessageFromBackend(json const& message);
public:
    SwitchesScreen(GUIElementManager* manager, ConfigManager* _configManager);
    ~SwitchesScreen();

    GUIElement* Create();
};


