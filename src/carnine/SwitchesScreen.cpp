#include "SwitchesScreen.h"
#include "framework/gui/GUIElementManager.h"
#include "framework/gui/GUITextButton.h"
#include "framework/SDLEventManager.h"
#include "../common/BackendMessages.h"
#include "../common/utils/json.hpp"

using json = nlohmann::json;

bool SwitchesScreen::MessageFromBackend(json const& message) {
    BaseMessage messageJson = message;
    if(messageJson.MessageType == "SwitchEvent") {
        SwitchEvent event = message;
        std::string buttonName = "IoButton" + event.device + std::to_string(event.id);
        GUITextButton* button = static_cast<GUITextButton*>(manager_->GetElementByName(buttonName));
        if(button && event.event == Button_Event::On) {
            //button->Select();
            button->ChangeBackColor(own_red_color);
            _buttonState[buttonName] = Button_Event::Off;
        } else if (button && event.event == Button_Event::Off) {
            button->ChangeBackColor(own_blue_color);
            _buttonState[buttonName] = Button_Event::On;
        }
        return true;
    }
    return false;
}

SwitchesScreen::SwitchesScreen(GUIElementManager* manager, ConfigManager* configManager) {
    manager_ = manager;
    _configManager = configManager;
}

SwitchesScreen::~SwitchesScreen() {
}

GUIElement* SwitchesScreen::Create() {
    auto screenBackground = new GUITestElement(GUIPoint(0, 0), GUISize(100, 100, sizeType::relative), lightgray_t_color, "switchesScreen");
    manager_->AddElement(screenBackground);
    screenBackground->ImageBackground("SwitchesBackground.png");

    auto messageFromBackendDelegate = std::bind(&SwitchesScreen::MessageFromBackend, this, std::placeholders::_1);
    screenBackground->EventManager()->RegisterMeForBackendMessage(messageFromBackendDelegate);

    int count = 0;
    for (auto &buttonEntry : _configManager->GetButtonConfig()) {
        const std::string buttonName = "IoButton" + buttonEntry.Device + std::to_string(buttonEntry.Id);
        _buttonState[buttonName] = Button_Event::On;
        auto test5 = new GUITextButton(GUIPoint(buttonEntry.PosX, buttonEntry.PosY), GUISize(buttonEntry.Width, buttonEntry.Height), buttonName, own_blue_color, lightgray_t_color);
        manager_->AddElement(screenBackground, test5);
        //test5->FontHeight(30);
        test5->Text(buttonEntry.DisplayName);
        test5->RegisterOnClick([buttonEntry, this, buttonName](IGUIElement* sender) {
            SwitchAction action;
            action.device = buttonEntry.Device;
            action.event = _buttonState[buttonName];
            action.id = buttonEntry.Id;
            action.type = buttonEntry.Type;
            sender->EventManager()->PushBackendMessage(action);
        });
        count++;
    }
    
    screenBackground->Invisible();
    
    SendIoStates stateMessage;
    screenBackground->EventManager()->PushBackendMessage(stateMessage);

    return screenBackground;
}