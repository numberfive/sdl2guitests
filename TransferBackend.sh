SFTP_SERVER=$RPI_IP
SFTP_USER=$RPI_USER
SFTP_PWD=$RPI_PASSWORD
currentdir=$(pwd)

if [[ -z "$SFTP_SERVER" ]]; then
   SFTP_SERVER="192.168.2.50"
fi

if [[ -z "$SFTP_USER" ]]; then
   SFTP_USER="pi"
fi

if [[ -z "$SFTP_PWD" ]]; then
   SFTP_PWD="raspberry"
fi

sshpass -p $SFTP_PWD ssh $SFTP_USER@$SFTP_SERVER 'sudo systemctl stop CarnineBackendD'
echo backenend stopped
pscp -batch -pw "$SFTP_PWD" $currentdir/bin/PI/CarnineBackendD.bin pi@$SFTP_SERVER:/home/pi/devtest/
sshpass -p $SFTP_PWD ssh $SFTP_USER@$SFTP_SERVER 'sudo systemctl start CarnineBackendD'
echo backenend started