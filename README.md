## Projekt to Build an Gui and an Backend Service for an CarPC [see](https://www.carnine.de)
Todo Project Description 

### Dependencies
look at DebianPackages.txt
[see](https://www.carnine.de/depends.html)

### Known Bugs

* Inhalt bei ziehen anzeigen no Redraw beim ziehen unter Windows SDL2 Bug ?
  * SDL_SetWindowsMessageHook ein weg ? Ist aber Plattform abhängig egal da Windows nicht die Ziel Plattform ist.
* Nach dem Bildschirmschoner schwarzen Fenster Auf dem PI gibt es keinen Bildschrim Bildschirmschoner
* High DPI Aware könnte bei machne Displays ein Thema werden
* search for an funktion/logik to insert the screen before statusbar an action because theare always on top

### CommandLine
add --v=9 to commandline to see all log Verbose level 9 (Performens is slower)

### How to Build
[Build Dependecy](https://www.carnine.de/depends.html)
```
git clone
mkdir build
cd build
cmake -DTRAGET=Linux or cmake -DTRAGET=PI ..
cmake --build .
```

### How to Install
The Software can run from Build dir but the Backend Service muss be add to Systemd (Todo Description)
Default Buildresult Dir is <clonedir>/bin/<target>/
run carnine.bin

### Key Codes
ctrl + q => Quit
ctrl + f => Fullscreen

### Config File
Location => /home/<user>/.local/share/MiNeSoftware/CarNiNe
It is Json Text File
Testet VideoDriver Values x11, RPI see also https://wiki.libsdl.org/FAQUsingSDL
On RPI4 the SDL2 Driver RPI do not work you muss use the kmsdrm !

### Nano I²C Protokoll
i2cget -y 1 0x30 0x00 w -> AD0 abfragen
i2cget -y 1 0x30 0x01 w -> AD1 abfragen
i2cget -y 1 0x30 0x02 w -> AD2 abfragen
i2cget -y 1 0x30 0x03 w -> AD3 abfragen
i2cget -y 1 0x30 0x04 w -> AD4 abfragen
i2cget -y 1 0x30 0x05 w -> AD5 abfragen
i2cget -y 1 0x30 0x06 w -> PWM 1 Abfragen
i2cget -y 1 0x30 0x07 w -> PWM 2 Abfragen
i2cget -y 1 0x30 0x08 w -> DIGI Pins abfragen
i2cset -y 1 0x30 0x10 0x10 -> Interval einstellen Value * 100 Ms
i2cset -y 1 0x30 0x11 0x10 -> PWM 1 einstellen
i2cset -y 1 0x30 0x12 0x10 -> PWM 2 einstellen 
i2cset -y 1 0x30 0x13 0x03 -> AD's enable / disable 0x03 => 0 und 1 Enabled
i2cset -y 1 0x30 0x14 0x03 -> DIGI's enable / disable 0x03 => 0 und 1 Enabled
i2cset -y 1 0x30 0x15 0x00 -> DIGI Pins auf Input umschalten 0x00 alle da wo ne 1 ist Input
i2cset -y 1 0x30 0xE0 -> Debug Meldungen auf der RS232 ein und ausschalten (Toggled)
i2cset -y 1 0x30 0xF0 -> Konfiguration im EEPROM speichern

### Netteil Dokumentation
dtoverlay gpio-poweroff,active_low=1,gpiopin=19 ->  in die /boot/config.txt eintragen ->
Mit Dig3 verbinden damit der Halt zustand erkannt werden kann. Sonst gilt ein Timeout in AVR Software
Natürlich kann man jeden Anderen GPIO auch verwenden. Nur auf dem Netzteil ist fix es sei den man
Ändert den AVR Code.
https://github.com/raspberrypi/firmware/blob/master/boot/overlays/README Dokumentation für den PI.

